package com.hot106.apibackend.requestobjects;

import com.hot106.apibackend.objects.Schedule;
import com.hot106.apibackend.requestobjects.validation.RequestKey;

public class ScheduleRequest {

    private RequestKey rqk;
    private Schedule schedule;

    public RequestKey getRqk() {
        return rqk;
    }

    public Schedule getSchedule() {
        return schedule;
    }
}
