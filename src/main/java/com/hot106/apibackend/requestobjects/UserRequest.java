package com.hot106.apibackend.requestobjects;

import com.hot106.apibackend.objects.User;
import com.hot106.apibackend.requestobjects.validation.RequestKey;

public class UserRequest {

    private RequestKey rqk;
    private User user;

    public RequestKey getRqk() {
        return rqk;
    }

    public User getUser() {
        return user;
    }
}
