package com.hot106.apibackend.requestobjects;

import com.hot106.apibackend.objects.Verify;
import com.hot106.apibackend.requestobjects.validation.RequestKey;

public class VerifyRequest {

    private RequestKey rqk;
    private Verify verify;

    public RequestKey getRqk() {
        return rqk;
    }

    public Verify getVerify() {
        return verify;
    }
}
