package com.hot106.apibackend.requestobjects;

import com.hot106.apibackend.objects.News;
import com.hot106.apibackend.requestobjects.validation.RequestKey;

public class NewsRequest {

    private RequestKey rqk;
    private News news;

    public RequestKey getRqk() {
        return rqk;
    }

    public News getNews() {
        return news;
    }
}
