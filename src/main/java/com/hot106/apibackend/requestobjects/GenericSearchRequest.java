package com.hot106.apibackend.requestobjects;

import com.hot106.apibackend.objects.Reaction;
import com.hot106.apibackend.requestobjects.validation.RequestKey;

public class GenericSearchRequest {

    private RequestKey rqk;
    private String search_query;

    public RequestKey getRqk() {
        return rqk;
    }

    public String getSearch_query() {
        return search_query;
    }
}
