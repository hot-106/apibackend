package com.hot106.apibackend.requestobjects;

import com.hot106.apibackend.objects.Session;
import com.hot106.apibackend.requestobjects.validation.RequestKey;

public class SessionRequest {

    private final RequestKey rqk;
    private final Session session;

    public SessionRequest(RequestKey rqk, Session session) {
        this.rqk = rqk;
        this.session = session;
    }

    public RequestKey getRqk() {
        return rqk;
    }

    public Session getSession() {
        return session;
    }
}
