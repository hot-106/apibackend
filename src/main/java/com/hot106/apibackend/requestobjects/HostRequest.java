package com.hot106.apibackend.requestobjects;

import com.hot106.apibackend.objects.Host;
import com.hot106.apibackend.requestobjects.validation.RequestKey;

public class HostRequest {

    private RequestKey rqk;
    private Host host;

    public RequestKey getRqk() {
        return rqk;
    }

    public Host getHost() {
        return host;
    }

}
