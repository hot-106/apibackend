package com.hot106.apibackend.requestobjects;

import com.hot106.apibackend.objects.UserPermission;
import com.hot106.apibackend.requestobjects.validation.RequestKey;

public class UserPermissionRequest {

    private RequestKey rqk;
    private UserPermission user_permission;

    public RequestKey getRqk() {
        return rqk;
    }

    public UserPermission getUser_permission() {
        return user_permission;
    }
}
