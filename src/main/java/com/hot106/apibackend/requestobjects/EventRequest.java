package com.hot106.apibackend.requestobjects;

import com.hot106.apibackend.objects.Event;
import com.hot106.apibackend.requestobjects.validation.RequestKey;

public class EventRequest {

    private RequestKey rqk;
    private Event event;

    public RequestKey getRqk() {
        return rqk;
    }

    public Event getEvent() {
        return event;
    }

}
