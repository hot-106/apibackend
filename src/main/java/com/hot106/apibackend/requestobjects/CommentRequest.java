package com.hot106.apibackend.requestobjects;

import com.hot106.apibackend.objects.Comment;
import com.hot106.apibackend.requestobjects.validation.RequestKey;

public class CommentRequest {

    private RequestKey rqk;
    private Comment comment;

    public RequestKey getRqk() {
        return rqk;
    }

    public Comment getComment() {
        return comment;
    }

}
