package com.hot106.apibackend.requestobjects;

import com.hot106.apibackend.objects.Reaction;
import com.hot106.apibackend.requestobjects.validation.RequestKey;

public class ReactionRequest {

    private RequestKey rqk;
    private Reaction reaction;

    public RequestKey getRqk() {
        return rqk;
    }

    public Reaction getReaction() {
        return reaction;
    }
}
