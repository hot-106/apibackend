package com.hot106.apibackend.requestobjects;

import com.hot106.apibackend.objects.PasswordReset;
import com.hot106.apibackend.requestobjects.validation.RequestKey;

public class PasswordResetRequest {

    private RequestKey rqk;
    private PasswordReset password_reset;

    public RequestKey getRqk() {
        return rqk;
    }

    public PasswordReset getPassword_reset() {
        return password_reset;
    }
}
