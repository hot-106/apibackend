package com.hot106.apibackend.requestobjects.validation;

public class UserSession {

    private String httpSessionId;
    private String clientIpAddress;

    public UserSession() {}

    public UserSession(String httpSessionId, String clientIpAddress) {
        this.httpSessionId = httpSessionId;
        this.clientIpAddress = clientIpAddress;
    }

    public String getHttpSessionId() {
        return httpSessionId;
    }

    public String getClientIpAddress() {
        return clientIpAddress;
    }
}
