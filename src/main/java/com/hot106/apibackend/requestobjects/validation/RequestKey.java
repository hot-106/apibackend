package com.hot106.apibackend.requestobjects.validation;

import org.slf4j.Logger;

import com.hot106.apibackend.exception.InvalidApiKeyException;
import com.hot106.apibackend.exception.InvalidParametersException;
import com.hot106.apibackend.exception.MissingParametersException;
import org.apache.http.conn.util.InetAddressUtils;

public class RequestKey {

    private String apikey;
    private UserSession userSession;

    public RequestKey() {}

    public RequestKey(String apikey, UserSession userSession) {
        this.apikey = apikey;
        this.userSession = userSession;
    }

    public String getApikey() {
        return apikey;
    }

    public UserSession getUserSession() {
        return userSession;
    }

    public static void validateRqk(RequestKey rqk, String currentApiKey, Logger logger){

        if(rqk == null) {
            logger.warn("RequestKey (rqk) was not supplied in the request body. Request denied.");
            throw new MissingParametersException("RequestKey (rqk) was not supplied in the request body. Request denied.");
        }

        if(rqk.getUserSession() == null){
            logger.warn("UserSession was not supplied in the request body. Request denied");
            throw new MissingParametersException("UserSession was not supplied in the request body. Request denied");
        }

        if(!currentApiKey.equals(rqk.getApikey())){
            logger.warn("API Key '" + rqk.getApikey() + "' is not valid. Request denied.");
            throw new InvalidApiKeyException("API key value '" + rqk.getApikey() + "' is not valid. Request denied.");
        }

        if(rqk.getUserSession().getClientIpAddress() == null){
            logger.warn("Client IP Address was not supplied in the request. Request denied.");
            throw new MissingParametersException("Client IP Address was not supplied in the request. Request denied.");
        }
        else if(!InetAddressUtils.isIPv4Address(rqk.getUserSession().getClientIpAddress()) && !InetAddressUtils.isIPv6Address(rqk.getUserSession().getClientIpAddress())){
            logger.warn("Client IP Address provided in request was not valid. IP Address: " + rqk.getUserSession().getClientIpAddress());
            throw new InvalidParametersException("Client IP Address provided in request was not valid.");
        }

    }
}
