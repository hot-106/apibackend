package com.hot106.apibackend.utils;

import com.hot106.apibackend.objects.User;
import com.hot106.apibackend.objects.Verify;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class SendGridMail {

    SendGridConfiguration conf;
    Logger logger = LoggerFactory.getLogger(SendGridMail.class);

    public SendGridMail(SendGridConfiguration conf) {
        this.conf = conf;
    }

    private boolean sendMail(Mail mailToSend) throws IOException {
        SendGrid sendGrid = new SendGrid(conf.getApikey());
        Request request = new Request();
        boolean success = true;

        request.setMethod(Method.POST);
        request.setEndpoint(conf.getEndpoint());
        request.setBody(mailToSend.build());

        Response response = sendGrid.api(request);

        if(response.getStatusCode() < 200 || response.getStatusCode() > 299){
            success = false;
            logger.info("Failed to send mail. Response code: " + response.getStatusCode() + ", Response body: " + response.getBody());
        }

        return success;
    }

    public boolean sendVerifyEmail(User userToVerify, Verify verifyData){
        logger.info("Sending verify email to " + userToVerify.getEmail());

        Mail mail = new Mail();
        Personalization personalization = new Personalization();
        boolean success = true;

        mail.setTemplateId(SendGridConfiguration.TEMPLATES.get("NEW_USER"));
        mail.setFrom(new Email(conf.getFromEmail()));
        personalization.addDynamicTemplateData("first_name", userToVerify.getFirst_name());
        personalization.addDynamicTemplateData("verify_url", conf.getVerifyUrl() + verifyData.getVerify_token());
        personalization.addTo(new Email(userToVerify.getEmail()));
        mail.addPersonalization(personalization);

        try{
            if(!sendMail(mail)){
                success = false;
                logger.error("Failed to send email to " + userToVerify.getEmail());
            }
        }
        catch (IOException ex){
            success = false;
            logger.error("An IOException occurred while sending an email to " + userToVerify.getEmail() + ": " + ex.getMessage());
        }

        return success;
    }

}
