package com.hot106.apibackend.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DatabaseConnection {

    private final Connection conn;
    private final String username;
    private final String password;
    private final String connectionString;
    private final String databaseName;

    Logger logger = LoggerFactory.getLogger(DatabaseConnection.class);

    private Connection initConn(){
        Connection newConn = null;

        try {
            newConn = DriverManager.getConnection(connectionString + databaseName, username, password);
        }
        catch (Exception ex) {
            logger.error("Unable to initiate Database connection: " + ex.getMessage());
        }

        return newConn;
    }

    public DatabaseConnection(DatabaseConfiguration dbConf) {
        this.username = dbConf.getUsername();
        this.password = dbConf.getPassword();
        this.connectionString = dbConf.getConnectionString();
        this.databaseName = dbConf.getDatabaseName();
        this.conn = initConn();
    }

    public Connection getConn() {
        return conn;
    }
}
