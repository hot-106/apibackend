package com.hot106.apibackend.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
@ConfigurationProperties("sendgrid")
public class SendGridConfiguration {

    private String apikey;
    private String endpoint;
    private String fromEmail;
    private String verifyUrl;

    public static Map<String, String> TEMPLATES = new HashMap<String, String>(){{
        put("NEW_USER", "d-c73e2bad436540f9a088f713caf15a1e");
    }};

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getVerifyUrl() {
        return verifyUrl;
    }

    public void setVerifyUrl(String verifyUrl) {
        this.verifyUrl = verifyUrl;
    }
}
