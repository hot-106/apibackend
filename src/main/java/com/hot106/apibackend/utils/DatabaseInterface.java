package com.hot106.apibackend.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.*;

import com.hot106.apibackend.exception.UnexpectedErrorException;
import com.hot106.apibackend.objects.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.Date;

public class DatabaseInterface {

    final DatabaseConfiguration dbConf;
    Logger logger = LoggerFactory.getLogger(DatabaseInterface.class);

    public DatabaseInterface(DatabaseConfiguration dbConf) {
        this.dbConf = dbConf;
    }

    final static private Map<String, String> queries = new HashMap<String, String>(){{
        //API Key
        put("verifyApiKeySQL", "SELECT key_value FROM internal_keys WHERE key_name = 'apikey'");

        //Session Queries
        put("getUserSessionSQL", "SELECT user_id, created_at, last_action, last_action_at, last_action_ip, session_ended_at FROM session WHERE session_id = ?");
        put("insertUserSessionSQL", "INSERT INTO session (session_id, user_id, created_at, last_action, last_action_at, last_action_ip) VALUES (?, ?, ?, ?, ?, ?)");
        put("checkSessionTokenSQL", "SELECT COUNT(*) FROM session where session_id = ?");
        put("endUserSessionSQL", "UPDATE session SET session_ended_at = ?, last_action = ?, last_action_at = ?, last_action_ip = ? WHERE session_id = ?");
        put("endAllUserSessionsSQL", "UPDATE session SET session_ended_at = ?, last_action = ?, last_action_at = ? WHERE user_id = ? AND session_ended_at IS NULL");
        put("updateUserSessionSQL", "UPDATE session SET last_action = ?, last_action_at = ?, last_action_ip = ? WHERE session_id = ?");

        //User Queries
        put("verifyUserIdSQL", "SELECT COUNT(*) FROM user WHERE id = ? AND deleted_date IS NULL");
        put("checkUsernameExistsSQL", "SELECT COUNT(*) FROM user WHERE UPPER(username) = ? AND deleted_date IS NULL");
        put("checkEmailExistsSQL", "SELECT COUNT(*) FROM user WHERE UPPER(email) = ? AND deleted_date IS NULL");
        put("getUserSQL", "SELECT first_name, last_name, username, password, email, role_id, img, last_modified_by, last_modified_date, deleted_date FROM user WHERE id = ? AND deleted_date IS NULL");
        put("getUserByEmailSQL", "SELECT id, first_name, last_name, username, password, role_id, img, last_modified_by, last_modified_date, deleted_date FROM user WHERE email = ? AND deleted_date IS NULL");
        put("searchUsersByQuery", "SELECT id, first_name, last_name, username, email, role_id, img, last_modified_by, last_modified_date, deleted_date FROM user WHERE id LIKE ? OR first_name LIKE ? OR last_name LIKE ? OR username LIKE ? OR email LIKE ? WHERE deleted_date IS NULL ORDER BY last_modified_date DESC LIMIT 10");
        put("searchUsersEmptyQuery", "SELECT id, first_name, last_name, username, email, role_id, img, last_modified_by, last_modified_date, deleted_date FROM user WHERE deleted_date IS NULL ORDER BY last_modified_date DESC LIMIT 10");
        put("createUserSQL", "INSERT INTO user (first_name, last_name, username, password, email, role_id, last_modified_by, last_modified_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
        put("updateUserSQL", "UPDATE user SET first_name = ?, last_name = ?, username = ?, email = ?, role_id = ?, img = ?, last_modified_by = ?, last_modified_date = ? WHERE id = ?");
        put("updateUserPassword", "UPDATE user SET password = ?, last_modified_by = ?, last_modified_date = ? where id = ?");
        put("deleteUserSQL", "UPDATE user SET deleted_date = ? WHERE id = ?");

        //User Permission Queries
        put("createUserPermissionsSQL", "INSERT INTO user_permissions (user_id, create_comment, create_event, create_host, create_news, create_reaction, create_schedule, create_session, edit_user, verify_user) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        put("getUserPermissionsSQL", "SELECT create_comment, create_event, create_host, create_news, create_reaction, create_schedule, create_session, edit_user, verify_user FROM user_permissions WHERE user_id = ?");
        put("getRolePermissionsSQL", "SELECT create_comment, create_event, create_host, create_news, create_reaction, create_schedule, create_session, edit_user, verify_user FROM user_roles WHERE role_id = ?");
        put("updateUserPermissionSQL", "UPDATE user_permissions SET create_comment = ?, create_event = ?, create_host = ?, create_news = ?, create_reaction = ?, create_schedule = ?, create_session = ?, edit_user = ?, verify_user = ? WHERE user_id = ?");

        //Password Reset Queries
        put("getPasswordReset", "SELECT reset_token, user_id, created_at, completed FROM password_reset WHERE reset_token = ?");
        put("createPasswordReset", "INSERT INTO password_reset (reset_token, user_id) VALUES (?, ?)");
        put("completePasswordReset", "UPDATE password_reset SET completed = 1 WHERE reset_token = ?");

        //Verify Email Queries
        put("createVerifyEmail", "INSERT INTO verify (verify_token, user_id) VALUES (?, ?)");
        put("getVerifyToken", "SELECT verify_token, user_id, created_at, completed FROM verify WHERE verify_token = ?");
        put("completeVerifyToken", "UPDATE verify SET completed = 1 WHERE verify_token = ?");

        //Host Queries
        put("createHostSQL", "INSERT INTO host (first_name, last_name, bio, img, facebook_url, instagram_url, twitter_url, tiktok_url, last_modified_by, last_modified_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        put("getHostByIDSQL", "SELECT first_name, last_name, bio, img, facebook_url, instagram_url, twitter_url, tiktok_url, last_modified_by, last_modified_date, deleted_date FROM host WHERE id = ? AND deleted_date IS NULL");
        put("getHostByNameSQL", "SELECT id, bio, img, facebook_url, instagram_url, twitter_url, tiktok_url, last_modified_by, last_modified_date, deleted_date FROM host WHERE first_name = ? AND last_name = ? AND deleted_date IS NULL");
        put("getAllHostSQL", "SELECT id, first_name, last_name, bio, img, facebook_url, instagram_url, twitter_url, tiktok_url, last_modified_by, last_modified_date, deleted_date FROM host WHERE deleted_date IS NULL ORDER BY first_name DESC LIMIT 10");
        put("updateHostSQL", "UPDATE host SET first_name = ?, last_name = ?, bio = ?, img = ?, facebook_url = ?, instagram_url = ?, twitter_url = ?, tiktok_url = ?, last_modified_by = ?, last_modified_date = ? WHERE id = ? AND deleted_date IS NULL");
        put("deleteHostSQL", "UPDATE host SET deleted_date = ? WHERE id = ?");

        //Schedule Queries
        put("createSchedule", "INSERT INTO schedule (title, host_id, event_id, recurring, begin, end, last_modified_by, last_modified_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
        put("updateSchedule", "UPDATE schedule SET title = ?, host_id = ?, event_id = ?, recurring = ?, begin = ?, end = ?, last_modified_by = ?, last_modified_date = ? WHERE id = ?");
        put("getScheduleById", "SELECT title, host_id, event_id, recurring, begin, end, last_modified_by, last_modified_date, deleted_date FROM schedule WHERE id = ? AND deleted_date IS NULL");
        put("getScheduleByDate", "SELECT title, host_id, event_id, recurring, begin, end, last_modified_by, last_modified_date, deleted_date FROM schedule WHERE ((begin > ? AND begin <= ?) OR (end > ? AND end <= ?)) AND deleted_date IS NULL ORDER BY begin DESC");
        put("getScheduleForHost", "SELECT title, host_id, event_id, recurring, begin, end, last_modified_by, last_modified_date, deleted_date FROM schedule WHERE host_id = ? AND deleted_date IS NULL ORDER BY begin DESC");
        put("getSchedulesBetweenTimes", "SELECT id, title, host_id, event_id, recurring, begin, end, last_modified_by, last_modified_date, deleted_date FROM schedule WHERE time(end) > ? AND time(begin) < ?");
        put("deleteSchedule", "UPDATE schedule SET deleted_date = ? WHERE id = ?");

        //Reaction Queries
        put("checkReactionExists", "SELECT COUNT(*) FROM reaction WHERE user_id = ? AND post_id = ? AND post_type = ? AND deleted_date IS NULL");
        put("getReaction", "SELECT user_id, post_type, post_id, reaction_type, last_modified_by, last_modified_date, deleted_date FROM reaction WHERE id = ? AND deleted_date IS NULL");
        put("createReaction", "INSERT INTO reaction (user_id, post_type, post_id, reaction_type, last_modified_by, last_modified_date) VALUES (?, ?, ?, ?, ?, ?)");
        put("getReactionCount", "SELECT COUNT(*) FROM reaction WHERE post_id = ? AND post_type = ? AND reaction_type = ? AND deleted_date IS NULL");
        put("deleteReaction", "UPDATE reaction SET deleted_date = CURRENT_TIMESTAMP, last_modified_by = ?, last_modified_date = ? WHERE id = ?");

        //News Queries
        put("checkNewsExists", "SELECT COUNT(*) FROM news WHERE id = ? AND deleted_date IS NULL");

        //Comment Queries
        put("checkCommentExists", "SELECT COUNT(*) FROM comment WHERE id = ? AND deleted_date IS NULL");
        put("createComment", "INSERT INTO comment (post_type, post_id, user_id, content, reply_to, created_date, last_modified_by, last_modified_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
        put("getCommentsForPost", "SELECT id, user_id, content, reply_to, created_date, last_modified_by, last_modified_date, deleted_date FROM comment WHERE post_id = ? AND post_type = ? AND deleted_date IS NULL");
        put("getComment", "SELECT post_type, post_id, user_id, content, reply_to, created_date, last_modified_by, last_modified_date FROM comment WHERE id = ? AND deleted_date IS NULL");
        put("deleteComment", "UPDATE comment SET deleted_date = CURRENT_TIMESTAMP, last_modified_by = ?, last_modified_date = ? WHERE id = ?");

        //Event Queries
        put("checkEventExists", "SELECT COUNT(*) FROM event WHERE id = ? AND deleted_date IS NULL");
    }};

    /*    START PRIVATE UTILS    */

    private Connection initConnection(){
        Connection conn = null;
        DatabaseConnection db = new DatabaseConnection(dbConf);
        conn = db.getConn();

        return conn;
    }

    private void closeConnection(Connection conn) throws SQLException{
        if(conn.isValid(10)){
            conn.close();
        }
    }

    private boolean isAlpha(String s) {
        return s.matches("^[a-zA-Z-]*$");
    }

    private boolean isAlphaNumeric(String s) {
        return s.matches("^[a-zA-Z0-9]*$");
    }

    private boolean isValidEmail(String s) {
        return s.matches("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$");
    }

    //Forcefully ends all active sessions by a user.
    private boolean endAllUserSessions(int endSessionsForUserId, String reasonForEnd) throws SQLException {
        Timestamp currentTimestamp = new java.sql.Timestamp(System.currentTimeMillis());

        Connection conn = initConnection();

        PreparedStatement ps = conn.prepareStatement(queries.get("endAllUserSessionsSQL"));
        ps.setTimestamp(1, currentTimestamp);
        ps.setString(2, "Session Forcefully Ended - " + reasonForEnd);
        ps.setTimestamp(3, currentTimestamp);
        ps.setInt(4, endSessionsForUserId);
        ps.executeUpdate();

        closeConnection(conn);

        return true;
    }

    //Gets user permissions for a specific role_id. Returns the UserPermissions if found, null otherwise.
    private UserPermission getRolePermissions(int role_id) throws SQLException {
        Connection conn = initConnection();
        UserPermission userPermission;

        PreparedStatement ps = conn.prepareStatement(queries.get("getRolePermissionsSQL"));
        ps.setInt(1, role_id);
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            userPermission = new UserPermission(0, rs.getInt("create_comment"), rs.getInt("create_event"), rs.getInt("create_host"), rs.getInt("create_news"), rs.getInt("create_reaction"), rs.getInt("create_schedule"), rs.getInt("create_session"), rs.getInt("edit_user"), rs.getInt("verify_user"));
        }
        else{
            userPermission = null;
        }

        closeConnection(conn);
        return userPermission;
    }

    //Creates a user's permissions. Returns true on success, false otherwise.
    private boolean createUserPermissions(User userToCreate) throws SQLException {
        logger.info("Creating user permissions for user ID "+ userToCreate.getId());
        boolean created = false;
        UserPermission userPermission = getRolePermissions(userToCreate.getRole_id());

        if(userPermission == null){
            logger.error("User permission role with ID " + userToCreate.getRole_id() + " does not exist. Creating permissions as GENERAL user.");
            userPermission = getRolePermissions(User.USER_ROLE_ID_GENERAL_USER);
        }

        Connection conn = initConnection();

        PreparedStatement ps = conn.prepareStatement(queries.get("createUserPermissionsSQL"));
        ps.setInt(1, userToCreate.getId());
        ps.setInt(2, userPermission.getCreate_comment());
        ps.setInt(3, userPermission.getCreate_event());
        ps.setInt(4, userPermission.getCreate_host());
        ps.setInt(5, userPermission.getCreate_news());
        ps.setInt(6, userPermission.getCreate_reaction());
        ps.setInt(7, userPermission.getCreate_schedule());
        ps.setInt(8, userPermission.getCreate_session());
        ps.setInt(9, userPermission.getEdit_user());
        ps.setInt(10, userPermission.getVerify_user());

        int rowsCreated = ps.executeUpdate();

        if(rowsCreated > 0){
            created = true;
        }
        else{
            logger.error("Unable to create user permissions for user ID " + userToCreate.getId());
        }

        closeConnection(conn);
        return created;
    }

    private boolean userIdExists(int user_id) throws SQLException {
        Connection conn = initConnection();

        PreparedStatement ps = conn.prepareStatement(queries.get("verifyUserIdSQL"));
        ps.setInt(1, user_id);
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            int rows = rs.getInt(1);
            if(rows <= 0){
                logger.warn("User ID " + user_id + " does not exist.");
                closeConnection(conn);
                return false;
            }
        }
        else{
            logger.error("SEVERE ERROR: No rows returned for query with expected output!");
            return false;
        }

        closeConnection(conn);
        return true;
    }

    private boolean hostIdExists(int host_id) throws SQLException {
        boolean exists;
        Connection conn = initConnection();

        PreparedStatement ps = conn.prepareStatement(queries.get("getHostByIDSQL"));
        ps.setInt(1, host_id);
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            exists = true;
        }
        else{
            exists = false;
        }

        closeConnection(conn);
        return exists;
    }

    //Gets a certain PasswordReset when given the reset token. Returns the PasswordReset on success, null if not found.
    private PasswordReset getPasswordReset(String resetToken) throws SQLException {
        PasswordReset foundPasswordReset;
        Connection conn = initConnection();

        PreparedStatement ps = conn.prepareStatement(queries.get("getPasswordReset"));
        ps.setString(1, resetToken);
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            foundPasswordReset = new PasswordReset(rs.getString("reset_token"), rs.getInt("user_id"), null, rs.getTimestamp("created_at"), rs.getInt("completed"));
        }
        else{
            foundPasswordReset = null;
        }

        closeConnection(conn);
        return foundPasswordReset;
    }

    //Gets a certain Verify object when given the verification token. Returns the Verify object on success, null if not found.
    private Verify getVerifyToken(String verifyToken) throws SQLException {
        Verify foundVerifyToken;
        Connection conn = initConnection();

        PreparedStatement ps = conn.prepareStatement(queries.get("getVerifyToken"));
        ps.setString(1, verifyToken);
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            foundVerifyToken = new Verify(rs.getString("verify_token"), rs.getInt("user_id"), rs.getTimestamp("created_at"), rs.getInt("completed"));
        }
        else{
            foundVerifyToken = null;
        }

        closeConnection(conn);
        return foundVerifyToken;
    }

/*    private List<Schedule> checkConflictingSchedules(LocalDateTime start, LocalDateTime end, String recurring) throws SQLException {
        List<Schedule> conflictingSchedules = new ArrayList<Schedule>();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Schedule.SCHEDULE_DATE_TIME_FORMAT);
        LocalDate today = LocalDate.now();
        LocalDateTime compareStart1 = today.atTime(start.getHour(), start.getMinute());
        LocalDateTime compareEnd1 = today.atTime(end.getHour(), end.getMinute());;
        LocalDateTime compareStart2;
        LocalDateTime compareEnd2;

        Connection conn = initConnection();
        PreparedStatement ps = conn.prepareStatement(queries.get("getSchedulesBetweenTimes"));
        ps.setString(1, start.getHour() + ":" + start.getMinute() + ":" + start.getSecond());
        ps.setString(2, end.getHour() + ":" + end.getMinute() + ":" + end.getSecond());
        ResultSet rs = ps.executeQuery();

        while(rs.next()){

            Schedule scheduleToCheck = new Schedule(rs.getInt("id"), rs.getString("title"), rs.getInt("host_id"), rs.getInt("event_id"), rs.getString("recurring"), rs.getTimestamp("begin").toString(), rs.getTimestamp("end").toString(), rs.getInt("last_modified_by"), rs.getTimestamp("last_modified_date"), rs.getTimestamp("deleted_date"));
            LocalDateTime start2 = LocalDateTime.parse(scheduleToCheck.getBegin(), formatter);
            LocalDateTime end2 = LocalDateTime.parse(scheduleToCheck.getEnd(), formatter);

            //Set compareStart2 and compareEnd2 to the time values of the selected schedule
            compareStart2 = today.atTime(start2.getHour(), start2.getMinute());
            compareEnd2 = today.atTime(end2.getHour(), end2.getMinute());

            //Check if the TIME of the given schedule and selected schedule conflict regardless of date (using TODAY for comparison)
            //If the TIME conflicts, do further checks for date, recurring, etc.
            //If the TIME does not conflict, we can assume that the given schedule and selected schedule will not conflict regardless of date.
            if(compareStart1.isBefore(compareEnd2) && compareEnd1.isAfter(compareStart2)){

                //Times conflict, test if EXACT dates conflict
                if(start.isBefore(end2) && end.isAfter(start2)){
                    //Exact date and times conflict. Therefore, schedule conflicts.
                    conflictingSchedules.add(scheduleToCheck);
                }
                else{
                    //Exact date and times do not conflict. Check if either the given schedule or selected schedule are recurring. Then check for future conflicts.
                    if(recurring != null && scheduleToCheck.getRecurring() == null){
                        //Only given schedule is recurring
                    }
                    else if(recurring == null && scheduleToCheck.getRecurring() != null){
                        //Only selected schedule is recurring
                    }
                    else if(recurring != null && scheduleToCheck.getRecurring() != null){
                        //Both given and selected schedules are recurring
                    }
                    else{
                        //Neither are recurring
                    }
                }

            }


            if(recurring == null){

            }
            else{
                if(recurring.equals(Schedule.SCHEDULE_RECURRING_DAILY)){
                    //Check only if times conflict. Even if both are recurring, day does not matter.
                }

                if(recurring.equals(Schedule.SCHEDULE_RECURRING_WEEKLY)){
                    //Check if DayOfWeek is the same. If so, check if times conflict.
                }

                if(recurring.equals(Schedule.SCHEDULE_RECURRING_MONTHLY)){
                    //Check if Month number & Day number are the same. If so, check if times conflict.
                }

                if(recurring.equals(Schedule.SCHEDULE_RECURRING_YEARLY)){

                }
            }

        }

        closeConnection(conn);
        return conflictingSchedules;
    }*/

    private boolean postIdExists(int postId, char postType) throws SQLException {
        boolean exists = false;
        Connection conn = initConnection();
        PreparedStatement ps = null;

        switch (postType){
            case Reaction.POST_TYPE_COMMENT:
                ps = conn.prepareStatement(queries.get("checkCommentExists"));
                break;
            case Reaction.POST_TYPE_EVENT:
                ps = conn.prepareStatement(queries.get("checkEventExists"));
                break;
            case Reaction.POST_TYPE_NEWS:
                ps = conn.prepareStatement(queries.get("checkNewsExists"));
                break;
            default:
                logger.error("Unable to check if post exists. Invalid post type provided (Post Type: '" + postType + "')");
                break;
        }

        if(ps != null){
            ps.setInt(1, postId);
            ResultSet rs = ps.executeQuery();

            if(rs.next()){
                int numRows = rs.getInt(1);
                if(numRows >= 1){
                    exists = true;
                }
            }
            else{
                logger.error("SEVERE ERROR: No results returned for query expecting results -> (postIdExists)");
            }
        }

        closeConnection(conn);
        return exists;
    }

    private boolean reactionExists(int userId, int postId, char postType) throws SQLException {
        boolean exists = false;
        Connection conn = initConnection();

        PreparedStatement ps = conn.prepareStatement(queries.get("checkReactionExists"));
        ps.setInt(1, userId);
        ps.setInt(2, postId);
        ps.setString(3, String.valueOf(postType));
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            int numRows = rs.getInt(1);
            if(numRows >= 1){
                exists = true;
            }
        }
        else{
            logger.error("SEVERE ERROR: No results returned for query expecting results -> (reactionExists)");
        }

        closeConnection(conn);
        return exists;
    }

    /*     END PRIVATE UTILS     */

    /*     START PRIVATE VERIFICATION     */

    //Verifies data in a new Session object to be created. Returns the provided Session object if valid, NULL otherwise.
    private Session verifyUserSession(Session sessionToVerify, boolean requireLastAction) throws SQLException {
        logger.info("Verifying new session for user ID " + sessionToVerify.getUser_id() + "...");

        if(!userIdExists(sessionToVerify.getUser_id())){
            logger.warn("Verification failed for session ID " + sessionToVerify.getSession_id() + ". User ID " + sessionToVerify.getUser_id() + " does not exist.");
            return null;
        }

        if(requireLastAction){
            if(sessionToVerify.getLast_action() == null || sessionToVerify.getLast_action().isEmpty()){
                logger.warn("Unable to verify fields for session ID " + sessionToVerify.getSession_id() + ". Last action field was not supplied.");
                return null;
            }
            else{
                if(sessionToVerify.getLast_action().length() > 100){
                    logger.warn("Verification failed for session ID " + sessionToVerify.getSession_id() + ". Last action field was too long. (" + sessionToVerify.getLast_action().length() + ")");
                }
            }

            if(sessionToVerify.getLast_action_at() == null){
                logger.warn("Unable to verify fields for session ID " + sessionToVerify.getSession_id() + ". Last action at field was not supplied.");
                return null;
            }

            if(sessionToVerify.getLast_action_ip() == null || sessionToVerify.getLast_action_ip().isEmpty()){
                logger.warn("Unable to verify fields for session ID " + sessionToVerify.getSession_id() + ". Last action IP field was not supplied.");
                return null;
            }
            else{
                if(sessionToVerify.getLast_action().length() > 50){
                    logger.warn("Verification failed for session ID " + sessionToVerify.getSession_id() + ". Last action IP field was too long. (" + sessionToVerify.getLast_action().length() + ")");
                }
            }
        }

        return sessionToVerify;
    }

    private boolean verifyUserFirstName(String first_name){
        boolean valid = true;

        if(first_name == null){
            logger.info("Request missing first_name field.");
            valid = false;
        }

        if(valid && first_name.length() > 30){
            logger.info("First name field is too long (" + first_name.length() + ")");
            valid = false;
        }

        if(valid && !isAlpha(first_name)){
            logger.info("First name field contains invalid characters (" + first_name + ")");
            valid = false;
        }

        return valid;
    }

    private boolean verifyUserLastName(String last_name){
        boolean valid = true;

        if(last_name == null){
            logger.info("Request missing last_name field.");
            valid = false;
        }

        if(valid && last_name.length() > 30){
            logger.info("Last name field is too long (" + last_name.length() + ")");
            valid = false;
        }

        if(valid && !isAlpha(last_name)){
            logger.info("Last name field contains invalid characters (" + last_name + ")");
            valid = false;
        }

        return valid;
    }

    private boolean verifyUsername(String username){
        boolean valid = true;

        if(username == null){
            logger.info("Request missing username field.");
            valid = false;
        }

        if(valid && username.length() > 20){
            logger.info("Username field is too long (" + username.length() + ")");
            valid = false;
        }

        if(valid && !isAlphaNumeric(username)){
            logger.info("Username field contains invalid characters (" + username + ")");
            valid = false;
        }

        return valid;
    }

    private boolean verifyPassword(String password){
        boolean valid = true;

        if(password == null){
            logger.info("Request missing password field.");
            valid = false;
        }

        return valid;
    }

    private boolean verifyEmail(String email){
        boolean valid = true;

        if(email == null){
            logger.info("Request missing first_name field.");
            valid = false;
        }

        if(valid && email.length() > 100){
            logger.info("First name field is too long (" + email.length() + ")");
            valid = false;
        }

        if(valid && !isValidEmail(email)){
            logger.info("First name field contains invalid characters (" + email + ")");
            valid = false;
        }

        return valid;
    }

    private boolean verifyRoleId(int roleId){
        boolean valid = true;

        if(roleId != User.USER_ROLE_ID_GENERAL_USER && roleId != User.USER_ROLE_ID_ADMIN_USER){
            valid = false;
        }

        return valid;
    }

    private boolean verifyUrl(String imgToCheck){
        boolean valid = true;

        if(imgToCheck != null){
            try{
                URL url = new URL(imgToCheck);
            }
            catch (MalformedURLException e) {
                logger.warn("An invalid URL was supplied for img field (" + imgToCheck + ")");
                valid = false;
            }

            if(valid && imgToCheck.length() > 1000){
                logger.warn("The url provided for the img field is too long (" + imgToCheck.length() + ")");
                valid = false;
            }
        }

        return valid;
    }

    private boolean verifyUserForCreate(User userToVerify) throws SQLException {
        boolean valid = true;
        logger.info("Verifying new user with email address '" + userToVerify.getEmail() + "'");

        if(!verifyUserFirstName(userToVerify.getFirst_name())){
            logger.info("First name field is invalid (" + userToVerify.getFirst_name() + ")");
            valid = false;
        }

        if(valid && !verifyUserLastName(userToVerify.getLast_name())){
            logger.info("Last name field is invalid (" + userToVerify.getLast_name() + ")");
            valid = false;
        }

        if(valid && !verifyUsername(userToVerify.getUsername())){
            logger.info("Username field is invalid (" + userToVerify.getUsername() + ")");
            valid = false;
        }

        if(valid && checkUsernameExists(userToVerify.getUsername())){
            logger.info("Username already exists (" + userToVerify.getUsername() + ")");
            valid = false;
        }

        if(valid && !verifyPassword(userToVerify.getPassword())){
            logger.info("Password field is invalid.");
            valid = false;
        }

        if(valid && !verifyEmail(userToVerify.getEmail())){
            logger.info("Email field is invalid (" + userToVerify.getEmail() + ")");
            valid = false;
        }

        if(valid && checkEmailExists(userToVerify.getEmail())){
            logger.info("Email already exists (" + userToVerify.getEmail() + ")");
            valid = false;
        }

        if(valid && !verifyRoleId(userToVerify.getRole_id())){
            logger.error("An invalid user role ID was provided. (" + userToVerify.getRole_id() + ")");
            valid = false;
        }

        return valid;
    }

    private User verifyUserForUpdate(User oldUser, User newUser) throws SQLException {
        logger.info("Verifying existing user for update (ID: " + newUser.getId() + ")");

        if(newUser.getFirst_name() == null || !verifyUserFirstName(newUser.getFirst_name())){
            logger.info("First name is invalid (" + newUser.getFirst_name() + "). Using current first name for update.");
            newUser.setFirst_name(oldUser.getFirst_name());
        }

        if(newUser.getLast_name() == null || !verifyUserLastName(newUser.getLast_name())){
            logger.info("Last name is invalid (" + newUser.getLast_name() + "). Using current last name for update.");
            newUser.setLast_name(oldUser.getLast_name());
        }

        if(newUser.getUsername() == null || !verifyUsername(newUser.getUsername())){
            logger.info("Username is invalid (" + newUser.getUsername() + "). Using current username for update.");
            newUser.setUsername(oldUser.getUsername());
        }

        if(!newUser.getUsername().equals(oldUser.getUsername()) && checkUsernameExists(newUser.getUsername())){
            logger.info("Username already exists (" + newUser.getUsername() + "). Using current username for update.");
            newUser.setUsername(oldUser.getUsername());
        }

        if(newUser.getEmail() == null || !verifyEmail(newUser.getEmail())){
            logger.info("Email is invalid (" + newUser.getEmail() + "). Using current email for update.");
            newUser.setEmail(oldUser.getEmail());
        }

        if(!newUser.getEmail().equals(oldUser.getEmail()) && checkEmailExists(newUser.getEmail())){
            logger.info("Email already exists (" + newUser.getEmail() + "). Using current email for update.");
            newUser.setEmail(oldUser.getEmail());
        }

        if(!verifyRoleId(newUser.getRole_id())){
            logger.info("Role ID is invalid (" + newUser.getRole_id() + "). Using current role ID for update.");
            newUser.setRole_id(oldUser.getRole_id());
        }

        if(!verifyUrl(newUser.getImg())){
            logger.info("Img is invalid (" + newUser.getImg() + "). Using current img for update.");
            newUser.setImg(oldUser.getImg());
        }

        return newUser;
    }

    private boolean verifyPasswordReset(PasswordReset passwordResetToVerify) throws SQLException {
        boolean valid = true;
        Date currentTime = new Date();
        long diffInSeconds;

        if(passwordResetToVerify.getUser_id() == 0){
            logger.warn("User ID not provided in request");
            valid = false;
        }

        if(valid && !userIdExists(passwordResetToVerify.getUser_id())){
            logger.warn("User ID does not exist (User ID " + passwordResetToVerify.getUser_id() + ")");
            valid = false;
        }

        diffInSeconds = (currentTime.getTime() - passwordResetToVerify.getCreated_at().getTime())/1000;
        if(valid && diffInSeconds >= PasswordReset.RESET_TOKEN_EXPIRY_TIME){
            //Invalid password reset. Too much time has passed since creation
            logger.warn("Password reset token is expired. More than " + PasswordReset.RESET_TOKEN_EXPIRY_TIME + " seconds have passed since creation.");
            valid = false;
        }

        if(valid && passwordResetToVerify.getCompleted() == 1){
            logger.warn("Password reset token has already been used.");
            valid = false;
        }

        return valid;
    }

    private UserPermission verifyUserPermission(UserPermission userPermissionToVerify) throws SQLException {

        User thisUser = getUser(userPermissionToVerify.getUser_id());

        if(thisUser == null){
            logger.warn("Unable to verify user permissions. Cannot get information for user ID " + userPermissionToVerify.getUser_id());
            return null;
        }

        //Check if user is an admin to set certain permissions
        if(userPermissionToVerify.getCreate_event() == 1 && thisUser.getRole_id() != User.USER_ROLE_ID_ADMIN_USER){
            logger.info("User ID " + userPermissionToVerify.getUser_id() + " cannot be given 'Create Event' permissions as they are not an admin user. Leaving permission disabled...");
            userPermissionToVerify.setCreate_event(0);
        }

        if(userPermissionToVerify.getCreate_host() == 1 && thisUser.getRole_id() != User.USER_ROLE_ID_ADMIN_USER){
            logger.info("User ID " + userPermissionToVerify.getUser_id() + " cannot be given 'Create Host' permissions as they are not an admin user. Leaving permission disabled...");
            userPermissionToVerify.setCreate_host(0);
        }

        if(userPermissionToVerify.getCreate_news() == 1 && thisUser.getRole_id() != User.USER_ROLE_ID_ADMIN_USER){
            logger.info("User ID " + userPermissionToVerify.getUser_id() + " cannot be given 'Create News' permissions as they are not an admin user. Leaving permission disabled...");
            userPermissionToVerify.setCreate_news(0);
        }

        if(userPermissionToVerify.getCreate_schedule() == 1 && thisUser.getRole_id() != User.USER_ROLE_ID_ADMIN_USER){
            logger.info("User ID " + userPermissionToVerify.getUser_id() + " cannot be given 'Create Schedule' permissions as they are not an admin user. Leaving permission disabled...");
            userPermissionToVerify.setCreate_schedule(0);
        }

        if(userPermissionToVerify.getEdit_user() == 1 && thisUser.getRole_id() != User.USER_ROLE_ID_ADMIN_USER){
            logger.info("User ID " + userPermissionToVerify.getUser_id() + " cannot be given 'Edit User' permissions as they are not an admin user. Leaving permission disabled...");
            userPermissionToVerify.setEdit_user(0);
        }

        return userPermissionToVerify;
    }

    private boolean verifyEmailToken(Verify verifyTokenToVerify) throws SQLException {
        boolean valid = true;
        Date currentTime = new Date();
        long diffInSeconds;

        if(verifyTokenToVerify.getUser_id() == 0){
            logger.warn("User ID not provided in request");
            valid = false;
        }

        if(valid && !userIdExists(verifyTokenToVerify.getUser_id())){
            logger.warn("User ID does not exist (User ID " + verifyTokenToVerify.getUser_id() + ")");
            valid = false;
        }

        diffInSeconds = (currentTime.getTime() - verifyTokenToVerify.getCreated_at().getTime())/1000;
        if(valid && diffInSeconds >= Verify.VERIFY_TOKEN_EXPIRY_TIME){
            //Invalid password reset. Too much time has passed since creation
            logger.warn("Verify email token is expired. More than " + Verify.VERIFY_TOKEN_EXPIRY_TIME + " seconds have passed since creation.");
            valid = false;
        }

        if(valid && verifyTokenToVerify.getCompleted() == 1){
            logger.warn("Verify email token has already been used.");
            valid = false;
        }

        return valid;
    }

    private boolean verifyHost(Host hostToVerify){
        boolean valid = true;

        if(!verifyUserFirstName(hostToVerify.getFirst_name())){
            logger.warn("Host first name is invalid (" + hostToVerify.getFirst_name() + ")");
            valid = false;
        }

        if(valid && !verifyUserLastName(hostToVerify.getLast_name())){
            logger.warn("Host last name is invalid (" + hostToVerify.getLast_name() + ")");
            valid = false;
        }

        if(valid && hostToVerify.getBio() != null){
            if(hostToVerify.getBio().length() > 5000){
                logger.warn("Host bio is too long (" + hostToVerify.getBio().length() + ")");
                valid = false;
            }
        }

        if(valid && !verifyUrl(hostToVerify.getImg())){
            logger.warn("Host img is invalid (" + hostToVerify.getImg() + ")");
            valid = false;
        }

        if(valid && !verifyUrl(hostToVerify.getFacebook_url())){
            logger.warn("Host Facebook URL is invalid (" + hostToVerify.getFacebook_url() + ")");
            valid = false;
        }

        if(valid && !verifyUrl(hostToVerify.getInstagram_url())){
            logger.warn("Host Instagram URL is invalid (" + hostToVerify.getInstagram_url() + ")");
            valid = false;
        }

        if(valid && !verifyUrl(hostToVerify.getTwitter_url())){
            logger.warn("Host Twitter URL is invalid (" + hostToVerify.getTwitter_url() + ")");
            valid = false;
        }

        if(valid && !verifyUrl(hostToVerify.getTiktok_url())){
            logger.warn("Host Tiktok URL is invalid (" + hostToVerify.getTiktok_url() + ")");
            valid = false;
        }

        return valid;
    }

    private boolean verifySchedule(Schedule scheduleToVerify) throws SQLException {
        boolean valid = true;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Schedule.SCHEDULE_DATE_TIME_FORMAT);
        LocalDateTime beginTime = null;
        LocalDateTime endTime = null;
        LocalDateTime currentTimestamp = LocalDateTime.now();

        if(scheduleToVerify.getTitle() == null){
            logger.warn("Schedule title was not provided.");
            valid = false;
        }

        if(valid && scheduleToVerify.getTitle().length() > 45){
            logger.warn("Schedule title is too long. (" + scheduleToVerify.getTitle().length() + ")");
            valid = false;
        }

        if(valid && scheduleToVerify.getHost_id() == 0){
            logger.warn("Host ID was not provided with schedule.");
            valid = false;
        }

        if(valid && !hostIdExists(scheduleToVerify.getHost_id())){
            logger.warn("Provided host ID does not exist.");
            valid = false;
        }

        if(valid && (scheduleToVerify.getRecurring() != null && !scheduleToVerify.getRecurring().equals(Schedule.SCHEDULE_RECURRING_DAILY) && !scheduleToVerify.getRecurring().equals(Schedule.SCHEDULE_RECURRING_WEEKLY) && !scheduleToVerify.getRecurring().equals(Schedule.SCHEDULE_RECURRING_MONTHLY) && !scheduleToVerify.getRecurring().equals(Schedule.SCHEDULE_RECURRING_YEARLY))){
            logger.warn("Recurring frequency value for schedule is not valid (" + scheduleToVerify.getRecurring() + ")");
            valid = false;
        }

        if(valid && scheduleToVerify.getBegin() == null){
            logger.warn("Begin time not provided.");
            valid = false;
        }
        else{
            try{
                beginTime = LocalDateTime.parse(scheduleToVerify.getBegin(), formatter);
            }
            catch (DateTimeParseException ex){
                logger.warn("Begin time is in an invalid format. Use format " + Schedule.SCHEDULE_DATE_TIME_FORMAT);
                valid = false;
            }
        }

        if(valid && scheduleToVerify.getEnd() == null){
            logger.warn("End time not provided");
            valid = false;
        }
        else{
            try{
                endTime = LocalDateTime.parse(scheduleToVerify.getEnd(), formatter);
            }
            catch (DateTimeParseException ex){
                logger.warn("End time is in an invalid format. Use format " + Schedule.SCHEDULE_DATE_TIME_FORMAT);
                valid = false;
            }
        }

        if(valid && beginTime.isBefore(currentTimestamp)){
            logger.warn("Begin time of schedule is before current time (Current: " + currentTimestamp.format(formatter) + " Begin: " + scheduleToVerify.getBegin() + ")");
            valid = false;
        }

        if(valid && endTime.isBefore(currentTimestamp)){
            logger.warn("End time of schedule is before current time (Current: " + currentTimestamp.format(formatter) + " End: " + scheduleToVerify.getEnd() + ")");
            valid = false;
        }

        if(valid && beginTime.isAfter(endTime)){
            logger.warn("Begin time of schedule is after end time (Begin: " + scheduleToVerify.getBegin() + " End: " + scheduleToVerify.getEnd() + ")");
            valid = false;
        }

        return valid;
    }

    private boolean verifyReaction(Reaction reactionToVerify, boolean checkUserIdValid) throws SQLException {
        boolean valid = true;

        if(checkUserIdValid && !userIdExists(reactionToVerify.getUser_id())){
            logger.warn("User ID " + reactionToVerify.getUser_id() + " does not exist.");
            valid = false;
        }

        if(valid && reactionToVerify.getPost_type() != Reaction.POST_TYPE_EVENT && reactionToVerify.getPost_type() != Reaction.POST_TYPE_NEWS && reactionToVerify.getPost_type() != Reaction.POST_TYPE_COMMENT){
            logger.warn("Post type is invalid for reaction ('" + reactionToVerify.getPost_type() + "')");
            valid = false;
        }

        if(valid && !postIdExists(reactionToVerify.getPost_id(), reactionToVerify.getPost_type())){
            logger.warn("Post ID does not exist or does not match post type (Post ID: " + reactionToVerify.getPost_id() + ", Post Type: " + reactionToVerify.getPost_type() + ")");
            valid = false;
        }

        if(valid && reactionToVerify.getReaction_type() != Reaction.REACTION_TYPE_LIKE && reactionToVerify.getReaction_type() != Reaction.REACTION_TYPE_DISLIKE){
            logger.warn("Reaction type is invalid ('" + reactionToVerify.getReaction_type() + "')");
            valid = false;
        }

        if(valid && reactionExists(reactionToVerify.getUser_id(), reactionToVerify.getPost_id(), reactionToVerify.getPost_type())){
            logger.warn("Reaction already exists for user ID " + reactionToVerify.getUser_id() + " on post ID " + reactionToVerify.getPost_id() + " (Post Type: " + reactionToVerify.getPost_type() + ")");
            valid = false;
        }

        return valid;
    }

    private boolean verifyComment(Comment commentToVerify) throws SQLException {
        boolean valid = true;

        if(commentToVerify.getPost_type() != Comment.POST_TYPE_EVENT && commentToVerify.getPost_type() != Comment.POST_TYPE_NEWS){
            logger.warn("Post type is invalid for comment ('" + commentToVerify.getPost_type() + "')");
            valid = false;
        }

        if(valid && !postIdExists(commentToVerify.getPost_id(), commentToVerify.getPost_type())){
            logger.warn("Post ID does not exist or does not match post type (Post ID: " + commentToVerify.getPost_id() + ", Post Type: " + commentToVerify.getPost_type() + ")");
            valid = false;
        }

        if(valid && !userIdExists(commentToVerify.getUser_id())){
            logger.warn("User ID " + commentToVerify.getUser_id() + " does not exist.");
            valid = false;
        }

        if(valid && commentToVerify.getContent() == null){
            logger.warn("Comment content cannot be NULL.");
            valid = false;
        }

        if(valid && commentToVerify.getContent().replace(" ", "").length() <= 0){
            logger.warn("Comment content cannot be empty (or only spaces). Content: " + commentToVerify.getContent());
            valid = false;
        }

        if(valid && commentToVerify.getContent().length() > 300){
            logger.warn("Comment content cannot be longer than 300 characters. (Length: " + commentToVerify.getContent().length() + ")");
            valid = false;
        }

        //Specific check if comment is a reply
        if(valid && commentToVerify.getReply_to() != 0){
            //Funky way to check if a comment ID exists
            if(!postIdExists(commentToVerify.getPost_id(), Reaction.POST_TYPE_COMMENT)){
                logger.warn("Comment ID provided in 'reply_to' field does not exist. (reply_to ID: " + commentToVerify.getReply_to() + ")");
                valid = false;
            }
        }

        return valid;
    }

    /*     END PRIVATE VERIFICATION     */

    /*     START PUBLIC VERIFICATION     */

    public String getInternalApiKey() throws SQLException {
        String keyValue = null;
        Connection conn = initConnection();

        PreparedStatement ps = conn.prepareStatement(queries.get("verifyApiKeySQL"));
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            keyValue = rs.getString("key_value");
        }
        else{
            logger.error("SEVERE ERROR: Unable to get internal apikey value!");
        }

        closeConnection(conn);
        return keyValue;
    }

    //Determines if a Session token already exists. Pretty unlikely that the same one would be generated twice. But just for safety.
    public boolean checkSessionTokenExists(String sessionToken) throws SQLException {
        Connection conn = initConnection();
        boolean exists = false;
        int rowsReturned = 0;

        PreparedStatement ps = conn.prepareStatement(queries.get("checkSessionTokenSQL"));
        ps.setString(1, sessionToken);
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            rowsReturned = rs.getInt(1);
            if(rowsReturned > 0){
                exists = true;
            }
        }

        closeConnection(conn);

        return exists;
    }

    public boolean checkUsernameExists(String usernameToCheck) throws SQLException {
        int rowsReturned;
        boolean exists = false;
        Connection conn = initConnection();

        PreparedStatement ps = conn.prepareStatement(queries.get("checkUsernameExistsSQL"));
        ps.setString(1, usernameToCheck.toUpperCase());
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            rowsReturned = rs.getInt(1);
            if(rowsReturned > 0){
                exists = true;
            }
        }
        else{
            logger.error("Unable to check if username exists. No data returned for query expecting results.");
            throw new UnexpectedErrorException("No data returned for query expecting results");
        }

        closeConnection(conn);
        return exists;
    }

    public boolean checkEmailExists(String emailToCheck) throws SQLException {
        int rowsReturned;
        boolean exists = false;
        Connection conn = initConnection();

        PreparedStatement ps = conn.prepareStatement(queries.get("checkEmailExistsSQL"));
        ps.setString(1, emailToCheck.toUpperCase());
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            rowsReturned = rs.getInt(1);
            if(rowsReturned > 0){
                exists = true;
            }
        }
        else{
            logger.error("Unable to check if email exists. No data returned for query expecting results.");
            throw new UnexpectedErrorException("No data returned for query expecting results");
        }

        closeConnection(conn);
        return exists;
    }

    /*     END PUBLIC VERIFICATION     */

    /*     START PUBLIC METHODS     */

    //Ends a user session.
    public boolean endUserSession(Session sessionToEnd, String requestIP, String reasonForEnd) throws SQLException {
        boolean closed = false;

        Connection conn = initConnection();

        PreparedStatement ps = conn.prepareStatement(queries.get("endUserSessionSQL"));
        ps.setTimestamp(1, sessionToEnd.getSession_ended_at());
        ps.setString(2, reasonForEnd);
        ps.setTimestamp(3, sessionToEnd.getLast_action_at());
        ps.setString(4, requestIP);
        ps.setString(5, sessionToEnd.getSession_id());
        int rowsUpdated = ps.executeUpdate();

        if(rowsUpdated > 0){
            closed = true;
        }

        closeConnection(conn);

        return closed;
    }

    //Determines if a pre-existing Session object is valid. Returns the provided Session object if valid, NULL otherwise.
    public Session validateUserSession(Session sessionToVerify, String sessionIPAddress) throws SQLException {
        Date currentTime = new Date();
        long diffInSeconds = 0;
        boolean sessionEnded = false;

        logger.info("Validating user session ID " + sessionToVerify.getSession_id() + "...");

        //Test if session was forcefully ended for some reason
        if(sessionToVerify.getSession_ended_at() != null){
            //Invalid session. Forcefully closed.
            logger.info("Session ID " + sessionToVerify.getSession_id() + " has been previously ended. Unable to use session.");
            return null;
        }

        //Test if difference between last_action_at and currentTime is less than SESSION_LAST_ACTION_EXPIRY_TIME
        diffInSeconds = (currentTime.getTime() - sessionToVerify.getLast_action_at().getTime())/1000;
        if(diffInSeconds >= Session.SESSION_LAST_ACTION_EXPIRY_TIME){
            //Invalid session. Too much time has passed since last action time
            logger.warn("Ending invalid session ID " + sessionToVerify.getSession_id() + ". More than " + Session.SESSION_LAST_ACTION_EXPIRY_TIME + " seconds have passed since last action was performed.");
            sessionEnded = endUserSession(sessionToVerify, sessionIPAddress, "Session Forcefully Ended - Last action expiry");
            if(!sessionEnded){
                logger.error("Failed to end session ID " + sessionToVerify.getSession_id());
            }
            return null;
        }
        else{
            //Test if difference between created_at and currentTime is less than SESSION_CREATE_EXPIRY_TIME
            diffInSeconds = (currentTime.getTime() - sessionToVerify.getLast_action_at().getTime())/1000;

            if(diffInSeconds >= Session.SESSION_CREATE_EXPIRY_TIME){
                //Invalid session. Too much time has passed since session creation, even though user is active.
                logger.warn("Ending invalid session ID " + sessionToVerify.getSession_id() + ". More than " + Session.SESSION_CREATE_EXPIRY_TIME + " seconds have passed since session was created.");
                sessionEnded = endUserSession(sessionToVerify, sessionIPAddress, "Session Forcefully Ended - Session create expiry");
                if(!sessionEnded){
                    logger.error("Failed to end session ID " + sessionToVerify.getSession_id());
                }
                return null;
            }
        }

        //Test if the current session IP address is the same as the
        if(!sessionToVerify.getLast_action_ip().equals(sessionIPAddress)){
            logger.warn("Ending invalid session ID " + sessionToVerify.getSession_id() + ". Session accessed by a new IP address (Old: " + sessionToVerify.getLast_action_ip() + " New: " + sessionIPAddress);
            sessionEnded = endUserSession(sessionToVerify, sessionIPAddress, "Session Forcefully Ended - Session accessed by new IP address");
            if(!sessionEnded){
                logger.error("Failed to end session ID " + sessionToVerify.getSession_id());
            }
            return null;
        }

        return sessionToVerify;
    }

    //Gets session details given an httpSessionId. Returns the Session object if found, null if not found.
    public Session getUserSession(String httpSessionId) throws SQLException {
        Connection conn = initConnection();
        Session session = null;
        PreparedStatement ps = conn.prepareStatement(queries.get("getUserSessionSQL"));
        ps.setString(1, httpSessionId);
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            session = new Session(httpSessionId, rs.getInt("user_id"), rs.getTimestamp("created_at"), rs.getString("last_action"), rs.getTimestamp("last_action_at"), rs.getString("last_action_ip"), rs.getTimestamp("session_ended_at"));
        }
        else{
            logger.warn("Session ID " + httpSessionId + " does not exist");
        }

        closeConnection(conn);
        return session;
    }

    //Creates a new user session. Returns the created Session object on success, null otherwise.
    public Session createUserSession(Session sessionToCreate) throws SQLException {
        logger.info("Creating session ID " + sessionToCreate.getSession_id() + " for user ID " + sessionToCreate.getUser_id() + "...");

        Session createdSession = null;
        boolean allSessionsEnded = false;

        if(verifyUserSession(sessionToCreate, false) != null){
            logger.info("Verification of session passed for user ID " + sessionToCreate.getUser_id() + " (" + sessionToCreate.getSession_id() + ")");

            allSessionsEnded = endAllUserSessions(sessionToCreate.getUser_id(), "New session created");
            if(allSessionsEnded){
                logger.info("All active sessions ended for user ID " + sessionToCreate.getUser_id());
            }
            else{
                logger.error("Failed to end all active sessions for user ID " + sessionToCreate.getUser_id());
                return null;
            }

            Connection conn = initConnection();

            PreparedStatement ps = conn.prepareStatement(queries.get("insertUserSessionSQL"));

            ps.setString(1, sessionToCreate.getSession_id());
            ps.setInt(2, sessionToCreate.getUser_id());
            ps.setTimestamp(3, sessionToCreate.getCreated_at());
            ps.setString(4, sessionToCreate.getLast_action());
            ps.setTimestamp(5, sessionToCreate.getLast_action_at());
            ps.setString(6, sessionToCreate.getLast_action_ip());
            int rowsAdded = ps.executeUpdate();

            if(rowsAdded > 0){
                logger.info("Session ID " + sessionToCreate.getSession_id() + " created successfully from " + sessionToCreate.getLast_action_ip());
                createdSession = sessionToCreate;
            }

            closeConnection(conn);
        }
        else{
            logger.warn("Verification of session failed for user ID " + sessionToCreate.getUser_id() + " (" + sessionToCreate.getSession_id() + ")");
        }

        return createdSession;
    }

    //Updates a current user session. Returns the updated Session object on success, null otherwise.
    public Session updateUserSession(Session sessionToUpdate) throws SQLException {
        logger.info("Updating session ID " + sessionToUpdate.getSession_id() + "...");

        //Get the current Session data from the Database
        Session updatedSession = getUserSession(sessionToUpdate.getSession_id());

        //Update the current Session data with the updated data provided from the user
        updatedSession.setLast_action(sessionToUpdate.getLast_action());
        updatedSession.setLast_action_at(sessionToUpdate.getLast_action_at());
        updatedSession.setLast_action_ip(sessionToUpdate.getLast_action_ip());

        //Verify data sent from the user
        if(verifyUserSession(updatedSession, true) != null){
            logger.info("Verification of session passed for user ID " + updatedSession.getUser_id() + " (" + updatedSession.getSession_id() + ")");

            //Validate current Session data with updated fields from the user (complete updated Session data in updatedSession object)
            if(validateUserSession(updatedSession, updatedSession.getLast_action_ip()) != null){
                logger.info("Validation of session passed for user ID " + updatedSession.getUser_id() + " (" + updatedSession.getSession_id() + ")");

                Connection conn = initConnection();

                PreparedStatement ps = conn.prepareStatement(queries.get("updateUserSessionSQL"));
                ps.setString(1, updatedSession.getLast_action());
                ps.setTimestamp(2, updatedSession.getLast_action_at());
                ps.setString(3, updatedSession.getLast_action_ip());
                ps.setString(4, updatedSession.getSession_id());
                int rowsUpdated = ps.executeUpdate();

                closeConnection(conn);

                if(rowsUpdated <= 0){
                    logger.error("Unable to update session ID " + updatedSession.getSession_id() + " for user ID " + updatedSession.getUser_id());
                    return null;
                }
            }
            else{
                logger.warn("Validation of session failed for user ID " + updatedSession.getUser_id() + " (" + updatedSession.getSession_id() + ")");
                return null;
            }
        }
        else{
            logger.warn("Verification of session failed for user ID " + updatedSession.getUser_id() + " (" + updatedSession.getSession_id() + ")");
            return null;
        }

        return updatedSession;
    }

    //Gets an existing user. Returns the user if exists, null otherwise.
    public User getUser(int userId) throws SQLException {
        logger.info("Getting information for user ID " + userId);
        User foundUser = null;

        Connection conn = initConnection();
        PreparedStatement ps = conn.prepareStatement(queries.get("getUserSQL"));
        ps.setInt(1, userId);
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            foundUser = new User(userId, rs.getString("first_name"), rs.getString("last_name"), rs.getString("username"), rs.getString("password"), rs.getString("email"), rs.getInt("role_id"), rs.getString("img"), rs.getInt("last_modified_by"), rs.getTimestamp("last_modified_date"), rs.getTimestamp("deleted_date"));

            //Set password to NULL on response
            foundUser.setPassword(null);
            logger.info("Successfully found user data for user ID " + userId);
        }
        else{
            logger.warn("User ID " + userId + " does not exist");
        }

        return foundUser;
    }

    //Creates a new user. Returns the created user on success, null otherwise.
    public User createUser(User userToCreate) throws SQLException {
        logger.info("Creating new user with email address '" + userToCreate.getEmail() + "'");

        if(!verifyUserForCreate(userToCreate)){
            logger.warn("Verification failed for new user with email address '" + userToCreate.getEmail() + "'");
            return null;
        }

        //Hash password for DB record
        String hashedPassword = BCrypt.hashpw(userToCreate.getPassword(), BCrypt.gensalt(12));

        Connection conn = initConnection();
        PreparedStatement ps = conn.prepareStatement(queries.get("createUserSQL"), Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, userToCreate.getFirst_name());
        ps.setString(2, userToCreate.getLast_name());
        ps.setString(3, userToCreate.getUsername());
        ps.setString(4, hashedPassword);
        ps.setString(5, userToCreate.getEmail());
        ps.setInt(6, userToCreate.getRole_id());
        ps.setInt(7, userToCreate.getLast_modified_by());
        ps.setTimestamp(8, userToCreate.getLast_modified_date());

        int rowsInserted = ps.executeUpdate();

        if(rowsInserted <= 0){
            logger.error("Unable to create user with email address '" + userToCreate.getEmail() + "'");
            closeConnection(conn);
            return null;
        }
        else{
            ResultSet rs = ps.getGeneratedKeys();
            if(rs.next()){
                int createdUserId = rs.getInt(1);
                closeConnection(conn);

                logger.info("Successfully created new user with user ID " + createdUserId);
                userToCreate.setId(createdUserId);
                userToCreate.setLast_modified_by(createdUserId);

                if(createUserPermissions(userToCreate)){
                    logger.info("Successfully created user permissions for user ID " + userToCreate.getId());
                }
                else{
                    logger.error("SEVERE ERROR: Unable to create user permissions for user ID " + userToCreate.getId());

                    conn = initConnection();
                    ps = conn.prepareStatement(queries.get("deleteUserSQL"));
                    ps.setInt(1, createdUserId);
                    int deletedRows = ps.executeUpdate();
                    closeConnection(conn);

                    if(deletedRows <= 0){
                        logger.error("SEVERE ERROR: Unable to delete created user ID " + userToCreate.getId());
                    }
                }
            }
            else{
                logger.error("Unable to get generated keys for created user (Email: " + userToCreate.getEmail() + ")");
            }
        }

        closeConnection(conn);
        return userToCreate;
    }

    //Validates the provided user email and password combination. Returns the validated user on success, null otherwise.
    public User checkUserPassword(User userToCheck) throws SQLException {
        logger.info("Checking password for user '" + userToCheck.getEmail() + "'");
        User foundUser = null;

        Connection conn = initConnection();
        PreparedStatement ps = conn.prepareStatement(queries.get("getUserByEmailSQL"));
        ps.setString(1, userToCheck.getEmail());
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            foundUser = new User(rs.getInt("id"), rs.getString("first_name"), rs.getString("last_name"), rs.getString("username"), rs.getString("password"), userToCheck.getEmail(), rs.getInt("role_id"), rs.getString("img"), rs.getInt("last_modified_by"), rs.getTimestamp("last_modified_date"), rs.getTimestamp("deleted_date"));
        }

        if(foundUser != null){
            if(!BCrypt.checkpw(userToCheck.getPassword(), foundUser.getPassword())){
                foundUser = null;
            }
            else{
                //Set password to NULL on response
                foundUser.setPassword(null);
            }
        }

        closeConnection(conn);
        return foundUser;
    }

    //Search all users. Returns a list of users where the search query matches (Limit of 10 results). Returns top 10 results if empty string query is provided.
    public List<User> searchUsers(String searchQuery) throws SQLException {
        PreparedStatement ps;
        List<User> foundUsers = new ArrayList<User>();
        Connection conn = initConnection();

        if(searchQuery == null){
            logger.info("Searching all users. Empty search query provided. Returning top 10 results.");
            ps = conn.prepareStatement(queries.get("searchUsersEmptyQuery"));
        }
        else{
            logger.info("Searching all users with search query '" + searchQuery + "'");
            ps = conn.prepareStatement(queries.get("searchUsersByQuery"));

            //Since LIKE is used in query we need to provide the wildcard values.
            String searchQuerySQL = "%" + searchQuery + "%";

            ps.setString(1, searchQuerySQL);
            ps.setString(2, searchQuerySQL);
            ps.setString(3, searchQuerySQL);
            ps.setString(4, searchQuerySQL);
            ps.setString(5, searchQuerySQL);
        }

        ResultSet rs = ps.executeQuery();

        while(rs.next()){
            User user = new User(rs.getInt("id"), rs.getString("first_name"), rs.getString("last_name"), rs.getString("username"), null, rs.getString("email"), rs.getInt("role_id"), rs.getString("img"), rs.getInt("last_modified_by"), rs.getTimestamp("last_modified_date"), rs.getTimestamp("deleted_date"));
            foundUsers.add(user);
        }

        logger.info("Returning a result set of " + foundUsers.size() + " for the provided search.");

        closeConnection(conn);
        return foundUsers;
    }

    public UserPermission getUserPermission(int user_id) throws SQLException {
        logger.info("Getting permissions for user ID " + user_id);
        Connection conn = initConnection();
        UserPermission userPermission;

        PreparedStatement ps = conn.prepareStatement(queries.get("getUserPermissionsSQL"));
        ps.setInt(1, user_id);
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            logger.info("Successfully found user permissions for user ID " + user_id);
            userPermission = new UserPermission(user_id, rs.getInt("create_comment"), rs.getInt("create_event"), rs.getInt("create_host"), rs.getInt("create_news"), rs.getInt("create_reaction"), rs.getInt("create_schedule"), rs.getInt("create_session"), rs.getInt("edit_user"), rs.getInt("verify_user"));
        }
        else{
            logger.error("Failed to get user permissions for user ID " + user_id);
            userPermission = null;
        }

        closeConnection(conn);
        return userPermission;
    }

    public UserPermission updateUserPermission(UserPermission userPermissionToUpdate) throws SQLException {
        logger.info("Updating permissions for user ID " + userPermissionToUpdate.getUser_id());

        if(!userIdExists(userPermissionToUpdate.getUser_id())){
            logger.warn("User ID " + userPermissionToUpdate.getUser_id() + " does not exist.");
            return null;
        }

        UserPermission verifiedUserPermission = verifyUserPermission(userPermissionToUpdate);

        if(verifiedUserPermission != null){
            Connection conn = initConnection();

            PreparedStatement ps = conn.prepareStatement(queries.get("updateUserPermissionSQL"));
            ps.setInt(1, verifiedUserPermission.getCreate_comment());
            ps.setInt(2, verifiedUserPermission.getCreate_event());
            ps.setInt(3, verifiedUserPermission.getCreate_host());
            ps.setInt(4, verifiedUserPermission.getCreate_news());
            ps.setInt(5, verifiedUserPermission.getCreate_reaction());
            ps.setInt(6, verifiedUserPermission.getCreate_schedule());
            ps.setInt(7, verifiedUserPermission.getCreate_session());
            ps.setInt(8, verifiedUserPermission.getEdit_user());
            ps.setInt(9, verifiedUserPermission.getVerify_user());
            ps.setInt(10, verifiedUserPermission.getUser_id());

            int rowsCreated = ps.executeUpdate();

            closeConnection(conn);

            if(rowsCreated <= 0){
                logger.error("Failed to update user permissions for user ID " + verifiedUserPermission.getUser_id());
                return null;
            }
        }
        else{
            logger.warn("User permissions provided for user ID " + userPermissionToUpdate.getUser_id() + " is not valid.");
            return null;
        }

        logger.info("Successfully updated permissions for user ID " + userPermissionToUpdate.getUser_id());
        return verifiedUserPermission;
    }

    //Updates a user's role. Returns the updated permissions on success, null otherwise.
    public UserPermission updateUserRole(int user_id, int role_id) throws SQLException {
        logger.info("Updating role for user ID " + user_id);
        UserPermission userPermission = getRolePermissions(role_id);
        UserPermission updatedUserPermission;

        if(userPermission == null){
            logger.warn("Unable to find user permissions for role ID " + role_id);
            updatedUserPermission = null;
        }
        else{
            userPermission.setUser_id(user_id);
            updatedUserPermission = updateUserPermission(userPermission);
            if(updatedUserPermission == null){
                logger.error("Unable to update user permissions for user ID " + user_id);
            }
        }

        return updatedUserPermission;
    }

    //Updates a user. Returns the updated user on success, null otherwise.
    public User updateUser(User userToUpdate) throws SQLException {
        logger.info("Updating information for user ID " + userToUpdate.getId());
        User oldUser = getUser(userToUpdate.getId());

        if(oldUser == null){
            logger.warn("Failed to update user ID " + userToUpdate.getId() + " (User ID does not exist)");
            return null;
        }

        User validUserForUpdate = verifyUserForUpdate(oldUser, userToUpdate);

        Connection conn = initConnection();
        PreparedStatement ps = conn.prepareStatement(queries.get("updateUserSQL"));
        ps.setString(1, validUserForUpdate.getFirst_name());
        ps.setString(2, validUserForUpdate.getLast_name());
        ps.setString(3, validUserForUpdate.getUsername());
        ps.setString(4, validUserForUpdate.getEmail());
        ps.setInt(5, validUserForUpdate.getRole_id());
        ps.setString(6, validUserForUpdate.getImg());
        ps.setInt(7, validUserForUpdate.getLast_modified_by());
        ps.setTimestamp(8, validUserForUpdate.getLast_modified_date());
        ps.setInt(9, validUserForUpdate.getId());
        int rowsUpdated = ps.executeUpdate();

        closeConnection(conn);

        if(rowsUpdated <= 0){
            logger.error("Failed to update user ID " + validUserForUpdate.getId());
            validUserForUpdate = null;
        }
        else{
            //set password to NULL on response
            validUserForUpdate.setPassword(null);

            //Update user permissions if role ID was updated
            if(validUserForUpdate.getRole_id() != oldUser.getRole_id()){
                UserPermission up = updateUserRole(validUserForUpdate.getId(), validUserForUpdate.getRole_id());
                if(up == null){
                    logger.error("Failed to update permissions for user ID " + validUserForUpdate.getId() + ". Reverting changes...");

                    //Revert changes previously made to user
                    conn = initConnection();

                    ps = conn.prepareStatement(queries.get("updateUserSQL"));
                    ps.setString(1, oldUser.getFirst_name());
                    ps.setString(2, oldUser.getLast_name());
                    ps.setString(3, oldUser.getUsername());
                    ps.setString(4, oldUser.getEmail());
                    ps.setInt(5, oldUser.getRole_id());
                    ps.setString(6, oldUser.getImg());
                    ps.setInt(7, oldUser.getLast_modified_by());
                    ps.setTimestamp(8, oldUser.getLast_modified_date());
                    ps.setInt(9, oldUser.getId());
                    rowsUpdated = ps.executeUpdate();

                    if(rowsUpdated <= 0){
                        logger.error("Failed to revert user update for user ID " + validUserForUpdate.getId() + "!");
                    }

                    validUserForUpdate = null;
                }
            }
        }

        closeConnection(conn);
        return validUserForUpdate;
    }

    //Deletes a user. Returns the deleted user on success, null otherwise.
    public User deleteUser(int user_id) throws SQLException {
        logger.info("Deleting user ID " + user_id);

        Timestamp currentTimestamp = new java.sql.Timestamp(System.currentTimeMillis());
        User userToDelete = getUser(user_id);

        if(userToDelete == null){
            logger.warn("Failed to update user ID " + user_id + " (User ID does not exist)");
            return null;
        }

        //Set deleted date
        userToDelete.setDeleted_date(currentTimestamp);

        Connection conn = initConnection();
        PreparedStatement ps = conn.prepareStatement(queries.get("deleteUserSQL"));
        ps.setTimestamp(1, userToDelete.getDeleted_date());
        ps.setInt(2, userToDelete.getId());
        int rowsDeleted = ps.executeUpdate();

        if(rowsDeleted <= 0){
            logger.error("Failed to delete user ID " + userToDelete.getId());
            return null;
        }
        else{
            logger.info("Successfully deleted user ID " + userToDelete.getId());
            return userToDelete;
        }
    }

    //Updates a user password. Returns the updated user on success, null otherwise.
    public boolean updateUserPassword(User userToUpdate) throws SQLException {
        logger.info("Updating user password for user ID " + userToUpdate.getId());

        if(userToUpdate.getId() == 0){
            logger.warn("User ID was not provided on request to update user password.");
            return false;
        }

        if(!userIdExists(userToUpdate.getId())){
            logger.warn("User ID provided in request to update user password does not exist (User ID " + userToUpdate.getId() + ")");
            return false;
        }

        if(userToUpdate.getPassword() == null){
            logger.warn("Password was not provided on request to update user password.");
            return false;
        }

        if(userToUpdate.getLast_modified_by() == 0){
            userToUpdate.setLast_modified_by(userToUpdate.getId());
        }

        if(userToUpdate.getLast_modified_date() == null){
            Timestamp currentTimestamp = new java.sql.Timestamp(System.currentTimeMillis());
            userToUpdate.setLast_modified_date(currentTimestamp);
        }

        String hashedPassword = BCrypt.hashpw(userToUpdate.getPassword(), BCrypt.gensalt(12));

        Connection conn = initConnection();
        PreparedStatement ps = conn.prepareStatement(queries.get("updateUserPassword"));
        ps.setString(1, hashedPassword);
        ps.setInt(2, userToUpdate.getLast_modified_by());
        ps.setTimestamp(3, userToUpdate.getLast_modified_date());
        ps.setInt(4, userToUpdate.getId());
        int rowsUpdated = ps.executeUpdate();

        closeConnection(conn);

        if(rowsUpdated <= 0){
            logger.error("Password update failed for user ID " + userToUpdate.getId());
            return false;
        }
        else{
            logger.info("Password update successful for user ID " + userToUpdate.getId());
            return true;
        }
    }

    //Creates a password reset request for a given user ID. Returns the created PasswordReset on success, null otherwise.
    public PasswordReset createPasswordReset(int user_id) throws SQLException {
        logger.info("Creating password reset token for user ID " + user_id);

        if(user_id == 0){
            logger.info("User ID not provided in request");
            return null;
        }

        if(!userIdExists(user_id)){
            logger.warn("User ID provided in request to update user password does not exist (User ID " + user_id + ")");
            return null;
        }

        String newResetToken = PasswordReset.generateNewResetToken();

        while(getPasswordReset(newResetToken) != null){
            newResetToken = PasswordReset.generateNewResetToken();
        }

        Connection conn = initConnection();

        PreparedStatement ps = conn.prepareStatement(queries.get("createPasswordReset"));
        ps.setString(1, newResetToken);
        ps.setInt(2, user_id);
        int rowsCreated = ps.executeUpdate();

        closeConnection(conn);

        if(rowsCreated <= 0){
            logger.error("Failed to create password reset record for user ID " + user_id);
            return null;
        }
        else{
            logger.info("Successfully created new password reset token '" + newResetToken + "' for user ID " + user_id);
            PasswordReset createdPasswordReset = getPasswordReset(newResetToken);
            if(createdPasswordReset != null){
                return createdPasswordReset;
            }
            else{
                logger.error("Unexpected error getting password reset request with reset token '" + newResetToken + "'");
                return null;
            }
        }
    }

    //Validates a password reset token. Returns the valid PasswordReset on success, null otherwise.
    public PasswordReset validatePasswordReset(String resetToken) throws SQLException {
        logger.info("Validating password reset request for reset token '" + resetToken + "'");

        PasswordReset passwordResetToValidate = getPasswordReset(resetToken);

        if(passwordResetToValidate == null){
            logger.warn("Password rest token '" + resetToken + "' does not exist.");
            return null;
        }

        if(verifyPasswordReset(passwordResetToValidate)){
            logger.info("Password reset token '" + passwordResetToValidate.getReset_token() + "' is valid.");
            return passwordResetToValidate;
        }
        else{
            logger.warn("Password reset token '" + passwordResetToValidate.getReset_token() + "' is invalid.");
            return null;
        }
    }

    //Completes a password reset request and updates the password for the user associated with the reset token. Returns the completed PasswordReset on success, null otherwise.
    public PasswordReset completePasswordReset(PasswordReset passwordResetToComplete) throws SQLException {
        logger.info("Completing password reset token '" + passwordResetToComplete.getReset_token() + "'...");

        if(passwordResetToComplete.getReset_token() == null){
            logger.info("Reset token not provided in request");
            return null;
        }

        if(passwordResetToComplete.getPassword() == null){
            logger.info("Password not provided in request");
            return null;
        }

        PasswordReset completePasswordReset = getPasswordReset(passwordResetToComplete.getReset_token());

        if(completePasswordReset == null){
            logger.info("Unable to complete password reset. Reset token '" + passwordResetToComplete.getReset_token() + "' does not exist.");
            return null;
        }

        if(!verifyPasswordReset(completePasswordReset)){
            logger.info("Unable to complete password reset. Reset token '" + passwordResetToComplete.getReset_token() + "' is invalid.");
            return null;
        }

        User userForReset = new User();
        userForReset.setId(completePasswordReset.getUser_id());
        userForReset.setPassword(passwordResetToComplete.getPassword());

        if(!updateUserPassword(userForReset)){
            logger.error("Unable to complete password reset. Failure to update password for user ID " + userForReset.getId());
            return null;
        }

        Connection conn = initConnection();

        PreparedStatement ps = conn.prepareStatement(queries.get("completePasswordReset"));
        ps.setString(1, passwordResetToComplete.getReset_token());
        int rowsUpdated = ps.executeUpdate();

        closeConnection(conn);

        if(rowsUpdated <= 0){
            logger.error("Failed to mark password reset token '" + passwordResetToComplete.getReset_token() + "' as complete");
        }
        else{
            logger.info("Successfully marked password reset token '" + passwordResetToComplete.getReset_token() + "' as complete.");
            completePasswordReset.setCompleted(1);
        }

        return completePasswordReset;
    }

    //Creates an email verify token for a given user ID. Returns the created Verify on success, null otherwise.
    public Verify createVerifyEmailToken(int user_id) throws SQLException {
        logger.info("Creating verify email token for user ID " + user_id);
        User currentUser;

        if(user_id == 0){
            logger.info("User ID not provided in request");
            return null;
        }

        currentUser = getUser(user_id);

        if(currentUser == null){
            logger.warn("Unable to get information for user ID " + user_id);
            return null;
        }

        String newVerifyToken = Verify.generateNewVerifyToken();

        while(getVerifyToken(newVerifyToken) != null){
            newVerifyToken = Verify.generateNewVerifyToken();
        }

        Connection conn = initConnection();

        PreparedStatement ps = conn.prepareStatement(queries.get("createVerifyEmail"));
        ps.setString(1, newVerifyToken);
        ps.setInt(2, user_id);
        int rowsCreated = ps.executeUpdate();

        closeConnection(conn);

        if(rowsCreated <= 0){
            logger.error("Failed to create verify token for user ID " + user_id);
            return null;
        }
        else{
            logger.info("Successfully created new verify token '" + newVerifyToken + "' for user ID " + user_id);
            Verify createdVerifyToken = getVerifyToken(newVerifyToken);
            if(createdVerifyToken != null){
                return createdVerifyToken;
            }
            else{
                logger.error("Unexpected error getting password reset request with reset token '" + newVerifyToken + "'");
                return null;
            }
        }
    }

    //Completes a password reset request and updates the password for the user associated with the reset token. Returns the completed PasswordReset on success, null otherwise.
    public Verify completeVerifyEmailToken(Verify verifyToComplete) throws SQLException {
        logger.info("Completing password reset token '" + verifyToComplete.getVerify_token() + "'...");

        if(verifyToComplete.getVerify_token() == null){
            logger.info("Verify token not provided in request");
            return null;
        }

        Verify completeVerifyToken = getVerifyToken(verifyToComplete.getVerify_token());

        if(completeVerifyToken == null){
            logger.info("Unable to complete verify token. Verify token '" + verifyToComplete.getVerify_token() + "' does not exist.");
            return null;
        }

        if(!verifyEmailToken(completeVerifyToken)){
            logger.info("Unable to complete verify email token. Verify token '" + verifyToComplete.getVerify_token() + "' is invalid.");
            return null;
        }

        Connection conn = initConnection();

        PreparedStatement ps = conn.prepareStatement(queries.get("completeVerifyToken"));
        ps.setString(1, verifyToComplete.getVerify_token());
        int rowsUpdated = ps.executeUpdate();

        closeConnection(conn);

        if(rowsUpdated <= 0){
            logger.error("Failed to mark verify token '" + verifyToComplete.getVerify_token() + "' as complete");
        }
        else{
            logger.info("Successfully marked password reset token '" + verifyToComplete.getVerify_token() + "' as complete.");
            completeVerifyToken.setCompleted(1);
        }

        return completeVerifyToken;
    }

    //Creates a host. Returns the created host on success, null otherwise.
    public Host createHost(Host hostToCreate) throws SQLException {
        logger.info("Creating host with name " + hostToCreate.getFirst_name() + " " + hostToCreate.getLast_name() + "...");

        if(!verifyHost(hostToCreate)){
            logger.warn("Unable to create host. Verification has failed.");
            return null;
        }

        Connection conn = initConnection();
        PreparedStatement ps = conn.prepareStatement(queries.get("createHostSQL"), Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, hostToCreate.getFirst_name());
        ps.setString(2, hostToCreate.getLast_name());
        ps.setString(3, hostToCreate.getBio());
        ps.setString(4, hostToCreate.getImg());
        ps.setString(5, hostToCreate.getFacebook_url());
        ps.setString(6, hostToCreate.getInstagram_url());
        ps.setString(7, hostToCreate.getTwitter_url());
        ps.setString(8, hostToCreate.getTiktok_url());
        ps.setInt(9, hostToCreate.getLast_modified_by());
        ps.setTimestamp(10, hostToCreate.getLast_modified_date());

        int rowsInserted = ps.executeUpdate();

        if(rowsInserted <= 0){
            logger.error("Unable to create host with name '" + hostToCreate.getFirst_name() + " " + hostToCreate.getLast_name() + "'");
            closeConnection(conn);
            return null;
        }
        else {
            logger.info("Successfully created host with name '" + hostToCreate.getFirst_name() + " " + hostToCreate.getLast_name() + "'");
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                hostToCreate.setId(rs.getInt(1));
            }

            closeConnection(conn);
        }

        return hostToCreate;
    }

    //Gets all host data given a host ID. Returns the found host on success, null otherwise.
    public Host getHostById(int hostId) throws SQLException {
        Host foundHost;
        logger.info("Getting host by ID (" + hostId + ")...");

        if(hostId == 0){
            logger.warn("Host ID not provided in request");
            return null;
        }

        Connection conn = initConnection();
        PreparedStatement ps = conn.prepareStatement(queries.get("getHostByIDSQL"));
        ps.setInt(1, hostId);
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            logger.info("Successfully found information for Host ID " + hostId);
            foundHost = new Host(hostId, rs.getString("first_name"), rs.getString("last_name"), rs.getString("bio"), rs.getString("img"), rs.getString("facebook_url"), rs.getString("instagram_url"), rs.getString("twitter_url"), rs.getString("tiktok_url"), rs.getInt("last_modified_by"), rs.getTimestamp("last_modified_date"), rs.getTimestamp("deleted_date"));
            closeConnection(conn);
            return foundHost;
        }
        else{
            logger.warn("Could not find Host ID " + hostId);
            closeConnection(conn);
            return null;
        }
    }

    //Gets all host data given a host first & last name. Returns the found host on success, null otherwise.
    public Host getHostByName(String first_name, String last_name) throws SQLException {
        Host foundHost;
        logger.info("Getting host by name (" + first_name + " " + last_name + ")...");

        if(first_name == null){
            logger.warn("First name not provided in request");
            return null;
        }

        if(last_name == null){
            logger.warn("Last name not provided in request");
            return null;
        }

        Connection conn = initConnection();
        PreparedStatement ps = conn.prepareStatement(queries.get("getHostByNameSQL"));
        ps.setString(1, first_name);
        ps.setString(2, last_name);
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            logger.info("Successfully found information for host " + first_name + " " + last_name);
            foundHost = new Host(rs.getInt("id"), first_name, last_name, rs.getString("bio"), rs.getString("img"), rs.getString("facebook_url"), rs.getString("instagram_url"), rs.getString("twitter_url"), rs.getString("tiktok_url"), rs.getInt("last_modified_by"), rs.getTimestamp("last_modified_date"), rs.getTimestamp("deleted_date"));
            closeConnection(conn);
            return foundHost;
        }
        else{
            logger.warn("Could not find host name '" + first_name + " " + last_name + "'");
            closeConnection(conn);
            return null;
        }
    }

    //Gets all hosts (limit 10). Returns a list of hosts on success, empty list otherwise.
    public List<Host> getAllHosts() throws SQLException {
        List<Host> hostList = new ArrayList<Host>();

        Connection conn = initConnection();
        PreparedStatement ps = conn.prepareStatement(queries.get("getAllHostSQL"));
        ResultSet rs = ps.executeQuery();

        while(rs.next()){
            hostList.add(new Host(rs.getInt("id"), rs.getString("first_name"), rs.getString("last_name"), rs.getString("bio"), rs.getString("img"), rs.getString("facebook_url"), rs.getString("instagram_url"), rs.getString("twitter_url"), rs.getString("tiktok_url"), rs.getInt("last_modified_by"), rs.getTimestamp("last_modified_date"), rs.getTimestamp("deleted_date")));
        }

        logger.info("Successfully found " + hostList.size() + " hosts for GetAllHosts request.");

        return hostList;
    }

    //Updates a host given a host ID. Returns the updated host on success, null otherwise.
    public Host updateHost(Host hostToUpdate) throws SQLException {
        logger.info("Updating Host ID " + hostToUpdate.getId() + "...");

        if(hostToUpdate.getId() == 0){
            logger.warn("Host ID not provided in request.");
            return null;
        }

        if(!verifyHost(hostToUpdate)){
            logger.warn("Verification failed for update on host ID " + hostToUpdate.getId());
            return null;
        }

        Connection conn = initConnection();
        PreparedStatement ps  = conn.prepareStatement(queries.get("updateHostSQL"));
        ps.setString(1, hostToUpdate.getFirst_name());
        ps.setString(2, hostToUpdate.getLast_name());
        ps.setString(3, hostToUpdate.getBio());
        ps.setString(4, hostToUpdate.getImg());
        ps.setString(5, hostToUpdate.getFacebook_url());
        ps.setString(6, hostToUpdate.getInstagram_url());
        ps.setString(7, hostToUpdate.getTwitter_url());
        ps.setString(8, hostToUpdate.getTiktok_url());
        ps.setInt(9, hostToUpdate.getLast_modified_by());
        ps.setTimestamp(10, hostToUpdate.getLast_modified_date());
        ps.setInt(11, hostToUpdate.getId());
        int rowsUpdated = ps.executeUpdate();

        closeConnection(conn);

        if(rowsUpdated <= 0){
            logger.warn("Failed to update host ID " + hostToUpdate.getId());
            return null;
        }
        else{
            logger.info("Successfully updated host ID " + hostToUpdate.getId());
            return hostToUpdate;
        }
    }

    //Deletes a host given a host ID. Returns the deleted host on success, null otherwise
    public Host deleteHost(int hostToDelete) throws SQLException {
        logger.info("Deleting Host ID " + hostToDelete + "...");
        Timestamp currentTimestamp = new java.sql.Timestamp(System.currentTimeMillis());

        if(hostToDelete == 0){
            logger.warn("Host ID not provided in request.");
            return null;
        }

        Host deletedHost = getHostById(hostToDelete);

        if(deletedHost == null){
            logger.warn("Host ID " + hostToDelete + " does not exist.");
            return null;
        }

        Connection conn = initConnection();
        PreparedStatement ps = conn.prepareStatement(queries.get("deleteHostSQL"));
        ps.setTimestamp(1, currentTimestamp);
        ps.setInt(2, hostToDelete);
        int rowsDeleted = ps.executeUpdate();

        closeConnection(conn);

        if(rowsDeleted <= 0){
            logger.warn("Failed to delete host ID " + hostToDelete);
            return null;
        }
        else{
            logger.info("Successfully deleted host ID " + hostToDelete);
            return deletedHost;
        }
    }

    //Creates a schedule. Returns the created schedule on success, null otherwise.
    public Schedule createSchedule(Schedule scheduleToCreate) throws SQLException {
        logger.info("Creating schedule with name '" + scheduleToCreate.getTitle() + "' for host ID " + scheduleToCreate.getHost_id());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Schedule.SCHEDULE_DATE_TIME_FORMAT);
        LocalDateTime beginTime;
        LocalDateTime endTime;

        //Make sure Recurring value is in UPPERCASE before verifying
        scheduleToCreate.setRecurring(scheduleToCreate.getRecurring().toUpperCase());

        if(!verifySchedule(scheduleToCreate)){
            logger.warn("Verification failed for schedule with title '" + scheduleToCreate.getTitle() + "'");
            return null;
        }

        //Date formats verified in verifySchedule method above. A format exception will not be thrown here.
        beginTime = LocalDateTime.parse(scheduleToCreate.getBegin(), formatter);
        endTime = LocalDateTime.parse(scheduleToCreate.getEnd(), formatter);

        Connection conn = initConnection();
        PreparedStatement ps = conn.prepareStatement(queries.get("createSchedule"), Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, scheduleToCreate.getTitle());
        ps.setInt(2, scheduleToCreate.getHost_id());
        ps.setInt(3, scheduleToCreate.getEvent_id());
        ps.setString(4, scheduleToCreate.getRecurring());
        ps.setTimestamp(5, Timestamp.valueOf(beginTime));
        ps.setTimestamp(6, Timestamp.valueOf(endTime));
        ps.setInt(7, scheduleToCreate.getLast_modified_by());
        ps.setTimestamp(8, scheduleToCreate.getLast_modified_date());
        int rowsCreated = ps.executeUpdate();

        if(rowsCreated <= 0){
            logger.error("Failed to create schedule with title '" + scheduleToCreate.getTitle() + "'");
            return null;
        }
        else{
            ResultSet rs = ps.getGeneratedKeys();
            if(rs.next()){
                int createdScheduleId = rs.getInt(1);
                scheduleToCreate.setId(createdScheduleId);
            }
            else{
                logger.error("Unable to get generated keys for Schedule (Title: '" + scheduleToCreate.getTitle() + "')");
            }
        }

        logger.info("Successfully created Schedule with ID " + scheduleToCreate.getId() + "('" + scheduleToCreate.getTitle() + "')");
        closeConnection(conn);
        return scheduleToCreate;
    }

    //Updates a schedule given an ID and schedule fields. Returns the updated schedule on success, null otherwise.
    public Schedule editSchedule(Schedule scheduleToEdit){
        return null;
    }

    //Gets a schedule given a schedule ID. Returns the found schedule on success, null otherwise.
    public Schedule getSchedule(int scheduleToGet){
        return null;
    }

    //Gets all schedules which begin or end on a given day. Returns the list of found schedules on success, empty list otherwise.
    public List<Schedule> getScheduleForDay(Timestamp dayToSearch){
        return null;
    }

    //Gets all schedules for a given host ID. Returns the list of found schedules on success, null otherwise.
    public List<Schedule> getScheduleForHost(int hostId){
        return null;
    }

    //Deletes a schedule given a schedule ID. Returns the deleted schedule on success, null otherwise.
    public Schedule deleteSchedule(int scheduleToDelete){
        return null;
    }

    //Creates a reaction. Returns the created reaction on success, null otherwise.
    public Reaction createReaction(Reaction reactionToCreate) throws SQLException {
        logger.info("Creating reaction for post ID " + reactionToCreate.getPost_id() + " by user ID " + reactionToCreate.getUser_id() + "...");

        if(!verifyReaction(reactionToCreate, true)){
            logger.warn("Verification failed for reaction by user ID " + reactionToCreate.getUser_id() + " on post ID " + reactionToCreate.getPost_id() + " (Post Type: '" + reactionToCreate.getPost_type() + "')");
            return null;
        }

        Connection conn = initConnection();
        PreparedStatement ps = conn.prepareStatement(queries.get("createReaction"), Statement.RETURN_GENERATED_KEYS);
        ps.setInt(1, reactionToCreate.getUser_id());
        ps.setString(2, String.valueOf(reactionToCreate.getPost_type()));
        ps.setInt(3, reactionToCreate.getPost_id());
        ps.setString(4, String.valueOf(reactionToCreate.getReaction_type()));
        ps.setInt(5, reactionToCreate.getLast_modified_by());
        ps.setTimestamp(6, reactionToCreate.getLast_modified_date());
        int rowsCreated = ps.executeUpdate();

        if(rowsCreated <= 0){
            logger.error("Failed to create reaction by user ID " + reactionToCreate.getUser_id() + " on post ID " + reactionToCreate.getPost_id() + " (Post Type: '" + reactionToCreate.getPost_type() + "')");
            return null;
        }
        else{
            ResultSet rs = ps.getGeneratedKeys();
            if(rs.next()){
                int createdReactionId = rs.getInt(1);
                reactionToCreate.setId(createdReactionId);
            }
            else{
                logger.error("Unable to get generated keys for reaction by user ID " + reactionToCreate.getUser_id() + " on post ID " + reactionToCreate.getPost_id() + " (Post Type: '" + reactionToCreate.getPost_type() + "')");
            }
        }

        logger.info("Successfully created reaction by user ID " + reactionToCreate.getUser_id() + " on post ID " + reactionToCreate.getPost_id() + " (Post Type: '" + reactionToCreate.getPost_type() + "')");
        closeConnection(conn);
        return reactionToCreate;
    }

    //Gets a reaction given a reaction ID. Returns the found reaction on success, null otherwise.
    public Reaction getReaction(int reactionId) throws SQLException {
        logger.info("Getting info for reaction ID " + reactionId + "...");
        Reaction foundReaction;

        Connection conn = initConnection();
        PreparedStatement ps = conn.prepareStatement(queries.get("getReaction"));
        ps.setInt(1, reactionId);
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            foundReaction = new Reaction(reactionId, rs.getInt("user_id"), rs.getString("post_type").toCharArray()[0], rs.getInt("post_id"), rs.getString("reaction_type").toCharArray()[0], rs.getInt("last_modified_by"), rs.getTimestamp("last_modified_date"), rs.getTimestamp("deleted_date"));
        }
        else{
            return null;
        }

        closeConnection(conn);
        return foundReaction;
    }

    //Gets the number of reactions on a given post by reaction type. Returns zero on error, count otherwise. (I know this is shitty error handling, will fix if needed)
    public int getReactionCount(int postId, char postType, char reactionType) throws SQLException {
        logger.info("Getting reaction count for post ID " + postId + " (Post type: " + postType + " , Reaction type: " + reactionType + ")...");
        int reactionCount = 0;

        //Reaction object for verification
        Reaction reactionToVerify = new Reaction(0, 0, postType, postId, reactionType, 0, null, null);

        if(!verifyReaction(reactionToVerify, false)){
            logger.error("Verification failed while getting reaction count for post ID " + postId + " (Post Type: '" + postType + "', " + "Reaction Type: " + reactionType + ")");
            return 0;
        }

        Connection conn = initConnection();
        PreparedStatement ps = conn.prepareStatement(queries.get("getReactionCount"));
        ps.setInt(1, postId);
        ps.setString(2, String.valueOf(postType));
        ps.setString(3, String.valueOf(reactionType));
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            reactionCount = rs.getInt(1);
        }
        else{
            logger.error("SEVERE ERROR: No results returned for query expecting results -> (getReactionCount)");
            return 0;
        }

        closeConnection(conn);
        return reactionCount;
    }

    //Deletes a reaction given a reaction ID. Returns the deleted reaction on success, null otherwise.
    public Reaction deleteReaction(Reaction reactionToDelete) throws SQLException {
        logger.info("Deleting reaction ID " + reactionToDelete.getId());
        Reaction reactionData = getReaction(reactionToDelete.getId());

        if(reactionData != null){
            Connection conn = initConnection();
            PreparedStatement ps = conn.prepareStatement(queries.get("deleteReaction"));
            ps.setInt(1, reactionToDelete.getLast_modified_by());
            ps.setTimestamp(2, reactionToDelete.getLast_modified_date());
            ps.setInt(3, reactionToDelete.getId());
            int rowsDeleted = ps.executeUpdate();

            if(rowsDeleted <= 0){
                logger.error("Failed to delete reaction ID " + reactionToDelete.getId());
                return null;
            }

            closeConnection(conn);

            reactionData.setLast_modified_by(reactionToDelete.getLast_modified_by());
            reactionData.setLast_modified_date(reactionToDelete.getLast_modified_date());
            reactionData.setDeleted_date(reactionToDelete.getLast_modified_date());
        }
        else{
            logger.warn("Reaction ID " + reactionToDelete.getId() + " does not exist");
            return null;
        }

        return reactionData;
    }

    //Creates a given comment. Returns the created comment on success, null otherwise.
    public Comment createComment(Comment commentToCreate) throws SQLException {
        logger.info("Creating reaction for post ID " + commentToCreate.getPost_id() + " by user ID " + commentToCreate.getUser_id() + "...");

        if(!verifyComment(commentToCreate)){
            logger.warn("Verification failed for comment by user ID " + commentToCreate.getUser_id());
            return null;
        }

        Connection conn = initConnection();
        PreparedStatement ps = conn.prepareStatement(queries.get("createComment"), Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, String.valueOf(commentToCreate.getPost_type()));
        ps.setInt(2, commentToCreate.getPost_id());
        ps.setInt(3, commentToCreate.getUser_id());
        ps.setString(4, commentToCreate.getContent());
        ps.setInt(5, commentToCreate.getReply_to());
        ps.setTimestamp(6, commentToCreate.getCreated_date());
        ps.setInt(7, commentToCreate.getLast_modified_by());
        ps.setTimestamp(8, commentToCreate.getLast_modified_date());
        int rowsCreated = ps.executeUpdate();

        if(rowsCreated <= 0){
            logger.error("Failed to create comment by user ID " + commentToCreate.getUser_id() + " on post ID " + commentToCreate.getPost_id() + " (Post Type: '" + commentToCreate.getPost_type() + "')");
            return null;
        }
        else{
            ResultSet rs = ps.getGeneratedKeys();
            if(rs.next()){
                int createdCommentId = rs.getInt(1);
                commentToCreate.setId(createdCommentId);
            }
            else{
                logger.error("Unable to get generated keys for comment by user ID " + commentToCreate.getUser_id() + " on post ID " + commentToCreate.getPost_id() + " (Post Type: '" + commentToCreate.getPost_type() + "')");
            }
        }

        logger.info("Successfully created comment by user ID " + commentToCreate.getUser_id() + " on post ID " + commentToCreate.getPost_id() + " (Post Type: '" + commentToCreate.getPost_type() + "')");
        closeConnection(conn);
        return commentToCreate;
    }

    //Gets all comments on a given post. Returns a list of comments on success, null otherwise.
    public List<Comment> getCommentsForPost(int postId, char postType) throws SQLException {
        logger.info("Getting all comments for post ID " + postId + " (Post Type: " + postType + ")...");
        List<Comment> commentList = new ArrayList<Comment>();

        if(!postIdExists(postId, postType)){
            logger.warn("Post ID does not exist or does not match post type (Post ID: " + postId + ", Post Type: " + postType + ")");
            return null;
        }

        Connection conn = initConnection();
        PreparedStatement ps = conn.prepareStatement(queries.get("getCommentsForPost"));
        ps.setInt(1, postId);
        ps.setString(2, String.valueOf(postType));
        ResultSet rs = ps.executeQuery();

        while(rs.next()){
            Comment tmpComment = new Comment(rs.getInt("id"), postType, postId, rs.getInt("user_id"), rs.getString("content"), rs.getInt("reply_to"), rs.getTimestamp("created_date"), rs.getInt("last_modified_by"), rs.getTimestamp("last_modified_date"), rs.getTimestamp("deleted_date"));
            commentList.add(tmpComment);
        }

        closeConnection(conn);
        return commentList;
    }

    //Gets a comment given a comment ID. Returns the found comment on success, null otherwise.
    public Comment getComment(int commentId) throws SQLException {
        logger.info("Getting data for comment ID " + commentId);
        Comment foundComment;

        //Yet again, a funky way of determining if a comment exists. Should probably be improved at some point.
        if(!postIdExists(commentId, Reaction.POST_TYPE_COMMENT)){
            logger.warn("Comment ID " + commentId + " does not exist.");
            return null;
        }

        Connection conn = initConnection();
        PreparedStatement ps = conn.prepareStatement(queries.get("getComment"));
        ps.setInt(1, commentId);
        ResultSet rs = ps.executeQuery();

        if(rs.next()){
            foundComment = new Comment(commentId, rs.getString("post_type").toCharArray()[0], rs.getInt("post_id"), rs.getInt("user_id"), rs.getString("content"), rs.getInt("reply_to"), rs.getTimestamp("created_date"), rs.getInt("last_modified_by"), rs.getTimestamp("last_modified_date"), null);
        }
        else{
            logger.warn("Comment ID " + commentId + " does not exist.");
            return null;
        }

        closeConnection(conn);
        return foundComment;
    }

    //Deletes a given comment. Returns the deleted comment on success, null otherwise.
    public Comment deleteComment(Comment commentToDelete) throws SQLException {
        logger.info("Deleting comment ID " + commentToDelete.getId() + "...");
        Comment commentData = getComment(commentToDelete.getId());

        if(commentData == null){
            logger.warn("Comment with ID " + commentToDelete.getId() + " does not exist.");
            return null;
        }

        Connection conn = initConnection();
        PreparedStatement ps = conn.prepareStatement(queries.get("deleteComment"));
        ps.setInt(1, commentToDelete.getLast_modified_by());
        ps.setTimestamp(2, commentToDelete.getLast_modified_date());
        ps.setInt(3, commentToDelete.getId());
        int rowsDeleted = ps.executeUpdate();

        if(rowsDeleted <= 0){
            logger.error("Failed to delete comment ID " + commentToDelete.getId());
            return null;
        }

        closeConnection(conn);

        commentData.setLast_modified_by(commentToDelete.getLast_modified_by());
        commentData.setLast_modified_date(commentToDelete.getLast_modified_date());
        commentData.setDeleted_date(commentToDelete.getLast_modified_date());

        return commentData;
    }
    /*     END PUBLIC METHODS     */
}
