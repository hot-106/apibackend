package com.hot106.apibackend.objects;

import java.sql.Timestamp;

public class News {

    private int id;
    private String title;
    private String img;
    private String content;
    private Timestamp created_date;
    private int last_modified_by;
    private Timestamp last_modified_date;
    private Timestamp deleted_date;

    public News(int id, String title, String img, String content, Timestamp created_date, int last_modified_by, Timestamp last_modified_date, Timestamp deleted_date) {
        this.id = id;
        this.title = title;
        this.img = img;
        this.content = content;
        this.created_date = created_date;
        this.last_modified_by = last_modified_by;
        this.last_modified_date = last_modified_date;
        this.deleted_date = deleted_date;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getImg() {
        return img;
    }

    public String getContent() {
        return content;
    }

    public Timestamp getCreated_date() {
        return created_date;
    }

    public int getLast_modified_by() {
        return last_modified_by;
    }

    public Timestamp getLast_modified_date() {
        return last_modified_date;
    }

    public Timestamp getDeleted_date() {
        return deleted_date;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setCreated_date(Timestamp created_date) {
        this.created_date = created_date;
    }

    public void setLast_modified_by(int last_modified_by) {
        this.last_modified_by = last_modified_by;
    }

    public void setLast_modified_date(Timestamp last_modified_date) {
        this.last_modified_date = last_modified_date;
    }

    public void setDeleted_date(Timestamp deleted_date) {
        this.deleted_date = deleted_date;
    }
}
