package com.hot106.apibackend.objects;

import java.sql.Timestamp;

public class Session {

    private String session_id;
    private int user_id;
    private Timestamp created_at;
    private String last_action;
    private Timestamp last_action_at;
    private String last_action_ip;
    private Timestamp session_ended_at;

    public static final int SESSION_LAST_ACTION_EXPIRY_TIME = 3600; //1 hour
    public static final int SESSION_CREATE_EXPIRY_TIME = 28800; //8 hours
    public static final int SESSION_TOKEN_SIZE = 50;

    public Session() {
    }

    public Session(String session_id, int user_id, Timestamp created_at, String last_action, Timestamp last_action_at, String last_action_ip, Timestamp session_ended_at) {
        this.session_id = session_id;
        this.user_id = user_id;
        this.created_at = created_at;
        this.last_action = last_action;
        this.last_action_at = last_action_at;
        this.last_action_ip = last_action_ip;
        this.session_ended_at = session_ended_at;
    }

    public String getSession_id() {
        return session_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public String getLast_action() {
        return last_action;
    }

    public Timestamp getLast_action_at() {
        return last_action_at;
    }

    public String getLast_action_ip() {
        return last_action_ip;
    }

    public Timestamp getSession_ended_at() {
        return session_ended_at;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public void setLast_action(String last_action) {
        this.last_action = last_action;
    }

    public void setLast_action_at(Timestamp last_action_at) {
        this.last_action_at = last_action_at;
    }

    public void setLast_action_ip(String last_action_ip) {
        this.last_action_ip = last_action_ip;
    }

    public void setSession_ended_at(Timestamp session_ended_at) {
        this.session_ended_at = session_ended_at;
    }

    public static String generateNewSessionToken() {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxyz0123456789";
        StringBuilder sb = new StringBuilder(SESSION_TOKEN_SIZE);

        for (int i = 0; i < SESSION_TOKEN_SIZE; i++){
            sb.append(characters.charAt((int)(characters.length() * Math.random())));
        }

        return sb.toString();
    }
}
