package com.hot106.apibackend.objects;

import java.util.List;

public class Error {
    List<String> error_details;

    public Error(List<String> error_details) {
        this.error_details = error_details;
    }

    public List<String> getError_details() {
        return error_details;
    }
}
