package com.hot106.apibackend.objects;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public class GenericResponse<T> {

    private String status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM-dd-yyyy HH:mm:ss")
    private LocalDateTime time;
    private T response_data;

    public GenericResponse(String status, T object) {
        this.status = status;
        this.time = LocalDateTime.now();
        this.response_data = object;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public T getResponse_data() {
        return response_data;
    }

    public void setResponse_data(T response_data) {
        this.response_data = response_data;
    }
}
