package com.hot106.apibackend.objects;

import java.sql.Timestamp;

public class Reaction {

    private int id;
    private int user_id;
    private char post_type;
    private int post_id;
    private char reaction_type;
    private int last_modified_by;
    private Timestamp last_modified_date;
    private Timestamp deleted_date;

    public static final char POST_TYPE_COMMENT = 'C';
    public static final char POST_TYPE_EVENT = Comment.POST_TYPE_EVENT;
    public static final char POST_TYPE_NEWS = Comment.POST_TYPE_NEWS;
    public static final char REACTION_TYPE_LIKE = 'L';
    public static final char REACTION_TYPE_DISLIKE = 'D';

    public Reaction(int id, int user_id, char post_type, int post_id, char reaction_type, int last_modified_by, Timestamp last_modified_date, Timestamp deleted_date) {
        this.id = id;
        this.user_id = user_id;
        this.post_type = post_type;
        this.post_id = post_id;
        this.reaction_type = reaction_type;
        this.last_modified_by = last_modified_by;
        this.last_modified_date = last_modified_date;
        this.deleted_date = deleted_date;
    }

    public int getId() {
        return id;
    }

    public int getUser_id() {
        return user_id;
    }

    public char getPost_type() {
        return post_type;
    }

    public int getPost_id() {
        return post_id;
    }

    public char getReaction_type() {
        return reaction_type;
    }

    public int getLast_modified_by() {
        return last_modified_by;
    }

    public Timestamp getLast_modified_date() {
        return last_modified_date;
    }

    public Timestamp getDeleted_date() {
        return deleted_date;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public void setPost_type(char post_type) {
        this.post_type = post_type;
    }

    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }

    public void setReaction_type(char reaction_type) {
        this.reaction_type = reaction_type;
    }

    public void setLast_modified_by(int last_modified_by) {
        this.last_modified_by = last_modified_by;
    }

    public void setLast_modified_date(Timestamp last_modified_date) {
        this.last_modified_date = last_modified_date;
    }

    public void setDeleted_date(Timestamp deleted_date) {
        this.deleted_date = deleted_date;
    }
}
