package com.hot106.apibackend.objects;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class Schedule {

    private int id;
    private String title;
    private int host_id;
    private int event_id;
    private String recurring;
    private String begin;
    private String end;
    private int last_modified_by;
    private Timestamp last_modified_date;
    private Timestamp deleted_date;

    public static final String SCHEDULE_RECURRING_DAILY = "DAILY";
    public static final String SCHEDULE_RECURRING_WEEKLY = "WEEKLY";
    public static final String SCHEDULE_RECURRING_MONTHLY = "MONTHLY";
    public static final String SCHEDULE_RECURRING_YEARLY = "YEARLY";
    public static final String SCHEDULE_DATE_TIME_FORMAT = "MM-dd-yyyy HH:mm:ss";

    public Schedule(int id, String title, int host_id, int event_id, String recurring, String begin, String end, int last_modified_by, Timestamp last_modified_date, Timestamp deleted_date) {
        this.id = id;
        this.title = title;
        this.host_id = host_id;
        this.event_id = event_id;
        this.recurring = recurring;
        this.begin = begin;
        this.end = end;
        this.last_modified_by = last_modified_by;
        this.last_modified_date = last_modified_date;
        this.deleted_date = deleted_date;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getHost_id() {
        return host_id;
    }

    public int getEvent_id() {
        return event_id;
    }

    public String getRecurring() {
        return recurring;
    }

    public String getBegin() {
        return begin;
    }

    public String getEnd() {
        return end;
    }

    public int getLast_modified_by() {
        return last_modified_by;
    }

    public Timestamp getLast_modified_date() {
        return last_modified_date;
    }

    public Timestamp getDeleted_date() {
        return deleted_date;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setHost_id(int host_id) {
        this.host_id = host_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public void setRecurring(String recurring) {
        this.recurring = recurring;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public void setLast_modified_by(int last_modified_by) {
        this.last_modified_by = last_modified_by;
    }

    public void setLast_modified_date(Timestamp last_modified_date) {
        this.last_modified_date = last_modified_date;
    }

    public void setDeleted_date(Timestamp deleted_date) {
        this.deleted_date = deleted_date;
    }
}
