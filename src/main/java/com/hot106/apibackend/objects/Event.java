package com.hot106.apibackend.objects;

import java.sql.Timestamp;

public class Event {

    private int id;
    private String title;
    private String img;
    private String detail;
    private Timestamp begin;
    private Timestamp end;
    private int last_modified_by;
    private Timestamp last_modified_date;
    private Timestamp deleted_date;

    public Event(int id, String title, String img, String detail, Timestamp begin, Timestamp end, int last_modified_by, Timestamp last_modified_date, Timestamp deleted_date) {
        this.id = id;
        this.title = title;
        this.img = img;
        this.detail = detail;
        this.begin = begin;
        this.end = end;
        this.last_modified_by = last_modified_by;
        this.last_modified_date = last_modified_date;
        this.deleted_date = deleted_date;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getImg() {
        return img;
    }

    public String getDetail() {
        return detail;
    }

    public Timestamp getBegin() {
        return begin;
    }

    public Timestamp getEnd() {
        return end;
    }

    public int getLast_modified_by() {
        return last_modified_by;
    }

    public Timestamp getLast_modified_date() {
        return last_modified_date;
    }

    public Timestamp getDeleted_date() {
        return deleted_date;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public void setBegin(Timestamp begin) {
        this.begin = begin;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }

    public void setLast_modified_by(int last_modified_by) {
        this.last_modified_by = last_modified_by;
    }

    public void setLast_modified_date(Timestamp last_modified_date) {
        this.last_modified_date = last_modified_date;
    }

    public void setDeleted_date(Timestamp deleted_date) {
        this.deleted_date = deleted_date;
    }
}
