package com.hot106.apibackend.objects;

public class UserPermission {

    private int user_id;
    private int create_comment;
    private int create_event;
    private int create_host;
    private int create_news;
    private int create_reaction;
    private int create_schedule;
    private int create_session;
    private int edit_user;
    private int verify_user;

    public UserPermission(int user_id) {
        this.user_id = user_id;
    }

    public UserPermission(int user_id, int create_comment, int create_event, int create_host, int create_news, int create_reaction, int create_schedule, int create_session, int edit_user, int verify_user) {
        this.user_id = user_id;
        this.create_comment = create_comment;
        this.create_event = create_event;
        this.create_host = create_host;
        this.create_news = create_news;
        this.create_reaction = create_reaction;
        this.create_schedule = create_schedule;
        this.create_session = create_session;
        this.edit_user = edit_user;
        this.verify_user = verify_user;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getCreate_comment() {
        return create_comment;
    }

    public void setCreate_comment(int create_comment) {
        this.create_comment = create_comment;
    }

    public int getCreate_event() {
        return create_event;
    }

    public void setCreate_event(int create_event) {
        this.create_event = create_event;
    }

    public int getCreate_host() {
        return create_host;
    }

    public void setCreate_host(int create_host) {
        this.create_host = create_host;
    }

    public int getCreate_news() {
        return create_news;
    }

    public void setCreate_news(int create_news) {
        this.create_news = create_news;
    }

    public int getCreate_reaction() {
        return create_reaction;
    }

    public void setCreate_reaction(int create_reaction) {
        this.create_reaction = create_reaction;
    }

    public int getCreate_schedule() {
        return create_schedule;
    }

    public void setCreate_schedule(int create_schedule) {
        this.create_schedule = create_schedule;
    }

    public int getCreate_session() {
        return create_session;
    }

    public void setCreate_session(int create_session) {
        this.create_session = create_session;
    }

    public int getEdit_user() {
        return edit_user;
    }

    public void setEdit_user(int edit_user) {
        this.edit_user = edit_user;
    }

    public int getVerify_user() {
        return verify_user;
    }

    public void setVerify_user(int verify_user) {
        this.verify_user = verify_user;
    }
}
