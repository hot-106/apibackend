package com.hot106.apibackend.objects;

import java.sql.Timestamp;

public class PasswordReset {

    private String reset_token;
    private int user_id;
    private String password;
    private Timestamp created_at;
    private int completed;

    public static final int RESET_TOKEN_EXPIRY_TIME = 600; //10 minutes
    public static final int RESET_TOKEN_SIZE = 50;

    public PasswordReset(String reset_token, int user_id, String password, Timestamp created_at, int completed) {
        this.reset_token = reset_token;
        this.user_id = user_id;
        this.password = password;
        this.created_at = created_at;
        this.completed = completed;
    }

    public String getReset_token() {
        return reset_token;
    }

    public void setReset_token(String reset_token) {
        this.reset_token = reset_token;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public int getCompleted() {
        return completed;
    }

    public void setCompleted(int completed) {
        this.completed = completed;
    }

    public static String generateNewResetToken() {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxyz0123456789";
        StringBuilder sb = new StringBuilder(RESET_TOKEN_SIZE);

        for (int i = 0; i < RESET_TOKEN_SIZE; i++){
            sb.append(characters.charAt((int)(characters.length() * Math.random())));
        }

        return sb.toString();
    }
}
