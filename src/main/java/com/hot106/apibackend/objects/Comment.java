package com.hot106.apibackend.objects;

import java.sql.Timestamp;

public class Comment {

    private int id;
    private char post_type;
    private int post_id;
    private int user_id;
    private String content;
    private int reply_to;
    private Timestamp created_date;
    private int last_modified_by;
    private Timestamp last_modified_date;
    private Timestamp deleted_date;

    public static final char POST_TYPE_EVENT = 'E';
    public static final char POST_TYPE_NEWS = 'N';

    public Comment(int id, char post_type, int post_id, int user_id, String content, int reply_to, Timestamp created_date,  int last_modified_by, Timestamp last_modified_date, Timestamp deleted_date) {
        this.id = id;
        this.post_type = post_type;
        this.post_id = post_id;
        this.user_id = user_id;
        this.content = content;
        this.reply_to = reply_to;
        this.created_date = created_date;
        this.last_modified_by = last_modified_by;
        this.last_modified_date = last_modified_date;
        this.deleted_date = deleted_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public char getPost_type() {
        return post_type;
    }

    public int getPost_id() {
        return post_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getContent() {
        return content;
    }

    public int getReply_to() {
        return reply_to;
    }

    public Timestamp getCreated_date() {
        return created_date;
    }

    public int getLast_modified_by() {
        return last_modified_by;
    }

    public Timestamp getLast_modified_date() {
        return last_modified_date;
    }

    public Timestamp getDeleted_date() {
        return deleted_date;
    }

    public void setPost_type(char post_type) {
        this.post_type = post_type;
    }

    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setReply_to(int reply_to) {
        this.reply_to = reply_to;
    }

    public void setCreated_date(Timestamp created_date) {
        this.created_date = created_date;
    }

    public void setLast_modified_by(int last_modified_by) {
        this.last_modified_by = last_modified_by;
    }

    public void setLast_modified_date(Timestamp last_modified_date) {
        this.last_modified_date = last_modified_date;
    }

    public void setDeleted_date(Timestamp deleted_date) {
        this.deleted_date = deleted_date;
    }
}
