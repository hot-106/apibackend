package com.hot106.apibackend.objects;

import java.sql.Timestamp;

public class User {

    private int id;
    private String first_name;
    private String last_name;
    private String username;
    private String password;
    private String email;
    private int role_id;
    private String img;
    private int last_modified_by;
    private Timestamp last_modified_date;
    private Timestamp deleted_date;

    public static final int USER_ROLE_ID_GENERAL_USER = 1; //15 minutes
    public static final int USER_ROLE_ID_ADMIN_USER = 2; //1 hour

    public User() {}

    public User(int id, String first_name, String last_name, String username, String password, String email, int role_id, String img, int last_modified_by, Timestamp last_modified_date, Timestamp deleted_date) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.username = username;
        this.password = password;
        this.email = email;
        this.role_id = role_id;
        this.img = img;
        this.last_modified_by = last_modified_by;
        this.last_modified_date = last_modified_date;
        this.deleted_date = deleted_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public int getRole_id() {
        return role_id;
    }

    public String getImg() {
        return img;
    }

    public int getLast_modified_by() {
        return last_modified_by;
    }

    public Timestamp getLast_modified_date() {
        return last_modified_date;
    }

    public Timestamp getDeleted_date() {
        return deleted_date;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setLast_modified_by(int last_modified_by) {
        this.last_modified_by = last_modified_by;
    }

    public void setLast_modified_date(Timestamp last_modified_date) {
        this.last_modified_date = last_modified_date;
    }

    public void setDeleted_date(Timestamp deleted_date) {
        this.deleted_date = deleted_date;
    }
}
