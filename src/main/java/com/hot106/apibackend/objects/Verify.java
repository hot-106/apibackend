package com.hot106.apibackend.objects;

import java.sql.Timestamp;

public class Verify {

    private String verify_token;
    private int user_id;
    private Timestamp created_at;
    private int completed;

    public static final int VERIFY_TOKEN_EXPIRY_TIME = 600; //10 minutes
    public static final int VERIFY_TOKEN_SIZE = 50;

    public Verify(String verify_token, int user_id, Timestamp created_at, int completed) {
        this.verify_token = verify_token;
        this.user_id = user_id;
        this.created_at = created_at;
        this.completed = completed;
    }

    public String getVerify_token() {
        return verify_token;
    }

    public void setVerify_token(String verify_token) {
        this.verify_token = verify_token;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public int getCompleted() {
        return completed;
    }

    public void setCompleted(int completed) {
        this.completed = completed;
    }

    public static String generateNewVerifyToken() {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxyz0123456789";
        StringBuilder sb = new StringBuilder(VERIFY_TOKEN_SIZE);

        for (int i = 0; i < VERIFY_TOKEN_SIZE; i++){
            sb.append(characters.charAt((int)(characters.length() * Math.random())));
        }

        return sb.toString();
    }
}
