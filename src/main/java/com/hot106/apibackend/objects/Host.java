package com.hot106.apibackend.objects;

import java.sql.Timestamp;

public class Host {

    private int id;
    private String first_name;
    private String last_name;
    private String bio;
    private String img;
    private String facebook_url;
    private String instagram_url;
    private String twitter_url;
    private String tiktok_url;
    private int last_modified_by;
    private Timestamp last_modified_date;
    private Timestamp deleted_date;

    public Host(int id, String first_name, String last_name, String bio, String img, String facebook_url, String instagram_url, String twitter_url, String tiktok_url, int last_modified_by, Timestamp last_modified_date, Timestamp deleted_date) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.bio = bio;
        this.img = img;
        this.facebook_url = facebook_url;
        this.instagram_url = instagram_url;
        this.twitter_url = twitter_url;
        this.tiktok_url = tiktok_url;
        this.last_modified_by = last_modified_by;
        this.last_modified_date = last_modified_date;
        this.deleted_date = deleted_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getFacebook_url() {
        return facebook_url;
    }

    public void setFacebook_url(String facebook_url) {
        this.facebook_url = facebook_url;
    }

    public String getInstagram_url() {
        return instagram_url;
    }

    public void setInstagram_url(String instagram_url) {
        this.instagram_url = instagram_url;
    }

    public String getTwitter_url() {
        return twitter_url;
    }

    public void setTwitter_url(String twitter_url) {
        this.twitter_url = twitter_url;
    }

    public String getTiktok_url() {
        return tiktok_url;
    }

    public void setTiktok_url(String tiktok_url) {
        this.tiktok_url = tiktok_url;
    }

    public int getLast_modified_by() {
        return last_modified_by;
    }

    public void setLast_modified_by(int last_modified_by) {
        this.last_modified_by = last_modified_by;
    }

    public Timestamp getLast_modified_date() {
        return last_modified_date;
    }

    public void setLast_modified_date(Timestamp last_modified_date) {
        this.last_modified_date = last_modified_date;
    }

    public Timestamp getDeleted_date() {
        return deleted_date;
    }

    public void setDeleted_date(Timestamp deleted_date) {
        this.deleted_date = deleted_date;
    }
}
