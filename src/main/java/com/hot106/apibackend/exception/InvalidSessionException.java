package com.hot106.apibackend.exception;

public class InvalidSessionException extends RuntimeException {
    public InvalidSessionException(String message){
        super(message);
    }
}
