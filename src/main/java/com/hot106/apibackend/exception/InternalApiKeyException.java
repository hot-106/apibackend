package com.hot106.apibackend.exception;

public class InternalApiKeyException extends RuntimeException {
    public InternalApiKeyException(String message){
        super(message);
    }
}
