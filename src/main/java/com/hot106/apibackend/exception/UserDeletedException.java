package com.hot106.apibackend.exception;

public class UserDeletedException extends RuntimeException {
    public UserDeletedException(String message){
        super(message);
    }
}
