package com.hot106.apibackend.exception;

public class InvalidApiKeyException extends RuntimeException {
    public InvalidApiKeyException(String message){
        super(message);
    }
}
