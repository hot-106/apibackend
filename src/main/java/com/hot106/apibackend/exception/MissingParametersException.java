package com.hot106.apibackend.exception;

public class MissingParametersException extends RuntimeException {
    public MissingParametersException(String message){
        super(message);
    }
}
