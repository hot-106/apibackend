package com.hot106.apibackend.exception;

public class CreateOnActiveSessionException extends RuntimeException {
    public CreateOnActiveSessionException(String message){
        super(message);
    }
}
