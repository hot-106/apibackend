package com.hot106.apibackend.exception;

public class UnexpectedErrorException extends RuntimeException {
    public UnexpectedErrorException(String message){
        super(message);
    }
}
