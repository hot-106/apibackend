package com.hot106.apibackend.exception;

public class SessionDoesNotExistException extends RuntimeException {
    public SessionDoesNotExistException(String message){
        super(message);
    }
}
