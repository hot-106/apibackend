package com.hot106.apibackend.exception;

public class InvalidParametersException extends RuntimeException {
    public InvalidParametersException(String message){
        super(message);
    }
}
