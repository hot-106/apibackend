package com.hot106.apibackend.controller;

import com.hot106.apibackend.exception.*;
import com.hot106.apibackend.objects.*;
import com.hot106.apibackend.requestobjects.ScheduleRequest;
import com.hot106.apibackend.requestobjects.validation.RequestKey;
import com.hot106.apibackend.utils.DatabaseConfiguration;
import com.hot106.apibackend.utils.DatabaseInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
public class ScheduleController {

    Logger logger = LoggerFactory.getLogger(ScheduleController.class);

    @Autowired
    DatabaseConfiguration dbConf;

    private void validateRequestParameters(ScheduleRequest srq) {

        DatabaseInterface di = new DatabaseInterface(dbConf);
        String currentKeyValue;

        try{
            currentKeyValue = di.getInternalApiKey();
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        RequestKey.validateRqk(srq.getRqk(), currentKeyValue, logger);

        if(srq.getSchedule() == null){
            logger.warn("Schedule was not supplied in the request body. Request denied.");
            throw new MissingParametersException("Host was not supplied in the request body. Request denied.");
        }

    }

    private boolean validateUserSession(Session sessionToVerify, String requestIP) {
        DatabaseInterface di = new DatabaseInterface(dbConf);
        boolean validated = false;

        try{
            if(di.validateUserSession(sessionToVerify, requestIP) != null){
                logger.info("Session ID '" + sessionToVerify.getSession_id() + "' is valid for user " + sessionToVerify.getUser_id());
                validated = true;
            }
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        return validated;
    }

    //Send title, host_id, event_id (optional), recurring (optional), begin, and end
    @PostMapping(path = "/schedule/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<Schedule>> createSchedule(@RequestBody ScheduleRequest scheduleData) {
        validateRequestParameters(scheduleData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        Timestamp currentTimestamp = new java.sql.Timestamp(System.currentTimeMillis());
        Schedule scheduleToCreate = scheduleData.getSchedule();
        String httpSessionId = scheduleData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = scheduleData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received a CreateSchedule request for Schedule with title '" + scheduleToCreate.getTitle() + "' (IP Address: " + requestIP + ")");

        if(httpSessionId == null){
            logger.warn("User attempted to create a schedule without an active session.");
            throw new CreateOnActiveSessionException("Attempt to create a schedule without an active session.");
        }
        else{
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                UserPermission currentUserPermission;

                try{
                    currentUserPermission = di.getUserPermission(userSession.getUser_id());
                }
                catch(Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUserPermission == null){
                    logger.warn("Unable to get user permissions for user ID " + userSession.getUser_id());
                    throw new UnexpectedErrorException("Unable to get user permissions for user ID " + userSession.getUser_id());
                }
                else {
                    if(currentUserPermission.getCreate_schedule() != 1){
                        logger.warn("User attempted to create a schedule without sufficient privileges. User ID (" + userSession.getUser_id() + ")");
                        throw new UnauthorizedException("User attempted to create a schedule without sufficient privileges.");
                    }

                    scheduleToCreate.setLast_modified_by(userSession.getUser_id());
                    scheduleToCreate.setLast_modified_date(currentTimestamp);
                }
            }
            else{
                logger.warn("User attempted to create a schedule with an invalid session.");
                throw new InvalidSessionException("Attempt to create a schedule with an invalid session.");
            }
        }

        Schedule createdSchedule;

        try{
            createdSchedule = di.createSchedule(scheduleToCreate);
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(createdSchedule == null){
            logger.warn("Failed to create schedule with title '" + scheduleToCreate.getTitle() + "'");
            throw new UnexpectedErrorException("Failed to create schedule with title '" + scheduleToCreate.getTitle() + "'");
        }

        GenericResponse<Schedule> responseData = new GenericResponse<Schedule>("SUCCESS", createdSchedule);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    //Send all schedule fields
    @PostMapping(path = "/schedule/edit", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<Schedule>> editSchedule(@RequestBody ScheduleRequest scheduleData) {
        validateRequestParameters(scheduleData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        Timestamp currentTimestamp = new java.sql.Timestamp(System.currentTimeMillis());
        Schedule scheduleToUpdate = scheduleData.getSchedule();
        String httpSessionId = scheduleData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = scheduleData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received a EditSchedule request for Schedule ID " + scheduleToUpdate.getId() + " (IP Address: " + requestIP + ")");

        if(httpSessionId == null){
            logger.warn("User attempted to update a schedule without an active session.");
            throw new CreateOnActiveSessionException("Attempt to update a schedule without an active session.");
        }
        else{
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                UserPermission currentUserPermission;

                try{
                    currentUserPermission = di.getUserPermission(userSession.getUser_id());
                }
                catch(Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUserPermission == null){
                    logger.warn("Unable to get user permissions for user ID " + userSession.getUser_id());
                    throw new UnexpectedErrorException("Unable to get user permissions for user ID " + userSession.getUser_id());
                }
                else {
                    if(currentUserPermission.getCreate_schedule() != 1){
                        logger.warn("User attempted to update a schedule without sufficient privileges. User ID (" + userSession.getUser_id() + ")");
                        throw new UnauthorizedException("User attempted to update a schedule without sufficient privileges.");
                    }

                    scheduleToUpdate.setLast_modified_by(userSession.getUser_id());
                    scheduleToUpdate.setLast_modified_date(currentTimestamp);
                }
            }
            else{
                logger.warn("User attempted to update a schedule with an invalid session.");
                throw new InvalidSessionException("Attempt to update a schedule with an invalid session.");
            }
        }

        return null;
    }

    //Send schedule_id
    @PostMapping(path = "/schedule/get", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<Schedule>> getSchedule(@RequestBody ScheduleRequest scheduleData) {
        validateRequestParameters(scheduleData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        Schedule scheduleToGet = scheduleData.getSchedule();
        String requestIP = scheduleData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received a GetSchedule request for Schedule ID " + scheduleToGet.getId() + " (IP Address: " + requestIP + ")");

        return null;
    }

    //Send begin
    @PostMapping(path = "/schedule/getForDay", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<List<Schedule>>> getScheduleForDay(@RequestBody ScheduleRequest scheduleData) {
        validateRequestParameters(scheduleData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        Schedule scheduleToGet = scheduleData.getSchedule();
        String requestIP = scheduleData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received a GetScheduleForDay request for " + scheduleToGet.getBegin() + " (IP Address: " + requestIP + ")");

        return null;
    }

    //Send host_id
    @PostMapping(path = "/schedule/getForHost", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<List<Schedule>>> getScheduleForHost(@RequestBody ScheduleRequest scheduleData) {
        validateRequestParameters(scheduleData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        Schedule scheduleToGet = scheduleData.getSchedule();
        String requestIP = scheduleData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received a GetScheduleForHost request for host ID " + scheduleToGet.getHost_id() + " (IP Address: " + requestIP + ")");

        return null;
    }

    //Send schedule_id
    @PostMapping(path = "/schedule/delete", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<Schedule>> deleteSchedule(@RequestBody ScheduleRequest scheduleData)  {
        validateRequestParameters(scheduleData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        Timestamp currentTimestamp = new java.sql.Timestamp(System.currentTimeMillis());
        Schedule scheduleToUpdate = scheduleData.getSchedule();
        String httpSessionId = scheduleData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = scheduleData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received a DeleteSchedule request for Schedule ID " + scheduleToUpdate.getId() + " (IP Address: " + requestIP + ")");

        if(httpSessionId == null){
            logger.warn("User attempted to delete a schedule without an active session.");
            throw new CreateOnActiveSessionException("Attempt to delete a schedule without an active session.");
        }
        else{
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                UserPermission currentUserPermission;

                try{
                    currentUserPermission = di.getUserPermission(userSession.getUser_id());
                }
                catch(Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUserPermission == null){
                    logger.warn("Unable to get user permissions for user ID " + userSession.getUser_id());
                    throw new UnexpectedErrorException("Unable to get user permissions for user ID " + userSession.getUser_id());
                }
                else {
                    if(currentUserPermission.getCreate_schedule() != 1){
                        logger.warn("User attempted to delete a schedule without sufficient privileges. User ID (" + userSession.getUser_id() + ")");
                        throw new UnauthorizedException("User attempted to delete a schedule without sufficient privileges.");
                    }

                    scheduleToUpdate.setLast_modified_by(userSession.getUser_id());
                    scheduleToUpdate.setLast_modified_date(currentTimestamp);
                }
            }
            else{
                logger.warn("User attempted to delete a schedule with an invalid session.");
                throw new InvalidSessionException("Attempt to delete a schedule with an invalid session.");
            }
        }

        return null;
    }
}
