package com.hot106.apibackend.controller;

import com.hot106.apibackend.exception.*;
import com.hot106.apibackend.objects.*;
import com.hot106.apibackend.requestobjects.HostRequest;
import com.hot106.apibackend.requestobjects.validation.RequestKey;
import com.hot106.apibackend.utils.DatabaseConfiguration;
import com.hot106.apibackend.utils.DatabaseInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
public class HostController {

    Logger logger = LoggerFactory.getLogger(HostController.class);

    @Autowired
    DatabaseConfiguration dbConf;

    private void validateRequestParameters(HostRequest hrq) {

        DatabaseInterface di = new DatabaseInterface(dbConf);
        String currentKeyValue;

        try{
            currentKeyValue = di.getInternalApiKey();
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        RequestKey.validateRqk(hrq.getRqk(), currentKeyValue, logger);

        if(hrq.getHost() == null){
            logger.warn("Host was not supplied in the request body. Request denied.");
            throw new MissingParametersException("Host was not supplied in the request body. Request denied.");
        }

    }

    private void validateRequestKeyOnly(RequestKey rqk) {

        DatabaseInterface di = new DatabaseInterface(dbConf);
        String currentKeyValue;

        try{
            currentKeyValue = di.getInternalApiKey();
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        RequestKey.validateRqk(rqk, currentKeyValue, logger);

    }

    private boolean validateUserSession(Session sessionToVerify, String requestIP) {
        DatabaseInterface di = new DatabaseInterface(dbConf);
        boolean validated = false;

        try{
            if(di.validateUserSession(sessionToVerify, requestIP) != null){
                logger.info("Session ID '" + sessionToVerify.getSession_id() + "' is valid for user " + sessionToVerify.getUser_id());
                validated = true;
            }
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        return validated;
    }

    // (send first name, last name, and other fields optionally)
    @PostMapping(path = "/host/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<Host>> createHost(@RequestBody HostRequest hostData) {
        validateRequestParameters(hostData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        Timestamp currentTimestamp = new java.sql.Timestamp(System.currentTimeMillis());
        Host hostToCreate = hostData.getHost();
        String httpSessionId = hostData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = hostData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received a CreateHost request for host '" + hostToCreate.getFirst_name() + " " + hostToCreate.getLast_name() + "' (IP Address: " + requestIP + ")");

        if(httpSessionId == null){
            logger.warn("User attempted to create a host without an active session.");
            throw new CreateOnActiveSessionException("Attempt to create a host without an active session.");
        }
        else{
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                UserPermission currentUserPermission;

                try{
                    currentUserPermission = di.getUserPermission(userSession.getUser_id());
                }
                catch(Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUserPermission == null){
                    logger.warn("Unable to get user permissions for user ID " + userSession.getUser_id());
                    throw new UnexpectedErrorException("Unable to get user permissions for user ID " + userSession.getUser_id());
                }
                else {
                    if(currentUserPermission.getCreate_host() != 1){
                        logger.warn("User attempted to create a host without sufficient privileges. User ID (" + userSession.getUser_id() + ")");
                        throw new UnauthorizedException("User attempted to create a host without sufficient privileges.");
                    }

                    hostToCreate.setLast_modified_by(userSession.getUser_id());
                    hostToCreate.setLast_modified_date(currentTimestamp);
                }
            }
            else{
                logger.warn("User attempted to create a host with an invalid session.");
                throw new InvalidSessionException("Attempt to create a host with an invalid session.");
            }
        }

        Host createdHost;

        try{
            createdHost = di.createHost(hostToCreate);
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(createdHost == null){
            logger.warn("Failed to create new host with name '" + hostToCreate.getFirst_name() + " " + hostToCreate.getLast_name() + "'");
            throw new UnexpectedErrorException("Failed to create new host with name '" + hostToCreate.getFirst_name() + " " + hostToCreate.getLast_name() + "'");
        }

        GenericResponse<Host> responseData = new GenericResponse<Host>("SUCCESS", createdHost);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // (send id OR first + last name)
    @PostMapping(path = "/host/get", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<Host>> getHost(@RequestBody HostRequest hostData) {
        validateRequestParameters(hostData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        Host hostToGet = hostData.getHost();
        Host foundHostData;
        String requestIP = hostData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received a GetHost request for host ID " + hostToGet.getId() + " (IP Address: " + requestIP + ")");

        if(hostToGet.getId() == 0){
            logger.info("Host ID not provided in GetHost Request. Using First & Last name...");

            if(hostToGet.getFirst_name() == null){
                logger.warn("First name not provided in request");
                throw new MissingParametersException("First name not provided in request");
            }

            if(hostToGet.getLast_name() == null){
                logger.warn("Last name not provided in request");
                throw new MissingParametersException("Last name not provided in request");
            }

            try{
                foundHostData = di.getHostByName(hostToGet.getFirst_name(), hostToGet.getLast_name());
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }
        }
        else{
            try{
                foundHostData = di.getHostById(hostToGet.getId());
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }
        }

        if(foundHostData == null){
            logger.warn("Unable to find host");
            throw new NotFoundException("Unable to find host");
        }

        GenericResponse<Host> responseData = new GenericResponse<Host>("SUCCESS", foundHostData);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // (send nothing - key only)
    @PostMapping(path = "/host/getAll", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<List<Host>>> getAllHosts(@RequestBody HostRequest hostData) {
        validateRequestKeyOnly(hostData.getRqk());
        DatabaseInterface di = new DatabaseInterface(dbConf);
        String requestIP = hostData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received a GetAllHosts request (IP Address: " + requestIP + ")");

        List<Host> hostList;

        try{
            hostList = di.getAllHosts();
        }
        catch (Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(hostList.size() == 0){
            logger.warn("No hosts found on GetAllHosts request.");
            throw new NotFoundException("No hosts found on GetAllHosts request.");
        }

        GenericResponse<List<Host>> responseData = new GenericResponse<List<Host>>("SUCCESS", hostList);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // (send id + all other host fields)
    @PostMapping(path = "/host/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<Host>> updateHost(@RequestBody HostRequest hostData) {
        validateRequestParameters(hostData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        Timestamp currentTimestamp = new java.sql.Timestamp(System.currentTimeMillis());
        Host hostToUpdate = hostData.getHost();
        Host updatedHost;
        String httpSessionId = hostData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = hostData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received an UpdateHost request for host ID '" + hostToUpdate.getId() + "' (IP Address: " + requestIP + ")");

        if(httpSessionId == null){
            logger.warn("User attempted to update a host without an active session.");
            throw new CreateOnActiveSessionException("Attempt to update a host without an active session.");
        }
        else{
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                UserPermission currentUserPermission;

                try{
                    currentUserPermission = di.getUserPermission(userSession.getUser_id());
                }
                catch(Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUserPermission == null){
                    logger.warn("Unable to get user permissions for user ID " + userSession.getUser_id());
                    throw new UnexpectedErrorException("Unable to get user permissions for user ID " + userSession.getUser_id());
                }
                else {
                    if(currentUserPermission.getCreate_host() != 1){
                        logger.warn("User attempted to update a host without sufficient privileges. User ID (" + userSession.getUser_id() + ")");
                        throw new UnauthorizedException("User attempted to update a host without sufficient privileges.");
                    }
                }

                //Set last modified by and last modified date fields
                hostToUpdate.setLast_modified_by(userSession.getUser_id());
                hostToUpdate.setLast_modified_date(currentTimestamp);
            }
            else{
                logger.warn("User attempted to update a host with an invalid session.");
                throw new InvalidSessionException("Attempt to update a host with an invalid session.");
            }
        }

        if(hostToUpdate.getId() == 0){
            logger.warn("Host ID not provided in request.");
            throw new MissingParametersException("Host ID not provided in request.");
        }

        try{
            updatedHost = di.updateHost(hostToUpdate);
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(updatedHost == null){
            logger.warn("Failed to update host ID " + hostToUpdate.getId());
            throw new UnexpectedErrorException("Failed to update host ID " + hostToUpdate.getId());
        }

        GenericResponse<Host> responseData = new GenericResponse<Host>("SUCCESS", updatedHost);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // (send id)
    @PostMapping(path = "/host/delete", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<Host>> deleteHost(@RequestBody HostRequest hostData) {
        validateRequestParameters(hostData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        Host hostToDelete = hostData.getHost();
        String httpSessionId = hostData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = hostData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received an DeleteHost request for host ID '" + hostToDelete.getId() + "' (IP Address: " + requestIP + ")");

        if(httpSessionId == null){
            logger.warn("User attempted to delete a host without an active session.");
            throw new CreateOnActiveSessionException("Attempt to delete a host without an active session.");
        }
        else{
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                UserPermission currentUserPermission;

                try{
                    currentUserPermission = di.getUserPermission(userSession.getUser_id());
                }
                catch(Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUserPermission == null){
                    logger.warn("Unable to get user permissions for user ID " + userSession.getUser_id());
                    throw new UnexpectedErrorException("Unable to get user permissions for user ID " + userSession.getUser_id());
                }
                else {
                    if(currentUserPermission.getCreate_host() != 1){
                        logger.warn("User attempted to delete a host without sufficient privileges. User ID (" + userSession.getUser_id() + ")");
                        throw new UnauthorizedException("User attempted to delete a host without sufficient privileges.");
                    }
                }
            }
            else{
                logger.warn("User attempted to delete a host with an invalid session.");
                throw new InvalidSessionException("Attempt to delete a host with an invalid session.");
            }
        }

        if(hostToDelete.getId() == 0){
            logger.warn("Host ID not provided in request.");
            throw new MissingParametersException("Host ID not provided in request.");
        }

        Host deletedHost;

        try{
            deletedHost = di.deleteHost(hostToDelete.getId());
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(deletedHost == null){
            logger.warn("Failed to delete host ID " + hostToDelete.getId());
            throw new UnexpectedErrorException("Failed to delete host ID " + hostToDelete.getId());
        }

        GenericResponse<Host> responseData = new GenericResponse<Host>("SUCCESS", deletedHost);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }
}
