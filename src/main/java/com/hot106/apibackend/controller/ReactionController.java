package com.hot106.apibackend.controller;

import com.hot106.apibackend.exception.*;
import com.hot106.apibackend.objects.*;
import com.hot106.apibackend.requestobjects.ReactionRequest;
import com.hot106.apibackend.requestobjects.validation.RequestKey;
import com.hot106.apibackend.utils.DatabaseConfiguration;
import com.hot106.apibackend.utils.DatabaseInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.sql.Timestamp;

@RestController
public class ReactionController {

    Logger logger = LoggerFactory.getLogger(ReactionController.class);

    @Autowired
    DatabaseConfiguration dbConf;

    private void validateRequestParameters(ReactionRequest rrq) {

        DatabaseInterface di = new DatabaseInterface(dbConf);
        String currentKeyValue;

        try{
            currentKeyValue = di.getInternalApiKey();
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        RequestKey.validateRqk(rrq.getRqk(), currentKeyValue, logger);

        if(rrq.getReaction() == null){
            logger.warn("Reaction was not supplied in the request body. Request denied.");
            throw new MissingParametersException("Reaction was not supplied in the request body. Request denied.");
        }

    }

    private boolean validateUserSession(Session sessionToVerify, String requestIP) {
        DatabaseInterface di = new DatabaseInterface(dbConf);
        boolean validated = false;

        try{
            if(di.validateUserSession(sessionToVerify, requestIP) != null){
                logger.info("Session ID '" + sessionToVerify.getSession_id() + "' is valid for user " + sessionToVerify.getUser_id());
                validated = true;
            }
        }
        catch (Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        return validated;
    }

    //Send user_id, post_type, post_id, user_id, and reaction_type fields
    @PostMapping(path = "/reaction/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<Reaction>> createReaction(@RequestBody ReactionRequest reactionData) {
        validateRequestParameters(reactionData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
        Reaction reactionToCreate = reactionData.getReaction();
        String httpSessionId = reactionData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = reactionData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received a CreateReaction request from user ID " + reactionToCreate.getUser_id() + " for Post ID " + reactionToCreate.getPost_id() + " (Post Type: " + reactionToCreate.getPost_type() + ") (IP Address: " + requestIP + ")");

        if(httpSessionId == null){
            logger.warn("User attempted to create a reaction without an active session.");
            throw new CreateOnActiveSessionException("Attempt to create a reaction without an active session.");
        }
        else{
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                UserPermission currentUserPermission;

                try{
                    currentUserPermission = di.getUserPermission(userSession.getUser_id());
                }
                catch(Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUserPermission == null){
                    logger.warn("Unable to get user permissions for user ID " + userSession.getUser_id());
                    throw new UnexpectedErrorException("Unable to get user permissions for user ID " + userSession.getUser_id());
                }
                else {
                    if(currentUserPermission.getCreate_reaction() != 1){
                        logger.warn("User attempted to create a reaction without sufficient privileges. User ID (" + userSession.getUser_id() + ")");
                        throw new UnauthorizedException("User attempted to create a reaction without sufficient privileges.");
                    }

                    if(userSession.getUser_id() != reactionToCreate.getUser_id()){
                        logger.warn("User attempted to create a reaction for a different user ID. User ID (" + userSession.getUser_id() + ")");
                        throw new UnauthorizedException("User attempted to create a reaction for a different user ID.");
                    }

                    reactionToCreate.setLast_modified_by(userSession.getUser_id());
                    reactionToCreate.setLast_modified_date(currentTimestamp);
                }
            }
            else{
                logger.warn("User attempted to create a reaction with an invalid session.");
                throw new InvalidSessionException("Attempt to create a reaction with an invalid session.");
            }
        }

        Reaction createdReaction;

        try{
            createdReaction = di.createReaction(reactionToCreate);
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(createdReaction == null){
            logger.error("Failed to create reaction by user ID " + reactionToCreate.getUser_id() + " for Post ID " + reactionToCreate.getPost_id() + " (Post Type: " + reactionToCreate.getPost_type() + ")");
            throw new UnexpectedErrorException("Failed to create reaction by user ID " + reactionToCreate.getUser_id());
        }

        GenericResponse<Reaction> responseData = new GenericResponse<Reaction>("SUCCESS", createdReaction);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    //Send post_type, post_id, reaction_type fields
    @PostMapping(path = "/reaction/getCountForPost", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<Count>> getReactionCount(@RequestBody ReactionRequest reactionData) throws SQLException {
        validateRequestParameters(reactionData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        Reaction reactionToGet = reactionData.getReaction();
        String requestIP = reactionData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received a GetReactionCount request for Post ID " + reactionToGet.getPost_id() + " (Post Type: " + reactionToGet.getPost_type() + ") (IP Address: " + requestIP + ")");

        Count reactionCount = new Count();
        reactionCount.setCount(di.getReactionCount(reactionToGet.getPost_id(), reactionToGet.getPost_type(), reactionToGet.getReaction_type()));

        GenericResponse<Count> responseData = new GenericResponse<Count>("SUCCESS", reactionCount);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    //Send id
    @PostMapping(path = "/reaction/delete", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<Reaction>> deleteReaction(@RequestBody ReactionRequest reactionData) {
        validateRequestParameters(reactionData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
        Reaction reactionToDelete = reactionData.getReaction();
        String httpSessionId = reactionData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = reactionData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received a DeleteReaction request for reaction ID " + reactionToDelete.getId() + " (IP Address: " + requestIP + ")");

        if(httpSessionId == null){
            logger.warn("User attempted to delete a reaction without an active session.");
            throw new CreateOnActiveSessionException("Attempt to delete a reaction without an active session.");
        }
        else{
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                UserPermission currentUserPermission;

                try{
                    currentUserPermission = di.getUserPermission(userSession.getUser_id());
                }
                catch(Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUserPermission == null){
                    logger.warn("Unable to get user permissions for user ID " + userSession.getUser_id());
                    throw new UnexpectedErrorException("Unable to get user permissions for user ID " + userSession.getUser_id());
                }
                else {
                    if(currentUserPermission.getCreate_reaction() != 1){
                        logger.warn("User attempted to delete a reaction without sufficient privileges. User ID (" + userSession.getUser_id() + ")");
                        throw new UnauthorizedException("User attempted to delete a reaction without sufficient privileges.");
                    }

                    //Get reaction details to validate correct user is deleting the reaction
                    Reaction tmpReaction;

                    try{
                        tmpReaction = di.getReaction(reactionToDelete.getId());
                    }
                    catch (Exception e){
                        throw new UnexpectedErrorException(e.getMessage());
                    }

                    if(tmpReaction == null){
                        logger.warn("Failed to get reaction data for user validation. Reaction ID (" + reactionToDelete.getId() + ")");
                        throw new UnexpectedErrorException("Failed to get reaction data for user validation.");
                    }
                    else{
                        reactionToDelete.setUser_id(tmpReaction.getUser_id());

                        if(userSession.getUser_id() != reactionToDelete.getUser_id()){
                            logger.warn("User attempted to delete a reaction for a different user. User ID (" + userSession.getUser_id() + ")");
                            throw new UnauthorizedException("User attempted to delete a reaction for a different user.");
                        }
                    }

                    reactionToDelete.setLast_modified_by(userSession.getUser_id());
                    reactionToDelete.setLast_modified_date(currentTimestamp);
                }
            }
            else{
                logger.warn("User attempted to delete a reaction with an invalid session.");
                throw new InvalidSessionException("Attempt to delete a reaction with an invalid session.");
            }
        }

        Reaction deletedReaction;

        try{
            deletedReaction = di.deleteReaction(reactionToDelete);
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(deletedReaction == null){
            logger.warn("Failed to delete reaction ID " + reactionToDelete.getId());
            throw new UnexpectedErrorException("Failed to delete reaction ID " + reactionToDelete.getId());
        }

        GenericResponse<Reaction> responseData = new GenericResponse<Reaction>("SUCCESS", deletedReaction);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }
}
