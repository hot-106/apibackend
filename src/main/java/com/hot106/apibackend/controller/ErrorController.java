package com.hot106.apibackend.controller;

import com.hot106.apibackend.exception.*;
import com.hot106.apibackend.objects.Error;
import com.hot106.apibackend.objects.GenericResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
public class ErrorController extends ResponseEntityExceptionHandler {

    Logger logger = LoggerFactory.getLogger(ErrorController.class);

    private Error getError(Exception ex){
        List<String> error_details = new ArrayList<String>();
        error_details.add(ex.getLocalizedMessage());
        return new Error(error_details);
    }

    //Handle generic exceptions
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<GenericResponse<Error>> handleGenericException(Exception ex){
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;

        logger.error("An exception has occurred: " + ex.getMessage());

        GenericResponse<Error> errorData = new GenericResponse<Error>("ERROR", getError(ex));
        return new ResponseEntity<>(errorData, status);
    }

    //Handle generic exceptions
    @ExceptionHandler(UnexpectedErrorException.class)
    public final ResponseEntity<GenericResponse<Error>> handleUnexpectedErrorException(Exception ex){
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;

        logger.error("An UnexpectedError exception has occurred: " + ex.getMessage());

        GenericResponse<Error> errorData = new GenericResponse<Error>("ERROR", getError(ex));
        return new ResponseEntity<>(errorData, status);
    }

    //Handle MissingParameters exceptions
    @ExceptionHandler(MissingParametersException.class)
    public final ResponseEntity<GenericResponse<Error>> handleMissingParametersException(Exception ex){
        HttpStatus status = HttpStatus.BAD_REQUEST;

        logger.error("A MissingParameters exception has occurred: " + ex.getMessage());

        GenericResponse<Error> errorData = new GenericResponse<Error>("MISSING PARAMETERS", getError(ex));
        return new ResponseEntity<>(errorData, status);
    }

    //Handle NotFound exceptions
    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<GenericResponse<Error>> handleNotFoundException(Exception ex){
        HttpStatus status = HttpStatus.NOT_FOUND;

        logger.error("A NotFound exception has occurred: " + ex.getMessage());

        GenericResponse<Error> errorData = new GenericResponse<Error>("NOT FOUND", getError(ex));
        return new ResponseEntity<>(errorData, status);
    }

    //Handle InvalidSession exceptions
    @ExceptionHandler(InvalidSessionException.class)
    public final ResponseEntity<GenericResponse<Error>> handleInvalidSessionException(Exception ex){
        HttpStatus status = HttpStatus.OK;

        logger.error("An InvalidSession exception has occurred: " + ex.getMessage());

        GenericResponse<Error> errorData = new GenericResponse<Error>("INVALID", getError(ex));
        return new ResponseEntity<>(errorData, status);
    }

    //Handle NonExistentSession exceptions
    @ExceptionHandler(SessionDoesNotExistException.class)
    public final ResponseEntity<GenericResponse<Error>> handleNonExistentSessionException(Exception ex){
        HttpStatus status = HttpStatus.OK;

        logger.error("A SessionDoesNotExist exception has occurred: " + ex.getMessage());

        GenericResponse<Error> errorData = new GenericResponse<Error>("INVALID", getError(ex));
        return new ResponseEntity<>(errorData, status);
    }

    //Handle InvalidApiKey exceptions
    @ExceptionHandler(InvalidApiKeyException.class)
    public final ResponseEntity<GenericResponse<Error>> handleInvalidApiKeyException(Exception ex){
        HttpStatus status = HttpStatus.UNAUTHORIZED;

        logger.error("An InvalidApiKey exception has occurred: " + ex.getMessage());

        GenericResponse<Error> errorData = new GenericResponse<Error>("UNAUTHORIZED", getError(ex));
        return new ResponseEntity<>(errorData, status);
    }

    //Handle Unauthorized exceptions
    @ExceptionHandler(UnauthorizedException.class)
    public final ResponseEntity<GenericResponse<Error>> handleUnauthorizedException(Exception ex){
        HttpStatus status = HttpStatus.UNAUTHORIZED;

        logger.error("An Unauthorized exception has occurred: " + ex.getMessage());

        GenericResponse<Error> errorData = new GenericResponse<Error>("UNAUTHORIZED", getError(ex));
        return new ResponseEntity<>(errorData, status);
    }

    //Handle UserDeleted exceptions
    @ExceptionHandler(UserDeletedException.class)
    public final ResponseEntity<GenericResponse<Error>> handleUserDeletedException(Exception ex){
        HttpStatus status = HttpStatus.UNAUTHORIZED;

        logger.error("An UserDeleted exception has occurred: " + ex.getMessage());

        GenericResponse<Error> errorData = new GenericResponse<Error>("DELETED", getError(ex));
        return new ResponseEntity<>(errorData, status);
    }
}