package com.hot106.apibackend.controller;

import com.hot106.apibackend.exception.*;
import com.hot106.apibackend.objects.*;
import com.hot106.apibackend.requestobjects.CommentRequest;
import com.hot106.apibackend.requestobjects.validation.RequestKey;
import com.hot106.apibackend.utils.DatabaseConfiguration;
import com.hot106.apibackend.utils.DatabaseInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
public class CommentController {

    Logger logger = LoggerFactory.getLogger(CommentController.class);

    @Autowired
    DatabaseConfiguration dbConf;

    private void validateRequestParameters(CommentRequest crq) {

        DatabaseInterface di = new DatabaseInterface(dbConf);
        String currentKeyValue;

        try{
            currentKeyValue = di.getInternalApiKey();
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        RequestKey.validateRqk(crq.getRqk(), currentKeyValue, logger);

        if(crq.getComment() == null){
            logger.warn("Comment was not supplied in the request body. Request denied.");
            throw new MissingParametersException("Comment was not supplied in the request body. Request denied.");
        }

    }

    private boolean validateUserSession(Session sessionToVerify, String requestIP) {
        DatabaseInterface di = new DatabaseInterface(dbConf);
        boolean validated = false;

        try{
            if(di.validateUserSession(sessionToVerify, requestIP) != null){
                logger.info("Session ID '" + sessionToVerify.getSession_id() + "' is valid for user " + sessionToVerify.getUser_id());
                validated = true;
            }
        }
        catch (Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        return validated;
    }

    //Send post_type, post_id, user_id, content, and reply_to fields
    @PostMapping(path = "/comment/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<Comment>> createComment(@RequestBody CommentRequest commentData) {
        validateRequestParameters(commentData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
        Comment commentToCreate = commentData.getComment();
        String httpSessionId = commentData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = commentData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received a CreateComment request from user ID " + commentToCreate.getUser_id() + " for Post ID " + commentToCreate.getPost_id() + " (Post Type: " + commentToCreate.getPost_type() + ") (IP Address: " + requestIP + ")");

        if(httpSessionId == null){
            logger.warn("User attempted to create a comment without an active session.");
            throw new CreateOnActiveSessionException("Attempt to create a comment without an active session.");
        }
        else{
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                UserPermission currentUserPermission;

                try{
                    currentUserPermission = di.getUserPermission(userSession.getUser_id());
                }
                catch(Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUserPermission == null){
                    logger.warn("Unable to get user permissions for user ID " + userSession.getUser_id());
                    throw new UnexpectedErrorException("Unable to get user permissions for user ID " + userSession.getUser_id());
                }
                else {
                    if(currentUserPermission.getCreate_comment() != 1){
                        logger.warn("User attempted to create a comment without sufficient privileges. User ID (" + userSession.getUser_id() + ")");
                        throw new UnauthorizedException("User attempted to create a comment without sufficient privileges.");
                    }

                    if(userSession.getUser_id() != commentToCreate.getUser_id()){
                        logger.warn("User attempted to create a comment as a different user. User ID (" + userSession.getUser_id() + ")");
                        throw new UnauthorizedException("User attempted to create a comment as a different user.");
                    }

                    commentToCreate.setCreated_date(currentTimestamp);
                    commentToCreate.setLast_modified_by(userSession.getUser_id());
                    commentToCreate.setLast_modified_date(currentTimestamp);
                }
            }
            else{
                logger.warn("User attempted to create a comment with an invalid session.");
                throw new InvalidSessionException("Attempt to create a comment with an invalid session.");
            }
        }

        Comment createdComment;

        try{
            createdComment = di.createComment(commentToCreate);
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(createdComment == null){
            logger.warn("Failed to create comment by user ID " + commentToCreate.getUser_id() + " on post ID " + commentToCreate.getPost_id() + " (Post Type: " + commentToCreate.getPost_type() + ")");
            throw new UnexpectedErrorException("Failed to create comment by user ID " + commentToCreate.getUser_id() + " on post ID " + commentToCreate.getPost_id());
        }

        GenericResponse<Comment> responseData = new GenericResponse<Comment>("SUCCESS", createdComment);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    //Send post_id and post_type
    @PostMapping(path = "/comment/getForPost", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<List<Comment>>> getCommentForPost(@RequestBody CommentRequest commentData) {
        validateRequestParameters(commentData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        List<Comment> commentsOnPost = new ArrayList<Comment>();
        String requestIP = commentData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received a GetCommentForPost request for Post ID " + commentData.getComment().getPost_id() + " (Post Type: " + commentData.getComment().getPost_type() + ") (IP Address: " + requestIP + ")");

        try{
            commentsOnPost = di.getCommentsForPost(commentData.getComment().getPost_id(), commentData.getComment().getPost_type());
        }
        catch (Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(commentsOnPost == null){
            logger.warn("Failed to get comments for post ID " + commentData.getComment().getPost_id() + " (Post Type: " + commentData.getComment().getPost_type() + ")");
            throw new UnexpectedErrorException("Failed to get comments for post ID " + commentData.getComment().getPost_id());
        }

        GenericResponse<List<Comment>> responseData = new GenericResponse<List<Comment>>("SUCCESS", commentsOnPost);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    //Send id
    @PostMapping(path = "/comment/delete", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<Comment>> deleteComment(@RequestBody CommentRequest commentData) {
        validateRequestParameters(commentData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
        Comment commentToDelete = commentData.getComment();
        String httpSessionId = commentData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = commentData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received a DeleteComment request from user ID " + commentToDelete.getUser_id() + " (IP Address: " + requestIP + ")");

        if(httpSessionId == null){
            logger.warn("User attempted to delete a comment without an active session.");
            throw new CreateOnActiveSessionException("Attempt to delete a comment without an active session.");
        }
        else{
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                UserPermission currentUserPermission;

                try{
                    currentUserPermission = di.getUserPermission(userSession.getUser_id());
                }
                catch(Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUserPermission == null){
                    logger.warn("Unable to get user permissions for user ID " + userSession.getUser_id());
                    throw new UnexpectedErrorException("Unable to get user permissions for user ID " + userSession.getUser_id());
                }
                else{
                    Comment tmpComment;

                    try{
                        tmpComment = di.getComment(commentToDelete.getId());
                    }
                    catch(Exception e){
                        throw new UnexpectedErrorException(e.getMessage());
                    }

                    if(tmpComment == null){
                        logger.warn("Failed to get comment data for user validation. Comment ID " + commentToDelete.getId());
                        throw new UnexpectedErrorException("Failed to get comment data for user validation.");
                    }
                    else{
                        commentToDelete.setUser_id(tmpComment.getUser_id());

                        if( (userSession.getUser_id() != commentToDelete.getUser_id()) && (currentUserPermission.getEdit_user() != 1) ){
                            logger.warn("User attempted to delete a comment as a different user without sufficient permissions. User ID (" + userSession.getUser_id() + ")");
                            throw new UnauthorizedException("User attempted to delete a comment as a different user without sufficient permissions.");
                        }

                        commentToDelete.setLast_modified_by(userSession.getUser_id());
                        commentToDelete.setLast_modified_date(currentTimestamp);
                    }
                }
            }
            else{
                logger.warn("User attempted to delete a comment with an invalid session.");
                throw new InvalidSessionException("Attempt to delete a comment with an invalid session.");
            }
        }

        Comment deletedComment;

        try{
            deletedComment = di.deleteComment(commentToDelete);
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(deletedComment == null){
            logger.warn("Failed to delete comment ID " + commentToDelete.getId());
            throw new UnexpectedErrorException("Failed to delete comment ID " + commentToDelete.getId());
        }

        GenericResponse<Comment> responseData = new GenericResponse<Comment>("SUCCESS", deletedComment);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }
}
