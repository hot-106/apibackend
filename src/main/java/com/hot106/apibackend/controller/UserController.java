package com.hot106.apibackend.controller;

import com.hot106.apibackend.exception.*;
import com.hot106.apibackend.objects.*;
import com.hot106.apibackend.requestobjects.*;
import com.hot106.apibackend.requestobjects.validation.RequestKey;
import com.hot106.apibackend.utils.DatabaseConfiguration;
import com.hot106.apibackend.utils.DatabaseInterface;
import com.hot106.apibackend.utils.SendGridConfiguration;
import com.hot106.apibackend.utils.SendGridMail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

@RestController
public class UserController {

    Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    DatabaseConfiguration dbConf;

    @Autowired
    SendGridConfiguration sendGridConf;

    private void validateRequestParameters(UserRequest urq) {

        DatabaseInterface di = new DatabaseInterface(dbConf);
        String currentKeyValue;

        try{
            currentKeyValue = di.getInternalApiKey();
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        RequestKey.validateRqk(urq.getRqk(), currentKeyValue, logger);

        if(urq.getUser() == null){
            logger.warn("User was not supplied in the request body. Request denied.");
            throw new MissingParametersException("User was not supplied in the request body. Request denied.");
        }

    }

    private void validateRequestParameters(GenericSearchRequest grq) {

        DatabaseInterface di = new DatabaseInterface(dbConf);
        String currentKeyValue;

        try{
            currentKeyValue = di.getInternalApiKey();
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        RequestKey.validateRqk(grq.getRqk(), currentKeyValue, logger);
    }

    private void validateRequestParameters(PasswordResetRequest prq) {

        DatabaseInterface di = new DatabaseInterface(dbConf);
        String currentKeyValue;

        try{
            currentKeyValue = di.getInternalApiKey();
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        RequestKey.validateRqk(prq.getRqk(), currentKeyValue, logger);

        if(prq.getPassword_reset() == null) {
            logger.warn("PasswordReset was not supplied in the request body. Request denied.");
            throw new MissingParametersException("PasswordReset was not supplied in the request body. Request denied.");
        }

    }

    private void validateRequestParameters(UserPermissionRequest uprq) {

        DatabaseInterface di = new DatabaseInterface(dbConf);
        String currentKeyValue;

        try{
            currentKeyValue = di.getInternalApiKey();
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        RequestKey.validateRqk(uprq.getRqk(), currentKeyValue, logger);

        if(uprq.getUser_permission() == null){
            logger.warn("UserPermission was not supplied in the request body. Request denied.");
            throw new MissingParametersException("UserPermission was not supplied in the request body. Request denied.");
        }

    }

    private void validateRequestParameters(VerifyRequest vrq) {

        DatabaseInterface di = new DatabaseInterface(dbConf);
        String currentKeyValue;

        try{
            currentKeyValue = di.getInternalApiKey();
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        RequestKey.validateRqk(vrq.getRqk(), currentKeyValue, logger);

        if(vrq.getVerify() == null){
            logger.warn("Verify was not supplied in the request body. Request denied.");
            throw new MissingParametersException("Verify was not supplied in the request body. Request denied.");
        }

    }

    private boolean validateUserSession(Session sessionToVerify, String clientIpAddress) {
        DatabaseInterface di = new DatabaseInterface(dbConf);
        boolean validated = false;

        try{
            if(di.validateUserSession(sessionToVerify, clientIpAddress) != null){
                logger.info("Session ID '" + sessionToVerify.getSession_id() + "' is valid for user " + sessionToVerify.getUser_id());
                validated = true;
            }
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        return validated;
    }

    @PostMapping(path = "/user/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<User>> createUser(@RequestBody UserRequest userData) {

        //Exception is thrown here on failure to validate
        validateRequestParameters(userData);
        User userToCreate = userData.getUser();
        String httpSessionId = userData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = userData.getRqk().getUserSession().getClientIpAddress();
        User createdUser = null;

        logger.info("Received a UserCreate request for email " + userToCreate.getEmail() + " (IP Address: " + requestIP + ")");

        DatabaseInterface di = new DatabaseInterface(dbConf);
        Timestamp currentTimestamp = new java.sql.Timestamp(System.currentTimeMillis());

        //Check if the user is attempting to create a new user with an active session
        if(httpSessionId != null){
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                logger.warn("User attempted to create a new user with an active session (" + httpSessionId + ")");
                throw new CreateOnActiveSessionException("Attempt to create a new user with an active session");
            }
        }

        //Set role ID for this user to USER_ROLE_ID_GENERAL_USER
        userToCreate.setRole_id(User.USER_ROLE_ID_GENERAL_USER);
        userToCreate.setLast_modified_by(0);
        userToCreate.setLast_modified_date(currentTimestamp);

        try{
            createdUser = di.createUser(userToCreate);
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(createdUser == null){
            logger.error("Failed to create user with email address '" + userToCreate.getEmail() + "'");
            throw new UnexpectedErrorException("Failed to create user with email address '" + userToCreate.getEmail() + "'");
        }

        //Set password to NULL in response
        createdUser.setPassword(null);

        GenericResponse<User> responseData = new GenericResponse<User>("SUCCESS", createdUser);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // (send first_name, last_name, username, password, and email fields)
    @PostMapping(path = "/user/createAdmin", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<User>> createAdminUser (@RequestBody UserRequest userData) {

        //Exception is thrown here on failure to validate
        validateRequestParameters(userData);
        User userToCreate = userData.getUser();
        String httpSessionId = userData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = userData.getRqk().getUserSession().getClientIpAddress();
        User createdUser = null;

        logger.info("Received a UserCreateAdmin request for email " + userToCreate.getEmail() + " (IP Address: " + requestIP + ")");

        DatabaseInterface di = new DatabaseInterface(dbConf);
        Timestamp currentTimestamp = new java.sql.Timestamp(System.currentTimeMillis());

        //Check if the user is attempting to create an admin user without an active session
        if(httpSessionId == null){
            logger.warn("User attempted to create an admin user without an active session. " + "IP Address: " + requestIP);
            throw new CreateOnActiveSessionException("User attempted to create an admin user without an active session.");
        }
        else{
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch (Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                UserPermission currentUserPermission;

                try{
                    currentUserPermission = di.getUserPermission(userSession.getUser_id());
                }
                catch (Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUserPermission == null){
                    logger.warn("Unable to get user permissions for user ID " + userSession.getUser_id());
                    throw new UnexpectedErrorException("Unable to get user permissions for user ID " + userSession.getUser_id());
                }
                else {
                    if (currentUserPermission.getEdit_user() != 1) {
                        logger.warn("User attempted to create an admin user without admin privileges " + "IP Address: " + requestIP);
                        throw new UnauthorizedException("Insufficient privileges to create a user with admin privileges");
                    }
                }
            }
            else{
                logger.warn("User attempted to update a user with an invalid session.");
                throw new InvalidSessionException("Attempt to update a user with an invalid session.");
            }
        }

        //Set role ID for this user to USER_ROLE_ID_ADMIN_USER
        userToCreate.setRole_id(User.USER_ROLE_ID_ADMIN_USER);
        userToCreate.setLast_modified_by(0);
        userToCreate.setLast_modified_date(currentTimestamp);

        try{
            createdUser = di.createUser(userToCreate);
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(createdUser == null){
            logger.error("Failed to create user with email address '" + userToCreate.getEmail() + "'");
            throw new UnexpectedErrorException("Failed to create user with email address '" + userToCreate.getEmail() + "'");
        }

        //Set password to NULL in response
        createdUser.setPassword(null);

        GenericResponse<User> responseData = new GenericResponse<User>("SUCCESS", createdUser);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // (send id field)
    @PostMapping(path = "/user/get", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<User>> getUser (@RequestBody UserRequest userData) {

        validateRequestParameters(userData);
        int userToGet = userData.getUser().getId();
        DatabaseInterface di = new DatabaseInterface(dbConf);

        String httpSessionId = userData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = userData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received a UserGet request for user ID " + userToGet + " (IP Address: " + requestIP + ")");

        if(httpSessionId == null){
            logger.warn("User attempted to get a user without an active session.");
            throw new CreateOnActiveSessionException("User attempted to get a user without an active session.");
        }
        else{
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                UserPermission currentUserPermission;

                try{
                    currentUserPermission = di.getUserPermission(userSession.getUser_id());
                }
                catch(Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUserPermission == null){
                    logger.warn("Unable to get user permissions for user ID " + userSession.getUser_id());
                    throw new UnexpectedErrorException("Unable to get user permissions for user ID " + userSession.getUser_id());
                }
                else {
                    if(userSession.getUser_id() != userToGet && currentUserPermission.getEdit_user() != 1){
                        logger.warn("User attempted to get user without sufficient privileges. User ID " + userSession.getUser_id());
                        throw new UnauthorizedException("Insufficient privileges to get a user with a different user ID");
                    }
                }
            }
            else{
                logger.warn("User attempted to get a user with an invalid session.");
                throw new InvalidSessionException("Attempt to get a user with an invalid session.");
            }
        }

        User allUserData;

        try{
            allUserData = di.getUser(userToGet);
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(allUserData == null){
            logger.error("Unable to get user ID " + userToGet);
            throw new NotFoundException("User ID " + userToGet + " was not found");
        }

        GenericResponse<User> responseData = new GenericResponse<User>("SUCCESS", allUserData);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // (send id field)
    @PostMapping(path = "/user/getPermissions", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<UserPermission>> getUserPermissions (@RequestBody UserRequest userData) {

        validateRequestParameters(userData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        String httpSessionId = userData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = userData.getRqk().getUserSession().getClientIpAddress();
        User userToGet = userData.getUser();

        logger.info("Received a GetUserPermissions request for user ID " + userToGet.getId () + " (IP Address: " + requestIP + ")");

        if(httpSessionId == null){
            logger.warn("User attempted to get user permissions without an active session.");
            throw new CreateOnActiveSessionException("Attempt to get user permissions without an active session.");
        }
        else{
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                UserPermission currentUserPermission;

                try{
                    currentUserPermission = di.getUserPermission(userSession.getUser_id());
                }
                catch(Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUserPermission == null){
                    logger.warn("Unable to get user permissions for user ID " + userSession.getUser_id());
                    throw new UnexpectedErrorException("Unable to get user permissions for user ID " + userSession.getUser_id());
                }
                else {
                    if(currentUserPermission.getEdit_user() != 1){
                        logger.warn("User attempted to get user permissions without sufficient privileges.. User ID (" + userSession.getUser_id() + ")");
                        throw new UnauthorizedException("User attempted to get user permissions without sufficient privileges..");
                    }
                }
            }
            else{
                logger.warn("User attempted to get user permissions with an invalid session.");
                throw new InvalidSessionException("Attempt to get user permissions with an invalid session.");
            }
        }

        if(userToGet.getId() == 0){
            logger.warn("User ID not provided in request");
            throw new MissingParametersException("User ID not provided in request");
        }

        UserPermission currentUserPermission;

        try{
            currentUserPermission = di.getUserPermission(userToGet.getId());
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(currentUserPermission == null){
            logger.error("Unable to get user permissions for user ID " + userToGet.getId());
            throw new UnexpectedErrorException("Unable to get user permissions for user ID " + userToGet.getId());
        }

        GenericResponse<UserPermission> responseData = new GenericResponse<UserPermission>("SUCCESS", currentUserPermission);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // (send search_query)
    @PostMapping(path = "/user/search", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<List<User>>> searchUsers (@RequestBody GenericSearchRequest userSearchData) {

        validateRequestParameters(userSearchData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        String httpSessionId = userSearchData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = userSearchData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received UserSearch request for query '" + userSearchData.getSearch_query() + "' (IP Address: " + requestIP + ")");

        //Check if the user is attempting to create an admin user without an active session
        if(httpSessionId == null){
            logger.warn("User attempted to search users without an active session. " + "IP Address: " + requestIP);
            throw new CreateOnActiveSessionException("User attempted to search users without an active session.");
        }
        else{
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                UserPermission currentUserPermission;

                try{
                    currentUserPermission = di.getUserPermission(userSession.getUser_id());
                }
                catch(Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUserPermission == null){
                    logger.warn("Unable to get user permissions for user ID " + userSession.getUser_id());
                    throw new UnexpectedErrorException("Unable to get user permissions for user ID " + userSession.getUser_id());
                }
                else {
                    if(currentUserPermission.getEdit_user() != 1){
                        logger.warn("User attempted to search users without admin privileges " + "IP Address: " + requestIP);
                        throw new UnauthorizedException("Insufficient privileges to search users.");
                    }
                }
            }
            else{
                logger.warn("User attempted to search users with an invalid session.");
                throw new InvalidSessionException("Attempt to search users with an invalid session.");
            }
        }

        List<User> foundUsers;

        try{
            foundUsers = di.searchUsers(userSearchData.getSearch_query());
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        GenericResponse<List<User>> responseData = new GenericResponse<List<User>>("SUCCESS", foundUsers);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // (send email and password fields)
    @PostMapping(path = "/user/checkPassword", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<User>> checkUserPassword (@RequestBody UserRequest userData) {

        validateRequestParameters(userData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        String httpSessionId = userData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = userData.getRqk().getUserSession().getClientIpAddress();
        User userToCheck = userData.getUser();
        User foundUser;

        logger.info("Received a CheckUserPassword request for user " + userToCheck.getEmail() + " (IP Address: " + requestIP + ")");

        if(userToCheck.getEmail() == null){
            logger.warn("Email not provided in request");
            throw new MissingParametersException("Missing required email field");
        }

        if(userToCheck.getPassword() == null){
            logger.warn("Password not provided in request");
            throw new MissingParametersException("Missing required password field");
        }

        if(httpSessionId != null){
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                logger.warn("User " + userToCheck.getEmail() + " attempted to login (checkPassword) with an active session set. Session ID: " + httpSessionId);
                throw new CreateOnActiveSessionException("Attempt to login (checkPassword) with an active session set.");
            }
        }

        try{
            foundUser = di.checkUserPassword(userToCheck);
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(foundUser == null){
            logger.info("No matching user was found with the provided email/password combination (Email: " + userToCheck.getEmail() + ")");
            throw new UnauthorizedException("No matching user was found with the provided email/password combination.");
        }
        else{
            logger.info("Successfully validated password for " + userToCheck.getEmail() + " (User ID: " + foundUser.getId() + ")");
        }

        GenericResponse<User> responseData = new GenericResponse<User>("AUTHORIZED", foundUser);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // (send username field)
    @PostMapping(path = "/user/checkUsername", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<User>> checkUsername (@RequestBody UserRequest userData) {

        validateRequestParameters(userData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        String requestIP = userData.getRqk().getUserSession().getClientIpAddress();
        GenericResponse<User> responseData;

        logger.info("Received a CheckUsername request for username " + userData.getUser().getUsername() + " (IP Address: " + requestIP + ")");

        try{
            if(di.checkUsernameExists(userData.getUser().getUsername())){
                responseData = new GenericResponse<User>("NOT AVAILABLE", null);
            }
            else{
                responseData = new GenericResponse<User>("AVAILABLE", null);
            }
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // (send email field)
    @PostMapping(path = "/user/checkEmail", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<User>> checkUserEmail (@RequestBody UserRequest userData) {

        validateRequestParameters(userData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        String requestIP = userData.getRqk().getUserSession().getClientIpAddress();
        GenericResponse<User> responseData;

        logger.info("Received a CheckEmail request for email " + userData.getUser().getEmail() + " (IP Address: " + requestIP + ")");

        try{
            if(di.checkEmailExists(userData.getUser().getEmail())){
                responseData = new GenericResponse<User>("NOT AVAILABLE", null);
            }
            else{
                responseData = new GenericResponse<User>("AVAILABLE", null);
            }
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // (send id, first_name, last_name, username, email, role_id, and img fields)
    @PostMapping(path = "/user/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<User>> updateUser (@RequestBody UserRequest userData) {

        validateRequestParameters(userData);
        Timestamp currentTimestamp = new java.sql.Timestamp(System.currentTimeMillis());
        DatabaseInterface di = new DatabaseInterface(dbConf);
        String httpSessionId = userData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = userData.getRqk().getUserSession().getClientIpAddress();
        User userToUpdate = userData.getUser();

        logger.info("Received a UserUpdate request for user ID " + userToUpdate.getId() + " (IP Address: " + requestIP + ")");

        if(httpSessionId == null){
            logger.warn("User attempted to update a user without an active session.");
            throw new CreateOnActiveSessionException("Attempt to update a user without an active session.");
        }
        else{
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                User currentUser;
                UserPermission currentUserPermission;

                try{
                    currentUser = di.getUser(userSession.getUser_id());
                    currentUserPermission = di.getUserPermission(userSession.getUser_id());
                }
                catch (Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUserPermission == null){
                    logger.warn("Unable to get user permissions for user ID " + userSession.getUser_id());
                    throw new UnexpectedErrorException("Unable to get user permissions for user ID " + userSession.getUser_id());
                }
                else {
                    if(currentUser.getId() != userToUpdate.getId() && currentUserPermission.getEdit_user() != 1){
                        logger.warn("User attempted to update a different user ID without sufficient privileges. User ID (" + userSession.getUser_id() + ")");
                        throw new UnauthorizedException("Attempt to update a different user ID without sufficient privileges.");
                    }
                }

                //Set last modified by for update
                userToUpdate.setLast_modified_by(userSession.getUser_id());
            }
            else{
                logger.warn("User attempted to update a user with an invalid session.");
                throw new InvalidSessionException("Attempt to update a user with an invalid session.");
            }
        }

        if(userToUpdate.getId() == 0){
            logger.warn("User ID not provided in request");
            throw new MissingParametersException("User ID not provided in request");
        }

        //Set last updated time to now
        userToUpdate.setLast_modified_date(currentTimestamp);

        User updatedUser;

        try{
            updatedUser = di.updateUser(userToUpdate);
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(updatedUser == null){
            logger.warn("Update failed for user ID " + userToUpdate.getId());
            throw new UnexpectedErrorException("Failed to update user ID " + userToUpdate.getId());
        }

        GenericResponse<User> responseData = new GenericResponse<User>("SUCCESS", updatedUser);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // (send ALL user permission fields)
    @PostMapping(path = "/user/updatePermissions", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<UserPermission>> changeUserPermission (@RequestBody UserPermissionRequest userPermissionData) {

        validateRequestParameters(userPermissionData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        String httpSessionId = userPermissionData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = userPermissionData.getRqk().getUserSession().getClientIpAddress();
        UserPermission userPermissionToUpdate = userPermissionData.getUser_permission();

        logger.info("Received an UpdateUserPermissions request for user ID " + userPermissionToUpdate.getUser_id() + " (IP Address: " + requestIP + ")");

        if(httpSessionId == null){
            logger.warn("User attempted to update user permissions without an active session.");
            throw new CreateOnActiveSessionException("Attempt to update user permissions without an active session.");
        }
        else{
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                UserPermission currentUserPermission;

                try{
                    currentUserPermission = di.getUserPermission(userSession.getUser_id());
                }
                catch(Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUserPermission == null){
                    logger.warn("Unable to get user permissions for user ID " + userSession.getUser_id());
                    throw new UnexpectedErrorException("Unable to get user permissions for user ID " + userSession.getUser_id());
                }
                else {
                    if(currentUserPermission.getEdit_user() != 1){
                        logger.warn("User attempted to update user permissions without sufficient privileges.. User ID (" + userSession.getUser_id() + ")");
                        throw new UnauthorizedException("User attempted to update user permissions without sufficient privileges..");
                    }
                }
            }
            else{
                logger.warn("User attempted to update user permissions with an invalid session.");
                throw new InvalidSessionException("Attempt to update user permissions with an invalid session.");
            }
        }

        if(userPermissionToUpdate.getUser_id() == 0){
            logger.warn("User ID not provided in request");
            throw new MissingParametersException("User ID not provided in request");
        }

        UserPermission updatedUserPermission;

        try{
            updatedUserPermission = di.updateUserPermission(userPermissionToUpdate);
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(updatedUserPermission == null){
            logger.error("Unable to update user permissions for user ID " + userPermissionToUpdate.getUser_id());
            throw new UnexpectedErrorException("Unable to update user permissions for user ID " + userPermissionToUpdate.getUser_id());
        }

        GenericResponse<UserPermission> responseData = new GenericResponse<UserPermission>("SUCCESS", updatedUserPermission);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // (send id and password field)
    @PostMapping(path = "/user/changePassword", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<User>> changeUserPassword (@RequestBody UserRequest userData) {

        validateRequestParameters(userData);
        Timestamp currentTimestamp = new java.sql.Timestamp(System.currentTimeMillis());
        DatabaseInterface di = new DatabaseInterface(dbConf);
        String httpSessionId = userData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = userData.getRqk().getUserSession().getClientIpAddress();
        User userToUpdate = userData.getUser();
        User updatedUser;

        logger.info("Received a ChangePassword request for user ID " + userToUpdate.getId() + " (IP Address: " + requestIP + ")");

        if(httpSessionId == null){
            logger.warn("User attempted to change password without an active session.");
            throw new CreateOnActiveSessionException("Attempt to change password without an active session.");
        }
        else{
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                User currentUser;

                try{
                    currentUser = di.getUser(userSession.getUser_id());
                }
                catch(Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUser.getId() != userToUpdate.getId()){
                    logger.warn("User attempted to change password for a different user. User ID (" + userSession.getUser_id() + ")");
                    throw new UnauthorizedException("User attempted to change password for a different user ID.");
                }
            }
            else{
                logger.warn("User attempted to change password with an invalid session.");
                throw new InvalidSessionException("Attempt to change password with an invalid session.");
            }
        }

        if(userToUpdate.getId() == 0){
            logger.warn("User ID was not provided on request to update user password.");
            throw new InvalidParametersException("User ID was not provided on request to update user password.");
        }

        if(userToUpdate.getPassword() == null){
            logger.warn("Password was not provided on request to update user password.");
            throw new InvalidParametersException("Password was not provided on request to update user password.");
        }

        try{
            if(di.getUser(userToUpdate.getId()).getDeleted_date() != null){
                logger.warn("User ID " + userToUpdate.getId() + " has been deleted. Cannot update password.");
                throw new UserDeletedException("User with email " + userToUpdate.getId() + " has been deleted. Cannot update password.");
            }
        }
        catch(SQLException e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        //Set last modified by and last modified time
        userToUpdate.setLast_modified_by(userToUpdate.getId());
        userToUpdate.setLast_modified_date(currentTimestamp);

        try{
            if(di.updateUserPassword(userToUpdate)){
                updatedUser = di.getUser(userToUpdate.getId());
            }
            else{
                logger.error("Failed to update password for user ID " + userToUpdate.getId());
                throw new UnexpectedErrorException("Failed to update password for user ID " + userToUpdate.getId());
            }
        }
        catch(SQLException e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        GenericResponse<User> responseData = new GenericResponse<User>("SUCCESS", updatedUser);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // (send user_id field)
    @PostMapping(path = "/user/passwordReset", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<PasswordReset>> createPasswordReset (@RequestBody PasswordResetRequest passwordResetData) {
        validateRequestParameters(passwordResetData);
        String httpSessionId = passwordResetData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = passwordResetData.getRqk().getUserSession().getClientIpAddress();
        PasswordReset passwordResetToCreate = passwordResetData.getPassword_reset();

        logger.info("Received a PasswordReset request for reset token " + passwordResetToCreate.getReset_token() + " (IP Address: " + requestIP + ")");
        DatabaseInterface di = new DatabaseInterface(dbConf);

        if(passwordResetToCreate.getUser_id() == 0){
            logger.warn("User ID not provided in create password reset token request");
            throw new InvalidParametersException("User ID not provided in request.");
        }

        if(httpSessionId != null){
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                User currentUser;
                UserPermission currentUserPermission;

                try{
                    currentUser = di.getUser(userSession.getUser_id());
                    currentUserPermission = di.getUserPermission(userSession.getUser_id());
                }
                catch (Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUserPermission == null){
                    logger.warn("Unable to get user permissions for user ID " + userSession.getUser_id());
                    throw new UnexpectedErrorException("Unable to get user permissions for user ID " + userSession.getUser_id());
                }
                else {
                    if(currentUser.getId() != passwordResetToCreate.getUser_id() && currentUserPermission.getEdit_user() != 1){
                        logger.warn("User attempted to create a password reset for a different user ID without sufficient privileges. User ID (" + userSession.getUser_id() + ")");
                        throw new UnauthorizedException("Attempt to create a password reset for a different user ID without sufficient privileges.");
                    }
                }
            }
        }

        try{
            if(di.getUser(passwordResetToCreate.getUser_id()).getDeleted_date() != null){
                logger.warn("User ID " + passwordResetToCreate.getUser_id() + " has been deleted. Cannot reset password.");
                throw new UserDeletedException("User with email " + passwordResetToCreate.getUser_id() + " has been deleted. Cannot reset password.");
            }
        }
        catch (SQLException e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        PasswordReset createdPasswordReset;

        try{
            createdPasswordReset = di.createPasswordReset(passwordResetToCreate.getUser_id());
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(createdPasswordReset == null){
            logger.error("Failed to create password reset for user ID " + passwordResetToCreate.getUser_id());
            throw new UnexpectedErrorException("Failed to create password reset for user ID " + passwordResetToCreate.getUser_id());
        }

        GenericResponse<PasswordReset> responseData = new GenericResponse<PasswordReset>("SUCCESS", createdPasswordReset);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // (send reset_token field)
    @PostMapping(path = "/user/validatePasswordResetToken", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<PasswordReset>> validatePasswordResetToken (@RequestBody PasswordResetRequest passwordResetData) {
        validateRequestParameters(passwordResetData);
        String requestIP = passwordResetData.getRqk().getUserSession().getClientIpAddress();
        PasswordReset passwordResetToValidate = passwordResetData.getPassword_reset();

        logger.info("Received a ValidatePasswordResetToken request for reset token " + passwordResetToValidate.getReset_token() + " (IP Address: " + requestIP + ")");
        DatabaseInterface di = new DatabaseInterface(dbConf);

        if(passwordResetToValidate.getReset_token() == null){
            logger.warn("Reset token not provided in validate password reset token request");
            throw new InvalidParametersException("Reset token not provided in request.");
        }

        PasswordReset validPasswordReset;

        try{
            validPasswordReset = di.validatePasswordReset(passwordResetToValidate.getReset_token());
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(validPasswordReset == null){
            logger.warn("Password reset token '" + passwordResetToValidate.getReset_token() + "' is not valid.");
            throw new InvalidParametersException("Password reset token '" + passwordResetToValidate.getReset_token() + "' is not valid.");
        }

        GenericResponse<PasswordReset> responseData = new GenericResponse<PasswordReset>("VALID", validPasswordReset);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // (send reset_token and password fields)
    @PostMapping(path = "/user/completePasswordReset", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<PasswordReset>> completePasswordReset (@RequestBody PasswordResetRequest passwordResetData) {
        validateRequestParameters(passwordResetData);
        String requestIP = passwordResetData.getRqk().getUserSession().getClientIpAddress();
        PasswordReset passwordResetToComplete = passwordResetData.getPassword_reset();

        logger.info("Received a CompletePasswordReset request for reset token '" + passwordResetToComplete.getReset_token() + "' (IP Address: " + requestIP + ")");
        DatabaseInterface di = new DatabaseInterface(dbConf);

        if(passwordResetToComplete.getReset_token() == null){
            logger.warn("Reset token not provided in complete password reset token request");
            throw new InvalidParametersException("Reset token not provided in request.");
        }

        if(passwordResetToComplete.getPassword() == null){
            logger.warn("Password not provided in validate password reset token request");
            throw new InvalidParametersException("Password not provided in request.");
        }

        PasswordReset completedPasswordReset;

        try{
            completedPasswordReset = di.completePasswordReset(passwordResetToComplete);
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(completedPasswordReset == null){
            logger.error("Failed to complete password reset for reset token '" + passwordResetToComplete.getReset_token() + "'");
            throw new UnexpectedErrorException("Failed to complete password reset for reset token '" + passwordResetToComplete.getReset_token() + "'");
        }

        GenericResponse<PasswordReset> responseData = new GenericResponse<PasswordReset>("SUCCESS", completedPasswordReset);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // (send id field)
    @PostMapping(path = "/user/delete", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<User>> deleteUser (@RequestBody UserRequest userData) {
        validateRequestParameters(userData);
        Timestamp currentTimestamp = new java.sql.Timestamp(System.currentTimeMillis());
        DatabaseInterface di = new DatabaseInterface(dbConf);
        String httpSessionId = userData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = userData.getRqk().getUserSession().getClientIpAddress();
        User userToDelete = userData.getUser();

        logger.info("Received a UserDelete request for user ID " + userToDelete.getId() + " (IP Address: " + requestIP + ")");

        if(httpSessionId == null){
            logger.warn("User attempted to delete a user without an active session.");
            throw new CreateOnActiveSessionException("Attempt to delete a user without an active session.");
        }
        else{
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                User currentUser;
                UserPermission currentUserPermission;

                try{
                    currentUser = di.getUser(userSession.getUser_id());
                    currentUserPermission = di.getUserPermission(userSession.getUser_id());
                }
                catch (Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUserPermission == null){
                    logger.warn("Unable to get user permissions for user ID " + userSession.getUser_id());
                    throw new UnexpectedErrorException("Unable to get user permissions for user ID " + userSession.getUser_id());
                }
                else {
                    if(currentUser.getId() != userToDelete.getId() && currentUserPermission.getEdit_user() != 1){
                        logger.warn("User attempted to delete a different user ID without sufficient privileges. User ID (" + userSession.getUser_id() + ")");
                        throw new UnauthorizedException("Attempt to delete a different user ID without sufficient privileges.");
                    }
                }

                //Set last modified by for update
                userToDelete.setLast_modified_by(userSession.getUser_id());
            }
            else{
                logger.warn("User attempted to delete a user with an invalid session.");
                throw new InvalidSessionException("Attempt to delete a user with an invalid session.");
            }
        }

        if(userToDelete.getId() == 0){
            logger.warn("User ID not provided in request");
            throw new MissingParametersException("User ID not provided in request");
        }

        //Set last updated time to now
        userToDelete.setLast_modified_date(currentTimestamp);

        User deletedUser;

        try{
            deletedUser = di.deleteUser(userToDelete.getId());
        }
        catch (Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(deletedUser == null){
            logger.error("Failed to delete user ID " + userToDelete.getId());
            throw new UnexpectedErrorException("Failed to delete user ID " + userToDelete.getId());
        }

        GenericResponse<User> responseData = new GenericResponse<User>("SUCCESS", deletedUser);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // (send user_id field)
    @PostMapping(path = "/user/generateVerifyToken", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<Verify>> generateEmailVerifyToken (@RequestBody VerifyRequest verifyData) {
        validateRequestParameters(verifyData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        String httpSessionId = verifyData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = verifyData.getRqk().getUserSession().getClientIpAddress();
        SendGridMail sendGridMail = new SendGridMail(sendGridConf);
        Verify verifyToCreate = verifyData.getVerify();
        User userToVerify;

        logger.info("Received a GenerateVerifyToken request for user ID " + verifyToCreate.getUser_id() + " (IP Address: " + requestIP + ")");

        if(httpSessionId == null){
            logger.warn("User attempted to create a verify email token without an active session.");
            throw new CreateOnActiveSessionException("Attempt to create a verify email token without an active session.");
        }
        else{
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                User currentUser;
                UserPermission currentUserPermission;

                try{
                    currentUser = di.getUser(userSession.getUser_id());
                    currentUserPermission = di.getUserPermission(userSession.getUser_id());
                }
                catch (Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUserPermission == null){
                    logger.warn("Unable to get user permissions for user ID " + userSession.getUser_id());
                    throw new UnexpectedErrorException("Unable to get user permissions for user ID " + userSession.getUser_id());
                }
                else {
                    if(currentUser.getId() == verifyToCreate.getUser_id() && currentUserPermission.getVerify_user() != 1){
                        logger.warn("User attempted to create a verify email token without sufficient privileges. User ID (" + userSession.getUser_id() + ")");
                        throw new UnauthorizedException("User attempted to create a verify email token without sufficient privileges.");
                    }

                    if(currentUser.getId() != verifyToCreate.getUser_id() && currentUserPermission.getEdit_user() != 1){
                        logger.warn("User attempted to create a verify email token for another user without sufficient privileges. User ID (" + userSession.getUser_id() + ")");
                        throw new UnauthorizedException("User attempted to create a verify email token for another user without sufficient privileges.");
                    }
                }
            }
            else{
                logger.warn("User attempted to create a verify email token with an invalid session.");
                throw new InvalidSessionException("Attempt to create a verify email token with an invalid session.");
            }
        }

        try{
            userToVerify = di.getUser(verifyToCreate.getUser_id());

            if(userToVerify.getDeleted_date() != null){
                logger.warn("User ID " + verifyToCreate.getUser_id() + " has been deleted. Cannot verify email.");
                throw new UserDeletedException("User with email " + verifyToCreate.getUser_id() + " has been deleted. Cannot verify email.");
            }
        }
        catch(SQLException e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        Verify createdVerifyToken;

        try{
            createdVerifyToken = di.createVerifyEmailToken(verifyToCreate.getUser_id());
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(createdVerifyToken == null){
            logger.error("Failed to create verify token for user ID " + verifyToCreate.getUser_id());
            throw new UnexpectedErrorException("Failed to create password reset for user ID " + verifyToCreate.getUser_id());
        }

        if(!sendGridMail.sendVerifyEmail(userToVerify, createdVerifyToken)){
            logger.error("Failed to send verify email to user ID " + verifyToCreate.getUser_id());
            throw new UnexpectedErrorException("Failed to send verify email to user ID " + verifyToCreate.getUser_id());
        }

        GenericResponse<Verify> responseData = new GenericResponse<Verify>("SUCCESS", createdVerifyToken);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // (send verify_token field)
    @PostMapping(path = "/user/verifyEmail", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<Verify>> verifyUserEmail (@RequestBody VerifyRequest verifyData) {
        validateRequestParameters(verifyData);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        String httpSessionId = verifyData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = verifyData.getRqk().getUserSession().getClientIpAddress();
        Verify verifyToComplete = verifyData.getVerify();

        logger.info("Received a VerifyEmail request for verify token '" + verifyToComplete.getVerify_token() + "' (IP Address: " + requestIP + ")");

        if(httpSessionId != null){
            Session userSession;

            try{
                userSession = di.getUserSession(httpSessionId);
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }

            if(validateUserSession(userSession, requestIP)){
                User currentUser;

                try{
                    currentUser = di.getUser(userSession.getUser_id());
                }
                catch (Exception e){
                    throw new UnexpectedErrorException(e.getMessage());
                }

                if(currentUser.getId() == verifyToComplete.getUser_id()){
                    logger.warn("User attempted to complete a verify email request for a different user. User ID (" + userSession.getUser_id() + ")");
                    throw new UnauthorizedException("User attempted to complete a verify email request for a different user.");
                }
            }
        }

        Verify completedVerifyToken;

        try{
            completedVerifyToken = di.completeVerifyEmailToken(verifyToComplete);
        }
        catch (Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(completedVerifyToken == null){
            logger.error("Failed to complete password reset for reset token '" + verifyToComplete.getVerify_token() + "'");
            throw new UnexpectedErrorException("Failed to complete password reset for reset token '" + verifyToComplete.getVerify_token() + "'");
        }

        GenericResponse<Verify> responseData = new GenericResponse<Verify>("SUCCESS", completedVerifyToken);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }
}
