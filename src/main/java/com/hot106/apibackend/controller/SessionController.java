package com.hot106.apibackend.controller;

import com.hot106.apibackend.exception.*;
import com.hot106.apibackend.objects.UserPermission;
import com.hot106.apibackend.requestobjects.SessionRequest;
import com.hot106.apibackend.requestobjects.validation.RequestKey;
import com.hot106.apibackend.utils.DatabaseConfiguration;
import com.hot106.apibackend.utils.DatabaseInterface;
import com.hot106.apibackend.objects.GenericResponse;
import com.hot106.apibackend.objects.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;

@RestController
public class SessionController {

    Logger logger = LoggerFactory.getLogger(SessionController.class);

    @Autowired
    DatabaseConfiguration dbConf;

    private void validateRequestParameters(SessionRequest srq, boolean requireSessionObject) {

        DatabaseInterface di = new DatabaseInterface(dbConf);
        String currentKeyValue;

        try{
            currentKeyValue = di.getInternalApiKey();
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        RequestKey.validateRqk(srq.getRqk(), currentKeyValue, logger);

        if(requireSessionObject){
            if(srq.getSession() == null){
                logger.warn("Session was not supplied in the request body. Request denied.");
                throw new MissingParametersException("Session was not supplied in the request body. Request denied.");
            }
        }
    }

    // /session/create
    // (send user_id field)
    @PostMapping(path = "/session/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<Session>> createSession(@RequestBody SessionRequest sessionData) {

        //Exception is thrown here on failure to validate
        validateRequestParameters(sessionData, true);
        Session sessionToCreate = sessionData.getSession();
        String httpSessionId = sessionData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = sessionData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received a SessionCreate request for user ID " + sessionToCreate.getUser_id() + " (IP Address: " + requestIP + ")");

        DatabaseInterface di = new DatabaseInterface(dbConf);
        Timestamp currentTimestamp = new java.sql.Timestamp(System.currentTimeMillis());
        String newSessionId = Session.generateNewSessionToken();

        //Test if user already has a valid session set. If so, do not allow a new session to be created.
        if(httpSessionId != null){
            Session current_session;

            try{
                current_session = di.getUserSession(httpSessionId);

                if(current_session != null && di.validateUserSession(current_session, requestIP) != null){
                    logger.warn("User ID " + sessionToCreate.getUser_id() + " already has an active session");
                    throw new CreateOnActiveSessionException("User ID " + sessionToCreate.getUser_id() + " already has an active session (" + current_session.getSession_id() + ")");
                }
                else{
                    UserPermission userPermission = di.getUserPermission(sessionToCreate.getUser_id());
                    if(userPermission == null){
                        logger.warn("Unable to get user permissions for user ID " + sessionToCreate.getUser_id());
                        throw new UnexpectedErrorException("Unable to get user permissions for user ID " + sessionToCreate.getUser_id());
                    }
                    else{
                        if(userPermission.getCreate_session() == 0){
                            logger.warn("User ID " + sessionToCreate.getUser_id() + " does not have permission to create a new session.");
                            throw new UnauthorizedException("User ID " + sessionToCreate.getUser_id() + " does not have permission to create a new session.");
                        }
                    }
                }
            }
            catch(Exception e){
                throw new UnexpectedErrorException(e.getMessage());
            }
        }

        //Check the newly generated session token to see if it already exists in the database. If so, generate a new one.
        try{
            while(di.checkSessionTokenExists(newSessionId)){
                newSessionId = Session.generateNewSessionToken();
            }
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        // Populate data into session object
        sessionToCreate.setSession_id(newSessionId);
        sessionToCreate.setCreated_at(currentTimestamp);
        sessionToCreate.setLast_action("Session Created");
        sessionToCreate.setLast_action_at(currentTimestamp);
        sessionToCreate.setLast_action_ip(requestIP);

        //Create session
        Session session;

        try{
            session = di.createUserSession(sessionToCreate);
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(session == null){
            logger.error("Failed to create session ID " + sessionToCreate.getSession_id() + " for user ID " + sessionToCreate.getUser_id());
            throw new UnexpectedErrorException("Failed to create session for user ID " + sessionToCreate.getUser_id());
        }

        GenericResponse<Session> responseData = new GenericResponse<Session>("SUCCESS", session);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // /session/validate
    // (send apikey and session ID in rqk)
    @PostMapping(path = "/session/validate", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<Session>> validateSession(@RequestBody SessionRequest sessionData) {

        //Exception is thrown here on failure to validate
        validateRequestParameters(sessionData, false);
        DatabaseInterface di = new DatabaseInterface(dbConf);
        String httpSessionId = sessionData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = sessionData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received a SessionValidate request for Session ID " + httpSessionId + " (IP Address: " + requestIP + ")");

        if(httpSessionId == null){
            logger.info("No session is currently active for this user.");
            throw new SessionDoesNotExistException("No session is currently active for this user.");
        }

        Session sessionToValidate;

        try{
            sessionToValidate = di.getUserSession(httpSessionId);
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(sessionToValidate == null){
            logger.warn("Session ID " + httpSessionId + " does not exist.");
            throw new SessionDoesNotExistException("Session ID " + httpSessionId + " does not exist.");
        }

        Session validatedSession;

        try{
            validatedSession = di.validateUserSession(sessionToValidate, requestIP);
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(validatedSession == null){
            logger.info("Session ID " + httpSessionId + " is invalid for user ID " + sessionToValidate.getUser_id());
            throw new InvalidSessionException("Session ID " + httpSessionId + " is invalid");
        }
        else{
            logger.info("Session ID " + httpSessionId + " is valid for user ID " + validatedSession.getUser_id());
        }

        GenericResponse<Session> responseData = new GenericResponse<Session>("VERIFIED", validatedSession);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // /session/update
    // (send session_id and last_action fields)
    @PostMapping(path = "/session/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<Session>> updateSession(@RequestBody SessionRequest sessionData) {

        //Exception is thrown here on failure to validate
        validateRequestParameters(sessionData, true);
        Session sessionToUpdate = sessionData.getSession();
        String httpSessionId = sessionData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = sessionData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received a SessionUpdate request for session ID " + httpSessionId + " (IP Address: " + requestIP + ")");
        Timestamp currentTimestamp = new java.sql.Timestamp(System.currentTimeMillis());
        DatabaseInterface di = new DatabaseInterface(dbConf);

        sessionToUpdate.setSession_id(httpSessionId);
        sessionToUpdate.setLast_action_at(currentTimestamp);
        sessionToUpdate.setLast_action_ip(requestIP);

        Session updatedSession;

        try{
            updatedSession = di.updateUserSession(sessionToUpdate);
        }
        catch(Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(updatedSession != null){
            logger.info("Successfully updated session ID " + sessionToUpdate.getSession_id() + " for user ID " + sessionToUpdate.getUser_id());
        }
        else{
            logger.error("Failed to update session ID " + sessionToUpdate.getSession_id() + " for user ID " + sessionToUpdate.getUser_id());
            throw new UnexpectedErrorException("Failed to update session ID " + sessionToUpdate.getSession_id());
        }

        GenericResponse<Session> responseData = new GenericResponse<Session>("SUCCESS", updatedSession);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    // /session/end
    // (send nothing - only session ID from rqk is used)
    @PostMapping(path = "/session/end", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResponse<Session>> endSession(@RequestBody SessionRequest sessionData) {

        //Exception is thrown here on failure to validate
        validateRequestParameters(sessionData, false);
        Session sessionToEnd = new Session();
        String httpSessionId = sessionData.getRqk().getUserSession().getHttpSessionId();
        String requestIP = sessionData.getRqk().getUserSession().getClientIpAddress();

        logger.info("Received a SessionEnd request for session ID " + httpSessionId + " (IP Address: " + requestIP + ")");
        Timestamp currentTimestamp = new java.sql.Timestamp(System.currentTimeMillis());
        DatabaseInterface di = new DatabaseInterface(dbConf);

        sessionToEnd.setSession_id(httpSessionId);
        sessionToEnd.setLast_action("Session Ended - User Request");
        sessionToEnd.setLast_action_at(currentTimestamp);
        sessionToEnd.setLast_action_ip(requestIP);
        sessionToEnd.setSession_ended_at(currentTimestamp);

        boolean sessionEnded;

        try{
            sessionEnded = di.endUserSession(sessionToEnd, sessionToEnd.getLast_action_ip(), "Session Ended - User Request");
        }
        catch (Exception e){
            throw new UnexpectedErrorException(e.getMessage());
        }

        if(!sessionEnded){
            logger.error("Failed to end session ID " + sessionToEnd.getSession_id());
            throw new UnexpectedErrorException("Failed to end session ID " + sessionToEnd.getSession_id() + " for user ID " + sessionToEnd.getUser_id());
        }

        logger.info("Session ID " + sessionToEnd.getSession_id() + " ended successfully for user ID " + sessionToEnd.getUser_id());

        GenericResponse<Session> responseData = new GenericResponse<Session>("SUCCESS", sessionToEnd);

        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

}
